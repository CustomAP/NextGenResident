package nextgenentrycard.nextgenResident.Home;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;

import nextgenentrycard.nextgenResident.DSP.AddDSPActivity;
import nextgenentrycard.nextgenResident.Directory.DirectoryActivity;
import nextgenentrycard.nextgenResident.Forum.ForumActivity;
import nextgenentrycard.nextgenResident.Frequency.FrequencyActivity;
import nextgenentrycard.nextgenResident.Models.HomeItem;
import nextgenentrycard.nextgenResident.Models.PreBookItem;
import nextgenentrycard.nextgenResident.MyVisitors.MyVisitorsActivity;
import nextgenentrycard.nextgenResident.NearbyUtilities.NearbyUtilitiesActivity;
import nextgenentrycard.nextgenResident.PreBook.PreBookActivity;
import nextgenentrycard.nextgenResident.PreBookings.PrebookingsActivity;
import nextgenentrycard.nextgenResident.R;
import nextgenentrycard.nextgenResident.Statistics.StatisticsActivity;


/**
 * Created by Ashish Pawar(ashishpawar2015.ap@gmail.com) on 26/1/18.
 */

public class HomeAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private ArrayList<HomeItem> homeItems;
    private Context context;

    public HomeAdapter(ArrayList<HomeItem> homeItems){
        this.homeItems = homeItems;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.home_single_item, parent, false);
        return new HomeHolder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        final HomeHolder homeHolder = (HomeHolder)holder;
        context = homeHolder.ivOptionName.getContext();

        homeHolder.ivOptionName.setImageResource(homeItems.get(position).getImage());
        homeHolder.tvOptionName.setText(homeItems.get(position).getOptionName());

        homeHolder.llHome.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                switch (homeHolder.getAdapterPosition()){
                    case 0:
                        Intent i1 = new Intent(context, PreBookActivity.class);
                        i1.putExtra("PreBookItem",new PreBookItem("","","","","","","","","",0,0));
                        context.startActivity(i1);
                        break;
                    case 1:
                        Intent i = new Intent(context, AddDSPActivity.class);
                        context.startActivity(i);
                        break;
                    case  2:
                        i = new Intent(context, MyVisitorsActivity.class);
                        context.startActivity(i);
                        break;
                    case 3:
                        i = new Intent(context, PrebookingsActivity.class);
                        context.startActivity(i);
                        break;
                    case 4:
//                        i = new Intent(context, RoomBookingActivity.class);
//                        context.startActivity(i);
                        break;
                    case 5:
                        i = new Intent(context, NearbyUtilitiesActivity.class);
                        context.startActivity(i);
                        break;
                    case 6:
                        i = new Intent(context, DirectoryActivity.class);
                        context.startActivity(i);
                        break;
                    case 7:
                        i = new Intent(context, ForumActivity.class);
                        context.startActivity(i);
                        break;
                    case 8:
                        i = new Intent(context, StatisticsActivity.class);
                        context.startActivity(i);
                        break;
                    case 9:
                        i = new Intent(context, FrequencyActivity.class);
                        context.startActivity(i);
                        break;
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return homeItems.size();
    }

    class HomeHolder extends RecyclerView.ViewHolder {
        TextView tvOptionName;
        ImageView ivOptionName;
        LinearLayout llHome;

        public HomeHolder(View itemView) {
            super(itemView);
            tvOptionName = (TextView) itemView.findViewById(R.id.tvOptionName_homeSingleItem);
            ivOptionName = (ImageView) itemView.findViewById(R.id.ivHome_homeSingleItem);
            llHome = (LinearLayout)itemView.findViewById(R.id.llHomeSingleItem);
        }
    }

}
