package nextgenentrycard.nextgenResident.Home;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

import nextgenentrycard.nextgenResident.Models.HomeItem;
import nextgenentrycard.nextgenResident.R;


/**
 * Created by Ashish Pawar(ashishpawar2015.ap@gmail.com) on 28/1/18.
 */

public class HomeFragment extends Fragment {

    RecyclerView rvHome;
    ArrayList<HomeItem> homeItems;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_home, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        rvHome = (RecyclerView) view.findViewById(R.id.rvHomeFragment);

        homeItems = new ArrayList<>();
        fillHomeItems();

        HomeAdapter homeAdapter = new HomeAdapter(homeItems);
        rvHome.setAdapter(homeAdapter);
        rvHome.setLayoutManager(new GridLayoutManager(getContext(), 2));
        rvHome.setItemAnimator(new DefaultItemAnimator());
        rvHome.setNestedScrollingEnabled(false);
    }

    private void fillHomeItems() {
        homeItems.add(new HomeItem("Prebook Visitor",R.drawable.prebook_visitor));
        homeItems.add(new HomeItem("Daily Service Provider", R.drawable.service_provider));
        homeItems.add(new HomeItem("My Visitors",R.drawable.my_visitors));
        homeItems.add(new HomeItem("My Prebookings",R.drawable.prebookings));
        homeItems.add(new HomeItem("Community Hall",R.drawable.community_hall));
        homeItems.add(new HomeItem("Nearby Utilities", R.drawable.nearby_utilities));
        homeItems.add(new HomeItem("Directory", R.drawable.directory));
        homeItems.add(new HomeItem("Forum", R.drawable.forum_color));

        SharedPreferences preferences = getActivity().getSharedPreferences("LoginDetails", Context.MODE_PRIVATE);

        if(preferences.getBoolean("isAdmin",false)){
            homeItems.add(new HomeItem("Visitor Statistics",R.drawable.statistics));
            homeItems.add(new HomeItem("Visitor Log",R.drawable.directory));
        }
    }
}
