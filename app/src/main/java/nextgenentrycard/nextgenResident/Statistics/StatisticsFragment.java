package nextgenentrycard.nextgenResident.Statistics;

import android.app.DatePickerDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.DatePicker;
import android.widget.TextView;

import com.androidquery.AQuery;
import com.androidquery.callback.AjaxCallback;
import com.androidquery.callback.AjaxStatus;
import com.github.mikephil.charting.animation.Easing;
import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.components.Description;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;
import com.github.mikephil.charting.data.PieEntry;
import com.github.mikephil.charting.formatter.IValueFormatter;
import com.github.mikephil.charting.utils.ColorTemplate;
import com.github.mikephil.charting.utils.ViewPortHandler;

import org.json.JSONObject;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import nextgenentrycard.nextgenResident.Models.PurposeOfMeetItem;
import nextgenentrycard.nextgenResident.Models.StatisticItem;
import nextgenentrycard.nextgenResident.R;

/**
 * Created by Ashish Pawar(ashishpawar2015.ap@gmail.com) on 16/1/18.
 */

public class StatisticsFragment extends Fragment {
    PieChart pPreBookNewVisitors, pPurpose, pPrePostLunch;
    TextView tvDailyVisitors, tvStartDate, tvEndDate, tvNewVisitors;
    FloatingActionButton fabDone;
    AQuery aQuery;
    PurposeOfMeetItem purposeOfMeetItem;
    StatisticItem statisticItem;

    String getStatistics = "http://www.accesspoint.in/index.php/Admin/getStatistics";

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_statistics, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        pPreBookNewVisitors = (PieChart) view.findViewById(R.id.pPreBookedNewVisitors_fragmentStatistics);
        pPurpose = (PieChart) view.findViewById(R.id.pPurpose_fragmentStatistics);
        pPrePostLunch = (PieChart) view.findViewById(R.id.pPrePostLunch_fragmentStatistics);
        tvDailyVisitors = (TextView) view.findViewById(R.id.tvDailyVisitors_fragmentStatistics);
        tvStartDate = (TextView) view.findViewById(R.id.tvStartDate_fragmentStatistics);
        tvEndDate = (TextView) view.findViewById(R.id.tvEndDate_fragmentStatistics);
        fabDone = (FloatingActionButton) view.findViewById(R.id.fabDone_fragmentStatistics);
        tvNewVisitors = (TextView)view.findViewById(R.id.tvNewVisitors_fragmentStatistics) ;

        Date date1 = new Date();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

        tvStartDate.setText(sdf.format(date1));
        tvEndDate.setText(sdf.format(date1));

        getActivity().setTitle("Statistics");

        /*
            daily visitors
         */
        tvDailyVisitors.setText("-");
        tvNewVisitors.setText("-");


        tvStartDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String dateString = tvStartDate.getText().toString();

                String year = dateString.substring(0, dateString.indexOf("-"));
                String month_date = dateString.substring(dateString.indexOf("-") + 1);
                String month = month_date.substring(0, month_date.indexOf("-"));
                String date = month_date.substring(month_date.indexOf("-") + 1);
                final DatePickerDialog datePickerDialog = new DatePickerDialog(getContext(), new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int i, int i1, int i2) {
                        i1++;
                        if (i2 / 10 != 0 && i1 / 10 != 0)
                            tvStartDate.setText(i + "-" + i1 + "-" + i2);
                        else if (i2 / 10 == 0 && i1 / 10 != 0)
                            tvStartDate.setText(i + "-" + i1 + "-0" + i2);
                        else if (i2 / 10 != 0 && i1 / 10 == 0)
                            tvStartDate.setText(i + "-0" + i1 + "-" + i2);
                        else
                            tvStartDate.setText(i + "-0" + i1 + "-0" + i2);
                    }
                }, Integer.parseInt(year), Integer.parseInt(month) - 1, Integer.parseInt(date));
                datePickerDialog.show();
            }
        });

        tvEndDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String dateString = tvEndDate.getText().toString();

                String year = dateString.substring(0, dateString.indexOf("-"));
                String month_date = dateString.substring(dateString.indexOf("-") + 1);
                String month = month_date.substring(0, month_date.indexOf("-"));
                String date = month_date.substring(month_date.indexOf("-") + 1);
                final DatePickerDialog datePickerDialog = new DatePickerDialog(getContext(), new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int i, int i1, int i2) {
                        i1++;
                        if (i2 / 10 != 0 && i1 / 10 != 0)
                            tvEndDate.setText(i + "-" + i1 + "-" + i2);
                        else if (i2 / 10 == 0 && i1 / 10 != 0)
                            tvEndDate.setText(i + "-" + i1 + "-0" + i2);
                        else if (i2 / 10 != 0 && i1 / 10 == 0)
                            tvEndDate.setText(i + "-0" + i1 + "-" + i2);
                        else
                            tvEndDate.setText(i + "-0" + i1 + "-0" + i2);
                    }
                }, Integer.parseInt(year), Integer.parseInt(month) - 1, Integer.parseInt(date));
                datePickerDialog.show();
            }
        });

        fabDone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                SharedPreferences preferences = getContext().getSharedPreferences("LoginDetails", Context.MODE_PRIVATE);
                String sessionID = preferences.getString("adminSessionID", null);

                HashMap<String, Object> statisticsParams = new HashMap<>();
                statisticsParams.put("sessionID", sessionID);
                statisticsParams.put("startDate", tvStartDate.getText().toString());
                statisticsParams.put("endDate", tvEndDate.getText().toString());

                aQuery = new AQuery(getContext());

                aQuery.ajax(getStatistics, statisticsParams, JSONObject.class, new AjaxCallback<JSONObject>() {
                    @Override
                    public void callback(String url, JSONObject object, AjaxStatus status) {
                        int preBookCount, newCount, dailyCount, vendorCount, guestCount, preLunchCount, postLunchCount, pConference, pGovernment, pInterview, pOfficial, pPersonal, pPromotional, pSocial, pVendor;
                        if (object != null) {
                            try {
                                Log.d("getStatistics", "object != null");
                                preBookCount = object.getInt("prebookCount");
                                newCount = object.getInt("newVisitorCount");
                                dailyCount = object.getInt("dailyVisitorCount");
                                vendorCount = object.getInt("vendors");
                                guestCount = object.getInt("guests");
                                preLunchCount = object.getInt("prelunch");
                                postLunchCount = object.getInt("postlunch");
                                pConference = object.getInt("pConference");
                                pGovernment = object.getInt("pGovernment");
                                pInterview = object.getInt("pInterview");
                                pOfficial = object.getInt("pOfficial");
                                pPersonal = object.getInt("pPersonal");
                                pPromotional = object.getInt("pPromotional");
                                pSocial = object.getInt("pSocial");
                                pVendor = object.getInt("pVendor");

                                purposeOfMeetItem = new PurposeOfMeetItem(pVendor, pConference, pGovernment, pInterview, pOfficial, pPersonal, pPromotional, pSocial);
                                statisticItem = new StatisticItem(dailyCount, preBookCount, newCount, vendorCount, guestCount, preLunchCount, postLunchCount, purposeOfMeetItem);
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        } else {
                            Log.d("getStatistics", "object = null status:" + status.getCode());
                            Snackbar.make(getView(), "Error occurred Try again", Snackbar.LENGTH_SHORT);
                        }

                        if (status.getCode() == 200) {
                            setStatistics(statisticItem);
                        }
                    }
                });
            }
        });
    }

    private void setStatistics(StatisticItem statisticItem) {
        /*
            prebook and new visitors
         */
        List<PieEntry> preBookNewEntries = new ArrayList<>();
        preBookNewEntries.add(new PieEntry(statisticItem.getPreBookedVisitors(), "PreBooked Visitors"));
        preBookNewEntries.add(new PieEntry(statisticItem.getNewVisitors(), "New Visitors"));

        PieDataSet preBookNewDataSet = new PieDataSet(preBookNewEntries, "");
        preBookNewDataSet.setColors(ColorTemplate.VORDIPLOM_COLORS);
        preBookNewDataSet.setValueTextSize(10f);
        preBookNewDataSet.setValueFormatter(new IntegerValueFormatter());


        PieData preBookNewPieData = new PieData(preBookNewDataSet);

        pPreBookNewVisitors.setData(preBookNewPieData);
        pPreBookNewVisitors.animateX(800, Easing.EasingOption.EaseInOutCirc);
        pPreBookNewVisitors.animateY(800, Easing.EasingOption.EaseInOutCirc);
        pPreBookNewVisitors.setEntryLabelColor(R.color.colorAccent);
        pPreBookNewVisitors.setEntryLabelTextSize(10f);
        pPreBookNewVisitors.setRotationEnabled(false);
        pPreBookNewVisitors.invalidate();

        Legend pPreBookNewVisitorsLegend = pPreBookNewVisitors.getLegend();
        pPreBookNewVisitorsLegend.setPosition(Legend.LegendPosition.BELOW_CHART_CENTER);
        pPreBookNewVisitorsLegend.setForm(Legend.LegendForm.CIRCLE);
        pPreBookNewVisitorsLegend.setTextSize(10);
        pPreBookNewVisitorsLegend.setWordWrapEnabled(true);

        /*
            purpose of meet
         */
        List<PieEntry> purposeOfMeetEnteries = new ArrayList<>();
        if (statisticItem.getPurposeOfMeetItem().getConference() != 0)
            purposeOfMeetEnteries.add(new PieEntry(statisticItem.getPurposeOfMeetItem().getConference(), "Conference"));
        if (statisticItem.getPurposeOfMeetItem().getGovernment() != 0)
            purposeOfMeetEnteries.add(new PieEntry(statisticItem.getPurposeOfMeetItem().getGovernment(), "Government"));
        if (statisticItem.getPurposeOfMeetItem().getInterview() != 0)
            purposeOfMeetEnteries.add(new PieEntry(statisticItem.getPurposeOfMeetItem().getInterview(), "Interview"));
        if (statisticItem.getPurposeOfMeetItem().getOfficial() != 0)
            purposeOfMeetEnteries.add(new PieEntry(statisticItem.getPurposeOfMeetItem().getOfficial(), "Official"));
        if (statisticItem.getPurposeOfMeetItem().getPersonal() != 0)
            purposeOfMeetEnteries.add(new PieEntry(statisticItem.getPurposeOfMeetItem().getPersonal(), "Personal"));
        if (statisticItem.getPurposeOfMeetItem().getPromotional() != 0)
            purposeOfMeetEnteries.add(new PieEntry(statisticItem.getPurposeOfMeetItem().getPromotional(), "Promotional"));
        if (statisticItem.getPurposeOfMeetItem().getSocial() != 0)
            purposeOfMeetEnteries.add(new PieEntry(statisticItem.getPurposeOfMeetItem().getSocial(), "Social"));
        if (statisticItem.getPurposeOfMeetItem().getVendor() != 0)
            purposeOfMeetEnteries.add(new PieEntry(statisticItem.getPurposeOfMeetItem().getVendor(), "vendor"));


        PieDataSet purposeOfMeetDataSet = new PieDataSet(purposeOfMeetEnteries, "");
        purposeOfMeetDataSet.setColors(ColorTemplate.VORDIPLOM_COLORS);
        purposeOfMeetDataSet.setValueTextSize(10f);
        purposeOfMeetDataSet.setValueFormatter(new IntegerValueFormatter());

        PieData purposeOfMeetPieData = new PieData(purposeOfMeetDataSet);

        pPurpose.setData(purposeOfMeetPieData);
        pPurpose.animateY(800, Easing.EasingOption.EaseInOutCirc);
        pPurpose.animateX(800, Easing.EasingOption.EaseInOutCirc);
        pPurpose.setEntryLabelColor(R.color.colorAccent);
        pPurpose.setEntryLabelTextSize(10f);
        pPurpose.setRotationEnabled(false);
        pPurpose.invalidate();

        Legend pPurposeLegend = pPurpose.getLegend();
        pPurposeLegend.setEnabled(false);


        /*
            pre post lunch
         */
        List<PieEntry> prePostLunchEntries = new ArrayList<>();
        prePostLunchEntries.add(new PieEntry(statisticItem.getPreLunch(), "Pre-Lunch"));
        prePostLunchEntries.add(new PieEntry(statisticItem.getPostLunch(), "Post-Lunch"));

        PieDataSet prePostLunchDataSet = new PieDataSet(prePostLunchEntries, "");
        prePostLunchDataSet.setColors(ColorTemplate.VORDIPLOM_COLORS);
        prePostLunchDataSet.setValueTextSize(10f);
        prePostLunchDataSet.setValueFormatter(new IntegerValueFormatter());

        PieData prePostLunchPieData = new PieData(prePostLunchDataSet);

        pPrePostLunch.setData(prePostLunchPieData);
        pPrePostLunch.animateX(800, Easing.EasingOption.EaseInOutCirc);
        pPrePostLunch.animateY(800, Easing.EasingOption.EaseInOutCirc);
        pPrePostLunch.setEntryLabelColor(R.color.colorAccent);
        pPrePostLunch.setEntryLabelTextSize(10f);
        pPrePostLunch.setRotationEnabled(false);
        pPrePostLunch.invalidate();


        Legend pPrePostLunchLegend = pPrePostLunch.getLegend();
        pPrePostLunchLegend.setPosition(Legend.LegendPosition.BELOW_CHART_CENTER);
        pPrePostLunchLegend.setForm(Legend.LegendForm.CIRCLE);
        pPrePostLunchLegend.setTextSize(10);
        pPrePostLunchLegend.setWordWrapEnabled(true);

        /*
            hiding description text
         */
        Description description = new Description();
        description.setText("");
        pPurpose.setDescription(description);
        pPreBookNewVisitors.setDescription(description);
        pPrePostLunch.setDescription(description);

        /*
            daily visitors
         */
        tvDailyVisitors.setText(statisticItem.getDailyVisitors()+"");

        tvNewVisitors.setText(String.valueOf(statisticItem.getNewVisitors()+statisticItem.getPreBookedVisitors()));
    }

    class IntegerValueFormatter implements IValueFormatter {
        private DecimalFormat mFormat;

        public IntegerValueFormatter() {
            mFormat = new DecimalFormat("###,###,##0");
        }


        @Override
        public String getFormattedValue(float value, Entry entry, int dataSetIndex, ViewPortHandler viewPortHandler) {
            return mFormat.format(value);
        }

    }
}
