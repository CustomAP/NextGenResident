package nextgenentrycard.nextgenResident;

import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.Toast;

import nextgenentrycard.nextgenResident.Models.PreBookItem;
import nextgenentrycard.nextgenResident.MyVisitors.MyVisitorsFragment;
import nextgenentrycard.nextgenResident.PreBook.PreBookFragment;
import nextgenentrycard.nextgenResident.PreBook.PreBookServerConnection;
import nextgenentrycard.nextgenResident.PreBookings.PreBookingFragment;
import nextgenentrycard.nextgenResident.Profile.ProfileFragment;
import nextgenentrycard.nextgenResident.Sync.NewVisitorSync;

public class MainActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {
    private static FragmentManager fragmentManager;
    FragmentTransaction fragmentTransaction;
    private boolean exit = false;
    Handler mHandler = new Handler();
    final int INTERVAL = 1000 * 30;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        /*
            Drawer
         */
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        /*
            Navigation view
         */
        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        navigationView.setCheckedItem(R.id.nav_prebook);

        setTitle("PreBook Visitor");

        /*
            adding prebook fragment to activity
         */
        PreBookFragment preBookFragment = new PreBookFragment();
        Bundle args = new Bundle();
        args.putSerializable("PreBookItem", new PreBookItem("", "", "", "", "", "", "", "", "Guest", 0, 0));
        preBookFragment.setArguments(args);

        fragmentManager = getSupportFragmentManager();
        fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.add(R.id.main_container, preBookFragment);
        fragmentTransaction.commit();

        mHandlerTask.run();

    }

    @Override
    public void onBackPressed() {
       /*
            closing the drawer if open
         */
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            if (exit) {
            /*
                finish activity
             */
                finish();
            } else {
                Toast.makeText(getApplicationContext(), "Press Back again to exit", Toast.LENGTH_SHORT).show();
                exit = true;
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        exit = false;
                    }
                }, 2300);
            }
        }
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.nav_prebook) {

            setTitle("Pre-Book Visitor");
            PreBookFragment preBookFragment = new PreBookFragment();
            Bundle args = new Bundle();
            args.putSerializable("PreBookItem", new PreBookItem("", "", "", "", "", "", "", "", "Guest", 0, 0));
            preBookFragment.setArguments(args);
            replaceFragment(preBookFragment);

        } else if (id == R.id.nav_prebookings) {

            setTitle("Pre-Bookings");
            replaceFragment(new PreBookingFragment());

        } else if (id == R.id.nav_myVisitors) {

            setTitle("My Visitors");
            replaceFragment(new MyVisitorsFragment());

        } else if (id == R.id.nav_profile) {

            setTitle("Profile");
            replaceFragment(new ProfileFragment());

        } else if(id==R.id.nav_about){

        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    private void replaceFragment(Fragment fragment) {
        fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.main_container, fragment);
        fragmentTransaction.commit();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mHandler.removeCallbacks(mHandlerTask);
    }

    Runnable mHandlerTask = new Runnable() {
        @Override
        public void run() {

            NewVisitorSync newVisitorSync = new NewVisitorSync(getApplicationContext());
            newVisitorSync.fetchNewVisitors();

            PreBookServerConnection preBookServerConnection = new PreBookServerConnection(getApplicationContext());
            preBookServerConnection.getPreBookIDsToDelete();

            mHandler.postDelayed(mHandlerTask, INTERVAL);
        }
    };
}
