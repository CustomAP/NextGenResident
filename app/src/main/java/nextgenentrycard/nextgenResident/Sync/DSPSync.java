package nextgenentrycard.nextgenResident.Sync;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import com.androidquery.AQuery;
import com.androidquery.callback.AjaxCallback;
import com.androidquery.callback.AjaxStatus;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

import nextgenentrycard.nextgenResident.DbHelper.DbHelper;
import nextgenentrycard.nextgenResident.Models.DSPItem;

/**
 * Created by Ashish Pawar(ashishpawar2015.ap@gmail.com) on 11/3/18.
 */

public class DSPSync {
    private AQuery aQuery;
    private Context context;
    private ArrayList<DSPItem> dspItems;

    String getDSPs = "http://www.accesspoint.in/index.php/DSP/getDSP";

    public DSPSync(Context context){
        this.context = context;
        aQuery = new AQuery(context);
    }

    public void getDSPs(){
        dspItems = new ArrayList<>();

        final SharedPreferences preferences = context.getSharedPreferences("DSP", Context.MODE_PRIVATE);
        int IDMax = preferences.getInt("IDMax", 0);

        SharedPreferences preferences1 = context.getSharedPreferences("LoginDetails", Context.MODE_PRIVATE);
        String sessionID = preferences1.getString("sessionID", null);

        HashMap<String, Object> dspParams = new HashMap<>();
        dspParams.put("sessionID", sessionID);
        dspParams.put("IDMax", IDMax);

        aQuery.ajax(getDSPs, dspParams, JSONObject.class, new AjaxCallback<JSONObject>(){
            String dspName, dsp, mobNum;
            int dspID, num;
            @Override
            public void callback(String url, JSONObject object, AjaxStatus status) {
                if(object != null){
                    try{
                        Log.d("getDSP", "object != null");

                        dsp = object.getString("dsp");
                        num = object.getInt("num");

                        JSONObject reader = new JSONObject(dsp);

                        for(int i = 0 ; i < num; i++){
                            JSONObject obj = reader.getJSONObject(String.valueOf(i));

                            dspName = obj.getString("name");
                            dspID = obj.getInt("dspID");
                            mobNum = obj.getString("mobNum");

                            dspItems.add(new DSPItem(dspName, dspID, mobNum));
                        }

                    }catch (Exception e){
                        e.printStackTrace();
                    }
                }else{
                    Log.d("getDSP", "object = null status:" + status.getCode());
                }

                if(status.getCode() == 200){
                    DbHelper dbHelper = new DbHelper(context);

                    for(int i = 0; i < dspItems.size(); i++)
                        dbHelper.addDSP(dspItems.get(i));

                    if(num > 0) {
                        SharedPreferences.Editor editor = preferences.edit();
                        editor.putInt("IDMax", dspItems.get(num - 1).getDspID());
                        editor.apply();
                    }
                }
            }
        });
    }
}
