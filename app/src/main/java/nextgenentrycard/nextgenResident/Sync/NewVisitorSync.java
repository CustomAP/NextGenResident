package nextgenentrycard.nextgenResident.Sync;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import com.androidquery.AQuery;
import com.androidquery.callback.AjaxCallback;
import com.androidquery.callback.AjaxStatus;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

import nextgenentrycard.nextgenResident.DbHelper.DbHelper;
import nextgenentrycard.nextgenResident.Models.NewVisitorItem;

/**
 * Created by Ashish Pawar(ashishpawar2015.ap@gmail.com) on 27/10/17.
 */

public class NewVisitorSync {
    private AQuery aQuery;
    private Context context;
    private ArrayList<NewVisitorItem> newVisitorItems;

    String fetchNewVisitors = "http://www.accesspoint.in/index.php/NewVisitor/fetchNewVisitors";

    public NewVisitorSync(Context context) {
        this.context = context;
        aQuery = new AQuery(context);
    }

    public void fetchNewVisitors() {

        newVisitorItems = new ArrayList<>();

        final SharedPreferences preferences = context.getSharedPreferences("LoginDetails", Context.MODE_PRIVATE);
        String sessionID = preferences.getString("sessionID", null);

        final SharedPreferences preferences1 = context.getSharedPreferences("NewVisitor", Context.MODE_PRIVATE);
        int IDMax = preferences1.getInt("IDMax", 0);

        HashMap<String, Object> newVisitorParams = new HashMap<>();
        newVisitorParams.put("sessionID", sessionID);
        newVisitorParams.put("IDMax", IDMax);

        Log.d("sessionID",sessionID);

        aQuery.ajax(fetchNewVisitors, newVisitorParams, JSONObject.class, new AjaxCallback<JSONObject>() {
            @Override
            public void callback(String url, JSONObject object, AjaxStatus status) {
                String newVisitors;
                int num = 0, exited;

                String date, time, purposeOfMeet, organisation, IDProof, IDNum, visitorName, type, mobNum;
                int newVisitorID;

                if (object != null) {
                    try {
                        Log.d("newVisitors", "object!=null");
                        newVisitors = object.getString("newVisitors");

                        num = object.getInt("num");

                        JSONObject reader = new JSONObject(newVisitors);

                        for (int i = 0; i < num; i++) {
                            JSONObject obj = reader.getJSONObject(String.valueOf(i));
                            date = obj.getString("date");
                            time = obj.getString("inTime");
                            purposeOfMeet = obj.getString("purposeOfMeet");
                            organisation = obj.getString("organisation");
                            IDProof = obj.getString("IDproof");
                            IDNum = obj.getString("IDNo");
                            visitorName = obj.getString("visitorName");
                            type = obj.getString("type");
                            mobNum = obj.getString("mobNum");
                            newVisitorID = obj.getInt("newVisitorID");
                            exited = obj.getInt("exited");

                            newVisitorItems.add(new NewVisitorItem(mobNum, visitorName, purposeOfMeet, organisation, IDProof, IDNum, type, time, "", newVisitorID, date,exited));
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else {
                    Log.d("newVisitors", "object=null");
                }

                if (status.getCode() == 200) {

                    if (num > 0) {
                        SharedPreferences.Editor editor = preferences1.edit();
                        editor.putInt("IDMax", newVisitorItems.get(num - 1).getVisitorID());
                        editor.apply();
                    }

                    DbHelper dbHelper = new DbHelper(context);
                    for (int i = 0; i < num; i++)
                        dbHelper.addVisitor(newVisitorItems.get(i));
                }
            }
        });
    }
}
