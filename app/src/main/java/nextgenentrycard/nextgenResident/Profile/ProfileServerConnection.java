package nextgenentrycard.nextgenResident.Profile;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;
import android.widget.Toast;

import com.androidquery.AQuery;
import com.androidquery.callback.AjaxCallback;
import com.androidquery.callback.AjaxStatus;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import de.greenrobot.event.EventBus;
import nextgenentrycard.nextgenResident.EventBus.ProfilePicEvent;

/**
 * Created by Ashish Pawar(ashishpawar2015.ap@gmail.com) on 28/10/17.
 */

public class ProfileServerConnection {
    private Context context;
    private AQuery aQuery;

    String changeProfPic="http://www.accesspoint.in/index.php/Login/changeProfilePic";

    public ProfileServerConnection(Context context){
        this.context=context;
        aQuery=new AQuery(context);
    }

    public void changeProfilePic(String image){

        SharedPreferences preferences=context.getSharedPreferences("LoginDetails", Context.MODE_PRIVATE);
        String sessionID=preferences.getString("sessionID",null);

        Map<String,Object> profilePicParams=new HashMap<>();
        profilePicParams.put("image",image);
        profilePicParams.put("sessionID",sessionID);

        aQuery.ajax(changeProfPic,profilePicParams,JSONObject.class,new AjaxCallback<JSONObject>(){
            @Override
            public void callback(String url, JSONObject object, AjaxStatus status) {
                int code=0;
                if(object!=null){
                    try{
                        Log.d("changeProfPic","object!=null");
                        code=object.getInt("ResultSet");
                    }catch (Exception e){
                        e.printStackTrace();
                    }

                }else{
                    Log.d("changeProfPic","object=null");
                    Toast.makeText(context,"Failed to update Profile Pic Try again later",Toast.LENGTH_SHORT).show();
                    EventBus.getDefault().post(new ProfilePicEvent(false));
                }

                if(status.getCode()==200){
                    if(code==0){
                        EventBus.getDefault().post(new ProfilePicEvent(true));
                    }
                }
            }
        });

    }

}
