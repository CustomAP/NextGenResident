package nextgenentrycard.nextgenResident.Profile;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.graphics.drawable.RoundedBitmapDrawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.target.BitmapImageViewTarget;
import com.github.siyamed.shapeimageview.CircularImageView;

import java.io.File;

import nextgenentrycard.nextgenResident.R;

/**
 * Created by Ashish Pawar(ashishpawar2015.ap@gmail.com) on 28/10/17.
 */

public class ProfileFragment extends Fragment {
    TextView tvName,tvMobileNum,tvCompany,tvDepartment;
    CircularImageView ivPhoto;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_profile,container,false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        tvName=(TextView)view.findViewById(R.id.tvName_Profile);
        tvMobileNum=(TextView)view.findViewById(R.id.tvMobileNum_Profile);
        tvCompany=(TextView)view.findViewById(R.id.tvCompanyName_Profile);
        tvDepartment=(TextView)view.findViewById(R.id.tvDepartmentName_Profile);
        ivPhoto=(CircularImageView)view.findViewById(R.id.ivPhoto_Profile);

        SharedPreferences preferences=getContext().getSharedPreferences("LoginDetails", Context.MODE_PRIVATE);
        String name=preferences.getString("residentName","");
        String mobileNum=preferences.getString("mobileNum","");
        String apartmentName=preferences.getString("apartmentName","");
        String wing=preferences.getString("wing","");
        String flatNum = preferences.getString("flatNum", "");

        tvName.setText(name);
        tvDepartment.setText(wing + " " +flatNum);
        tvMobileNum.setText(mobileNum);
        tvCompany.setText(apartmentName);

        ivPhoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getContext(),ProfilePicDialog.class);
                startActivity(i);
            }
        });

        loadImageFromStorage();
    }


    @Override
    public void onResume() {
        super.onResume();
        loadImageFromStorage();
    }

    public void loadImageFromStorage(){
        try {
            SharedPreferences  preferences=getContext().getSharedPreferences("LoginDetails", Context.MODE_PRIVATE);
            String path = preferences.getString("profilePic", null);
            File f = new File(path, "profilePic.jpg");
            Drawable defaultProfPic=getResources().getDrawable(R.drawable.generic_profile);
            Glide.with(this).load(f).asBitmap().centerCrop().error(defaultProfPic).diskCacheStrategy(DiskCacheStrategy.NONE)
                    .skipMemoryCache(true).into(new BitmapImageViewTarget(ivPhoto) {
                @Override
                protected void setResource(Bitmap resource) {
                    RoundedBitmapDrawable circularBitmapDrawable =
                            RoundedBitmapDrawableFactory.create(getContext().getResources(), resource);
                    circularBitmapDrawable.setCircular(true);
                    ivPhoto.setImageDrawable(circularBitmapDrawable);
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
