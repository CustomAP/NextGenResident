package nextgenentrycard.nextgenResident.DbHelper;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.text.TextUtils;

import java.util.ArrayList;

import nextgenentrycard.nextgenResident.Models.DSPItem;
import nextgenentrycard.nextgenResident.Models.DSPLinkItem;
import nextgenentrycard.nextgenResident.Models.NewVisitorItem;
import nextgenentrycard.nextgenResident.Models.PreBookItem;
import nextgenentrycard.nextgenResident.Models.RoomItem;

/**
 * Created by Ashish Pawar(ashishpawar2015.ap@gmail.com) on 26/10/17.
 */

public class DbHelper extends SQLiteOpenHelper {

    private static final String DBName = "Resident";
    private static final int DBVersion = 1;

    private static final String prebookTable = "PreBook";
    private static final String newVisitorTable = "NewVisitor";
    private static final String roomTable = "rooms";
    private static final String dspTable = "dsp";
    private static final String dspLinkTable = "dsplink";

    private static final String column_prebookID = "prebookID";
    private static final String column_visitorID = "visitorID";
    private static final String column_inTime = "inTime";
    private static final String column_outTime = "outTime";
    private static final String column_purposeOfMeet = "purposeOfMeet";
    private static final String column_IDProof = "IDProof";
    private static final String column_IDNum = "IDNum";
    private static final String column_visitorName = "visitorName";
    private static final String column_prebookTime = "prebookTime";
    private static final String column_organisation = "organisation";
    private static final String column_type = "type";
    private static final String column_mobNum = "mobileNum";
    private static final String column_date = "date";
    private static final String column_exited = "exited";
    private static final String column_roomID = "roomID";
    private static final String column_roomName = "roomName";
    private static final String column_roomDescription = "roomDescription";
    private static final String column_dspID = "dspID";
    private static final String column_dspName = "dspName";
    private static final String column_dspLinkID = "dspLinkID";
    private static final String column_work = "work";

    public DbHelper(Context context) {
        super(context, DBName, null, DBVersion);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String createNewVisitorTable = "CREATE TABLE " + newVisitorTable + " ("
                + column_visitorID + " INTEGER PRIMARY KEY, "
                + column_visitorName + " VARCHAR(255), "
                + column_inTime + " TIME, "
                + column_outTime + " TIME, "
                + column_purposeOfMeet + " VARCHAR(255), "
                + column_IDProof + " VARCHAR(255), "
                + column_IDNum + " VARCHAR(255), "
                + column_organisation + " VARCHAR(255), "
                + column_date + " DATE, "
                + column_type + " VARCHAR(255), "
                + column_exited + " INT, "
                + column_mobNum + " VARCHAR(255));";

        String createPreBookTable = "CREATE TABLE " + prebookTable + " ("
                + column_prebookID + " INTEGER PRIMARY KEY, "
                + column_visitorName + " VARCHAR(255), "
                + column_prebookTime + " TIME, "
                + column_date + " DATE, "
                + column_purposeOfMeet + " VARCHAR(255), "
                + column_organisation + " VARCHAR(255), "
                + column_IDProof + " VARCHAR(255), "
                + column_IDNum + " VARCHAR(255), "
                + column_type + " VARCHAR(255), "
                + column_mobNum + " VARCHAR(255));";

        String createRoomsTable = "CREATE TABLE " + roomTable + " ("
                + column_roomID + " INTEGER PRIMARY KEY, "
                + column_roomName + " VARCHAR(255), "
                + column_roomDescription + " VARCHAR(255));";

        String createDSPTable = "CREATE TABLE " + dspTable + " ("
                + column_dspID + " INTEGER PRIMARY KEY, "
                + column_mobNum + " VARCHAR(255), "
                + column_dspName + " VARCHAR(255));";

        String createDSPLinkTable = "CREATE TABLE " + dspLinkTable + " ("
                + column_dspLinkID + " INTEGER PRIMARY KEY, "
                + column_dspName + " VARCHAR(255), "
                + column_work + " TEXT, "
                + column_dspID + " INT);";

        try {
            db.execSQL(createNewVisitorTable);
            db.execSQL(createPreBookTable);
            db.execSQL(createRoomsTable);
            db.execSQL(createDSPTable);
            db.execSQL(createDSPLinkTable);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        String dropNewVisitorTable = "DROP TABLE IF EXISTS " + newVisitorTable;
        String dropPrebookTable = "DROP TABLE IF EXISTS " + prebookTable;

        try {
            db.execSQL(dropNewVisitorTable);
            db.execSQL(dropPrebookTable);
            onCreate(db);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public void insertPreBookVisitor(PreBookItem preBookItem) {
        SQLiteDatabase database = this.getWritableDatabase();

        ContentValues contentValues = new ContentValues();
        contentValues.put(column_prebookID, preBookItem.getPrebookID());
        contentValues.put(column_visitorName, preBookItem.getVisitorName());
        contentValues.put(column_prebookTime, preBookItem.getTime());
        contentValues.put(column_purposeOfMeet, preBookItem.getPurposeOfMeet());
        contentValues.put(column_organisation, preBookItem.getOrganisation());
        contentValues.put(column_date, preBookItem.getDate());
        contentValues.put(column_IDProof, preBookItem.getIDProof());
        contentValues.put(column_IDNum, preBookItem.getIDNum());
        contentValues.put(column_type, preBookItem.getType());
        contentValues.put(column_mobNum, preBookItem.getMobNum());

        try {
            database.insert(prebookTable, null, contentValues);
        } catch (Exception e) {
            e.printStackTrace();
        }

        database.close();
    }

    public ArrayList<PreBookItem> getAllPreBookVisitors() {
        ArrayList<PreBookItem> preBookItems = new ArrayList<>();

        SQLiteDatabase database = this.getWritableDatabase();

        String selectQuery = "SELECT * FROM " + prebookTable;

        Cursor cursor = database.rawQuery(selectQuery, null);

        if (cursor.moveToFirst()) {
            do {
                preBookItems.add(new PreBookItem(cursor.getString(cursor.getColumnIndex(column_visitorName)),
                        cursor.getString(cursor.getColumnIndex(column_purposeOfMeet)),
                        cursor.getString(cursor.getColumnIndex(column_organisation)),
                        cursor.getString(cursor.getColumnIndex(column_date)),
                        cursor.getString(cursor.getColumnIndex(column_prebookTime)),
                        cursor.getString(cursor.getColumnIndex(column_IDProof)),
                        cursor.getString(cursor.getColumnIndex(column_IDNum)),
                        cursor.getString(cursor.getColumnIndex(column_mobNum)),
                        cursor.getString(cursor.getColumnIndex(column_type)),
                        cursor.getInt(cursor.getColumnIndex(column_prebookID)),
                        0
                ));
            } while (cursor.moveToNext());
        }

        cursor.close();

        database.close();

        return preBookItems;
    }

    public void addVisitor(NewVisitorItem newVisitorItem) {
        SQLiteDatabase database = this.getWritableDatabase();

        ContentValues contentValues = new ContentValues();

        contentValues.put(column_visitorID, newVisitorItem.getVisitorID());
        contentValues.put(column_visitorName, newVisitorItem.getName());
        contentValues.put(column_inTime, newVisitorItem.getInTime());
        contentValues.put(column_purposeOfMeet, newVisitorItem.getPurposeOfMeet());
        contentValues.put(column_IDProof, newVisitorItem.getGovtID());
        contentValues.put(column_IDNum, newVisitorItem.getIDNum());
        contentValues.put(column_organisation, newVisitorItem.getOrganisation());
        contentValues.put(column_type, newVisitorItem.getType());
        contentValues.put(column_outTime, newVisitorItem.getOutTime());
        contentValues.put(column_mobNum, newVisitorItem.getMobNum());
        contentValues.put(column_date, newVisitorItem.getDate());
        contentValues.put(column_exited, newVisitorItem.getExited());

        try {
            database.insert(newVisitorTable, null, contentValues);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public ArrayList<NewVisitorItem> getAllNewVisitors() {
        ArrayList<NewVisitorItem> newVisitorItems = new ArrayList<>();

        SQLiteDatabase database = this.getWritableDatabase();

        String selectQuery = "SELECT * FROM " + newVisitorTable;

        Cursor cursor = database.rawQuery(selectQuery, null);

        if (cursor.moveToFirst()) {
            do {
                newVisitorItems.add(new NewVisitorItem(
                        cursor.getString(cursor.getColumnIndex(column_mobNum)),
                        cursor.getString(cursor.getColumnIndex(column_visitorName)),
                        cursor.getString(cursor.getColumnIndex(column_purposeOfMeet)),
                        cursor.getString(cursor.getColumnIndex(column_organisation)),
                        cursor.getString(cursor.getColumnIndex(column_IDProof)),
                        cursor.getString(cursor.getColumnIndex(column_IDNum)),
                        cursor.getString(cursor.getColumnIndex(column_type)),
                        cursor.getString(cursor.getColumnIndex(column_inTime)),
                        cursor.getString(cursor.getColumnIndex(column_outTime)),
                        cursor.getInt(cursor.getColumnIndex(column_visitorID)),
                        cursor.getString(cursor.getColumnIndex(column_date)),
                        cursor.getInt(cursor.getColumnIndex(column_exited))
                ));
            } while (cursor.moveToNext());
        }

        database.close();
        cursor.close();

        return newVisitorItems;
    }

    public void deletePreBookItem(ArrayList<Integer> ids) {
        String args = TextUtils.join(", ", ids);

        SQLiteDatabase database = this.getWritableDatabase();


        String deleteQuery = String.format("DELETE FROM " + prebookTable + " WHERE NOT " + column_prebookID + " IN (%s);", args);

        database.execSQL(deleteQuery);

        database.close();
    }

    public void exitVisitor(int visitorID) {
        SQLiteDatabase database = this.getWritableDatabase();

        String exitVisitor = "UPDATE " + newVisitorTable + " SET " + column_exited + " = 1 " + " WHERE " + column_visitorID + " = " + visitorID;

        try {
            database.execSQL(exitVisitor);
        } catch (Exception e) {
            e.printStackTrace();
        }

        database.close();
    }

    public int getExited(int visitorID) {
        SQLiteDatabase database = this.getWritableDatabase();

        int exited = 0;

        String selectQuery = "SELECT " + column_exited + " FROM " + newVisitorTable + " WHERE " + column_visitorID + " = " + visitorID;

        Cursor cursor = database.rawQuery(selectQuery, null);

        if (cursor.moveToFirst()) {
            exited = cursor.getInt(cursor.getColumnIndex(column_exited));
        }

        database.close();
        return exited;
    }

    public void addRoom(RoomItem roomItem) {
        SQLiteDatabase database = this.getWritableDatabase();

        ContentValues contentValues = new ContentValues();
        contentValues.put(column_roomID, roomItem.getRoomID());
        contentValues.put(column_roomName, roomItem.getName());
        contentValues.put(column_roomDescription, roomItem.getDescription());

        try {
            database.insert(roomTable, null, contentValues);
        } catch (Exception e) {
            e.printStackTrace();
        }

        database.close();
    }

    public ArrayList<RoomItem> getRooms() {
        ArrayList<RoomItem> roomItems = new ArrayList<>();

        SQLiteDatabase database = this.getWritableDatabase();

        String selectQuery = "SELECT * FROM " + roomTable;

        Cursor cursor = database.rawQuery(selectQuery, null);

        if (cursor.moveToFirst()) {
            do {
                roomItems.add(new RoomItem(
                        cursor.getInt(cursor.getColumnIndex(column_roomID)),
                        cursor.getString(cursor.getColumnIndex(column_roomName)),
                        cursor.getString(cursor.getColumnIndex(column_roomDescription))
                        ));
            } while (cursor.moveToNext());
        }

        cursor.close();
        database.close();

        return roomItems;
    }

    public void addDSP(DSPItem dspItem){
        SQLiteDatabase database = this.getWritableDatabase();

        ContentValues contentValues = new ContentValues();
        contentValues.put(column_dspID, dspItem.getDspID());
        contentValues.put(column_dspName, dspItem.getDspName());
        contentValues.put(column_mobNum, dspItem.getMobNum());

        try{
            database.insert(dspTable, null, contentValues);
        }catch (Exception e){
            e.printStackTrace();
        }

        database.close();
    }

    public ArrayList<DSPItem> getDSPItems(){
        ArrayList<DSPItem> dspItems = new ArrayList<>();

        SQLiteDatabase database = this.getWritableDatabase();

        String selectQuery = "SELECT * FROM " + dspTable;

        Cursor cursor = database.rawQuery(selectQuery, null);

        if(cursor.moveToFirst()){
            do{
                dspItems.add(new DSPItem(cursor.getString(cursor.getColumnIndex(column_dspName)),
                        cursor.getInt(cursor.getColumnIndex(column_dspID)),
                        cursor.getString(cursor.getColumnIndex(column_mobNum))
                ));
            }while (cursor.moveToNext());
        }

        database.close();
        cursor.close();

        return dspItems;
    }

    public DSPItem getDSPItem(int dspID){
        SQLiteDatabase database = this.getWritableDatabase();

        String selectQuery = "SELECT * FROM " + dspTable + " WHERE " + column_dspID + " = " + dspID;

        Cursor cursor = database.rawQuery(selectQuery, null);

        if(cursor.moveToFirst()){
            DSPItem dspItem = new DSPItem(
                    cursor.getString(cursor.getColumnIndex(column_dspName)),
                    cursor.getInt(cursor.getColumnIndex(column_dspID)),
                    cursor.getString(cursor.getColumnIndex(column_mobNum))
            );

            cursor.close();
            database.close();
            return dspItem;
        }

        return null;
    }

    public void addDSPLink(DSPLinkItem dspLinkItem){
        SQLiteDatabase database = this.getWritableDatabase();

        ContentValues contentValues = new ContentValues();
        contentValues.put(column_dspLinkID, dspLinkItem.getDspLinkID());
        contentValues.put(column_dspName, dspLinkItem.getDspName());
        contentValues.put(column_dspID, dspLinkItem.getDspID());
        contentValues.put(column_work, dspLinkItem.getWork());

        try{
            database.insert(dspLinkTable, null, contentValues);
        }catch (Exception e){
            e.printStackTrace();
        }

        database.close();
    }

    public ArrayList<DSPLinkItem> getDSPLinkItems(){
        SQLiteDatabase database = this.getWritableDatabase();

        ArrayList<DSPLinkItem> dspLinkItems = new ArrayList<>();

        String selectQuery = "SELECT * FROM " + dspLinkTable;

        Cursor cursor = database.rawQuery(selectQuery, null);

        if(cursor.moveToFirst()){
            do{
                dspLinkItems.add(new DSPLinkItem(cursor.getInt(cursor.getColumnIndex(column_dspLinkID)),
                        cursor.getInt(cursor.getColumnIndex(column_dspID)),
                        cursor.getString(cursor.getColumnIndex(column_dspName)),
                        cursor.getString(cursor.getColumnIndex(column_work))
                ));
            }while (cursor.moveToNext());
        }

        cursor.close();
        database.close();

        return dspLinkItems;
    }

}
