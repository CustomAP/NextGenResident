package nextgenentrycard.nextgenResident.Directory;

import android.app.Dialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.androidquery.AQuery;
import com.androidquery.callback.AjaxCallback;
import com.androidquery.callback.AjaxStatus;
import com.rengwuxian.materialedittext.MaterialEditText;
import com.yarolegovich.lovelydialog.LovelyCustomDialog;
import com.yarolegovich.lovelydialog.LovelyProgressDialog;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

import nextgenentrycard.nextgenResident.Models.DirectoryItem;
import nextgenentrycard.nextgenResident.R;

/**
 * Created by Ashish Pawar(ashishpawar2015.ap@gmail.com) on 18/3/18.
 */

public class DirectoryActivity extends AppCompatActivity {
    RecyclerView rvDirectory;
    FloatingActionButton fabAddDirectory;

    AQuery aQuery;
    Dialog directoryProgress, dialog;
    MaterialEditText etName, etMobileNum, etWing, etFlatNum;

    private Context context;

    ArrayList<DirectoryItem> directoryItems;

    DirectoryAdapter directoryAdapter;

    String getDirectory = "http://www.accesspoint.in/index.php/PhoneDirectory/getDirectory";
    String addDirectory ="http://www.accesspoint.in/index.php/PhoneDirectory/addDirectory";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_directory);
        setTitle("Directory");
        context = this;
        initialize();
    }

    private void initialize() {
        rvDirectory = (RecyclerView)findViewById(R.id.rvDirectory);
        fabAddDirectory = (FloatingActionButton)findViewById(R.id.bAddDirectory_directory);

        directoryItems = new ArrayList<>();
        aQuery = new AQuery(getApplicationContext());

        directoryProgress = new LovelyProgressDialog(context)
                .setTopColorRes(R.color.colorPrimary)
                .setIcon(R.drawable.wait)
                .setTitle("Getting Directory")
                .show();

        final SharedPreferences sharedPreferences = getSharedPreferences("LoginDetails", Context.MODE_PRIVATE);
        String sessionID = sharedPreferences.getString("sessionID", null);

        HashMap<String, Object> directoryParams = new HashMap<>();
        directoryParams.put("sessionID", sessionID);

        aQuery.ajax(getDirectory, directoryParams, JSONObject.class, new AjaxCallback<JSONObject>(){
            @Override
            public void callback(String url, JSONObject object, AjaxStatus status) {
                String directory;
                String wing, flatNum, name, mobNum;
                int num, directoryID;

                if(object != null){
                    try {
                        Log.d("getDirectory", " object != null:");
                        directory = object.getString("directory");
                        num = object.getInt("num");

                        JSONObject reader = new JSONObject(directory);

                        for(int i = 0;i < num; i++){
                            JSONObject obj = reader.getJSONObject(String.valueOf(i));

                            wing = obj.getString("wing");
                            flatNum = obj.getString("flatNum");
                            name = obj.getString("name");
                            mobNum = obj.getString("mobNum");
                            directoryID = obj.getInt("directoryID");

                            directoryItems.add(new DirectoryItem(directoryID, mobNum, name, wing, flatNum));
                        }
                    }catch (Exception e){
                        e.printStackTrace();
                    }
                }else{
                    Log.d("getDirectory", " object = null status:" +status.getCode());
                    Toast.makeText(getApplicationContext(), "Failed to get Directory",Toast.LENGTH_SHORT).show();
                    directoryProgress.dismiss();
                }

                if(status.getCode() == 200){
                    directoryAdapter = new DirectoryAdapter(directoryItems);
                    rvDirectory.setAdapter(directoryAdapter);
                    rvDirectory.setItemAnimator(new DefaultItemAnimator());
                    rvDirectory.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
                    directoryProgress.dismiss();
                }
            }
        });

        Boolean isAdmin = sharedPreferences.getBoolean("isAdmin", false);

        if(isAdmin){
            fabAddDirectory.setVisibility(View.VISIBLE);
            fabAddDirectory.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog = new LovelyCustomDialog(context)
                            .setView(R.layout.fragment_add_directory)
                            .setTopColorRes(R.color.colorPrimary)
                            .setIcon(R.drawable.person)
                            .configureView(new LovelyCustomDialog.ViewConfigurator() {
                                @Override
                                public void configureView(View v) {
                                    etName = (MaterialEditText)v.findViewById(R.id.etName_fragmentAddDirectory);
                                    etFlatNum = (MaterialEditText)v.findViewById(R.id.etFlatNum_fragmentAddDirectory);
                                    etMobileNum = (MaterialEditText)v.findViewById(R.id.etMobileNum_fragmentAddDirectory);
                                    etWing = (MaterialEditText)v.findViewById(R.id.etWing_fragmentAddDirectory);
                                }
                            })
                            .setListener(R.id.tvAdd_fragmentAddDirectory, new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    final String name = etName.getText().toString();
                                    final String flatNum = etFlatNum.getText().toString();
                                    final String wing = etWing.getText().toString();
                                    final String mobileNum = etMobileNum.getText().toString();

                                    if(!name.equals("")){
                                        if(!wing.equals("")){
                                            if(!flatNum.equals("")){
                                                if(!mobileNum.equals("")){
                                                    String adminSessionID = sharedPreferences.getString("adminSessionID", null);

                                                    HashMap<String, Object> directoryParams = new HashMap<>();
                                                    directoryParams.put("adminSessionID", adminSessionID);
                                                    directoryParams.put("name", name);
                                                    directoryParams.put("wing", wing);
                                                    directoryParams.put("mobileNum", mobileNum);
                                                    directoryParams.put("flatNum", flatNum);

                                                    aQuery.ajax(addDirectory, directoryParams, JSONObject.class, new AjaxCallback<JSONObject>(){
                                                        @Override
                                                        public void callback(String url, JSONObject object, AjaxStatus status) {
                                                            int directoryID = 0;
                                                            if(object != null){
                                                                try {
                                                                    Log.d("addDirectory", "object != null");
                                                                    directoryID = object.getInt("directoryID");
                                                                } catch (JSONException e) {
                                                                    e.printStackTrace();
                                                                }
                                                            }else{
                                                                Log.d("addDirectory", "object = null status:" +status.getCode());
                                                            }

                                                            if(status.getCode() == 200){
                                                                Toast.makeText(getApplicationContext(), "Number added to directory", Toast.LENGTH_SHORT).show();
                                                                DirectoryItem directoryItem = new DirectoryItem(directoryID, mobileNum, name, wing, flatNum);
                                                                directoryItems.add(directoryItem);
                                                                directoryAdapter.update(directoryItems);
                                                                dialog.dismiss();
                                                            }
                                                        }
                                                    });
                                                }else{
                                                    Toast.makeText(getApplicationContext(), "Mobile Number cannot be empty", Toast.LENGTH_SHORT).show();
                                                }
                                            }else{
                                                Toast.makeText(getApplicationContext(), "Flat Num cannot be empty", Toast.LENGTH_SHORT).show();
                                            }
                                        }else{
                                            Toast.makeText(getApplicationContext(), "Wing cannot be empty", Toast.LENGTH_SHORT).show();
                                        }
                                    }else{
                                        Toast.makeText(getApplicationContext(), "Name cannot be empty", Toast.LENGTH_SHORT).show();
                                    }
                                }
                            })
                            .show();
                }
            });
        }
    }
}
