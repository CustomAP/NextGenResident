package nextgenentrycard.nextgenResident.Directory;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import nextgenentrycard.nextgenResident.Models.DirectoryItem;
import nextgenentrycard.nextgenResident.R;

/**
 * Created by Ashish Pawar(ashishpawar2015.ap@gmail.com) on 18/3/18.
 */

public class DirectoryAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder>{
    private ArrayList<DirectoryItem> directoryItems;
    private Context context;

    public DirectoryAdapter(ArrayList<DirectoryItem> directoryItems){
        this.directoryItems = directoryItems;
    }
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.directory_single_item, parent, false);
        return new DirectoryHolder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        final DirectoryHolder directoryHolder = (DirectoryHolder)holder;
        this.context = directoryHolder.ivCall.getContext();
        directoryHolder.tvFlatNum.setText(directoryItems.get(directoryHolder.getAdapterPosition()).getFlatNum());
        directoryHolder.tvWing.setText(directoryItems.get(directoryHolder.getAdapterPosition()).getWing());
        directoryHolder.tvName.setText(directoryItems.get(directoryHolder.getAdapterPosition()).getName());

        directoryHolder.ivCall.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_DIAL, Uri.parse("tel:" +directoryItems.get(directoryHolder.getAdapterPosition()).getMobNum()));
                context.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return directoryItems.size();
    }

    public class DirectoryHolder extends RecyclerView.ViewHolder {
        TextView tvName, tvWing, tvFlatNum;
        ImageView ivCall;
        public DirectoryHolder(View itemView) {
            super(itemView);
            tvName = (TextView)itemView.findViewById(R.id.tvName_directorySingleItem);
            tvWing = (TextView)itemView.findViewById(R.id.tvWing_directorySingleItem);
            tvFlatNum = (TextView)itemView.findViewById(R.id.tvFlatNum_directorySingleItem);
            ivCall = (ImageView)itemView.findViewById(R.id.ivCall_directorySingleItem);
        }
    }

    public void update(ArrayList<DirectoryItem> directoryItems){
        this.directoryItems = directoryItems;
        this.notifyDataSetChanged();
    }
}
