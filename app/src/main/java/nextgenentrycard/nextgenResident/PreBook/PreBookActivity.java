package nextgenentrycard.nextgenResident.PreBook;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;

import nextgenentrycard.nextgenResident.Models.PreBookItem;
import nextgenentrycard.nextgenResident.R;

/**
 * Created by Ashish Pawar(ashishpawar2015.ap@gmail.com) on 27/10/17.
 */

public class PreBookActivity extends AppCompatActivity {
    private static FragmentManager fragmentManager;
    FragmentTransaction fragmentTransaction;
    PreBookItem preBookItem;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_prebook);
        setTitle("Pre-Book Visitor");

        preBookItem=(PreBookItem)getIntent().getSerializableExtra("PreBookItem");


        PreBookFragment preBookFragment=new PreBookFragment();
        Bundle args=new Bundle();
        args.putSerializable("PreBookItem",preBookItem);
        preBookFragment.setArguments(args);
        /*
            adding fragment to the activity
         */
        fragmentManager = getSupportFragmentManager();
        fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.add(R.id.prebook_container, preBookFragment);
        fragmentTransaction.commit();
    }
}
