package nextgenentrycard.nextgenResident.PreBook;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;
import android.widget.Toast;

import com.androidquery.AQuery;
import com.androidquery.callback.AjaxCallback;
import com.androidquery.callback.AjaxStatus;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

import de.greenrobot.event.EventBus;
import nextgenentrycard.nextgenResident.DbHelper.DbHelper;
import nextgenentrycard.nextgenResident.EventBus.PrebookReqSentEvent;
import nextgenentrycard.nextgenResident.Models.PreBookItem;

/**
 * Created by Ashish Pawar(ashishpawar2015.ap@gmail.com) on 26/10/17.
 */

public class PreBookServerConnection {
    private AQuery aQuery;
    private Context context;

    String sendPreBookRequest="http://www.accesspoint.in/index.php/PreBookVisitor/sendPreBookRequest";
    String getPreBookIDsToDelete="http://www.accesspoint.in/index.php/PreBookVisitor/getPreBookItemsToDelete";

    public PreBookServerConnection(Context context){
        this.context=context;
        aQuery=new AQuery(context);
    }

    public void sendPreBookRequest(final PreBookItem preBookItem){
        SharedPreferences preferences=context.getSharedPreferences("LoginDetails",Context.MODE_PRIVATE);
        String sessionID=preferences.getString("sessionID",null);
        HashMap<String,Object> prebookParams=new HashMap<>();
        prebookParams.put("sessionID",sessionID);
        prebookParams.put("visitorName",preBookItem.getVisitorName());
        prebookParams.put("purposeOfMeet",preBookItem.getPurposeOfMeet());
        prebookParams.put("organisation",preBookItem.getOrganisation());
        prebookParams.put("date",preBookItem.getDate());
        prebookParams.put("time",preBookItem.getTime());
        prebookParams.put("IDProof",preBookItem.getIDProof());
        prebookParams.put("IDNum",preBookItem.getIDNum());
        prebookParams.put("mobNum",preBookItem.getMobNum());
        prebookParams.put("type",preBookItem.getType());
        prebookParams.put("visitorID",preBookItem.getVisitorID());

        Log.d("id num",preBookItem.getIDNum());

        aQuery.ajax(sendPreBookRequest,prebookParams, JSONObject.class,new AjaxCallback<JSONObject>(){
            @Override
            public void callback(String url, JSONObject object, AjaxStatus status) {
                int prebookID=0;

                if(object!=null){
                    Log.d("sendPrebookReq","object!=null");
                    try{
                        prebookID=object.getInt("prebookID");
                    }catch (Exception e){
                        e.printStackTrace();
                    }
                }else{
                    Toast.makeText(context,"Failed to send Prebook Request",Toast.LENGTH_SHORT).show();
                    Log.d("sendPrebookReq","object=null");
                }

                if(status.getCode()==200){
                    preBookItem.setPrebookID(prebookID);
                    DbHelper helper=new DbHelper(context);
                    helper.insertPreBookVisitor(preBookItem);
                    EventBus.getDefault().post(new PrebookReqSentEvent(true));
                }
            }
        });
    }

    public void getPreBookIDsToDelete(){
        final ArrayList<Integer> prebookIDList=new ArrayList<>();

        SharedPreferences preferences=context.getSharedPreferences("LoginDetails",Context.MODE_PRIVATE);
        String sessionID=preferences.getString("sessionID",null);

        HashMap<String,Object> prebookParams=new HashMap<>();
        prebookParams.put("sessionID",sessionID);

        aQuery.ajax(getPreBookIDsToDelete,prebookParams,JSONObject.class,new AjaxCallback<JSONObject>(){
            @Override
            public void callback(String url, JSONObject object, AjaxStatus status) {
                if(object!=null){
                    Log.d("prebookIDsToDelete","object!=null");
                    try{
                        int num,IDs;
                        String preBookID;

                        num=object.getInt("num");
                        preBookID=object.getString("prebookIDs");

                        JSONObject reader=new JSONObject(preBookID);

                        for(int i =0;i<num;i++){
                            JSONObject obj=reader.getJSONObject(String.valueOf(i));
                            IDs=obj.getInt("prebookID");

                            prebookIDList.add(IDs);
                        }
                    }catch (Exception e ){
                        e.printStackTrace();
                    }

                }else{
                    Log.d("prebookIDsToDelete","object=null");
                }
                if(status.getCode()==200){
                    DbHelper dbHelper=new DbHelper(context);
//                    dbHelper.deletePreBookItem(prebookIDList);
                }
            }
        });
    }
}
