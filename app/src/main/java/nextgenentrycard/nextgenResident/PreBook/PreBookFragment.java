package nextgenentrycard.nextgenResident.PreBook;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.yarolegovich.lovelydialog.LovelyStandardDialog;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import de.greenrobot.event.EventBus;
import nextgenentrycard.nextgenResident.EventBus.PrebookReqSentEvent;
import nextgenentrycard.nextgenResident.Models.PreBookItem;
import nextgenentrycard.nextgenResident.R;

/**
 * Created by Ashish Pawar(ashishpawar2015.ap@gmail.com) on 24/10/17.
 */

public class PreBookFragment extends Fragment implements View.OnClickListener {
    TextView  tvDate, tvTime, tvOfficial, tvPersonal , tvVendor, tvApartmentName, tvSecurityName;
//    LinearLayout llDate, llTime;
    EditText etVisitorName, etOrganisation;
    FloatingActionButton bSubmit;
//    Spinner sPurposeOfMeet;
//    String[] purposesOfMeet;
    PreBookItem prebookItem;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_prebook, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        /*
            initializing views
         */
//        llDate = (LinearLayout) view.findViewById(R.id.llDate_prebookVisitor);
//        llTime = (LinearLayout) view.findViewById(R.id.llTime_prebookVisitor);
//        tvPurposeOfMeet = (TextView) view.findViewById(R.id.tvPurposeOfMeet_prebookVisitor);
        tvDate = (TextView) view.findViewById(R.id.tvDate_prebookVisitor);
        tvTime = (TextView) view.findViewById(R.id.tvTime_prebookVisitor);
        etVisitorName = (EditText) view.findViewById(R.id.etName_prebookVisitor);
        etOrganisation = (EditText) view.findViewById(R.id.etOrganisation_prebookVisitor);
        bSubmit = (FloatingActionButton) view.findViewById(R.id.bSubmit_prebookVisitor);
        tvOfficial = (TextView)view.findViewById(R.id.tvOfficial_prebookFragment);
        tvPersonal = (TextView)view.findViewById(R.id.tvPersonal_prebookFragment);
        tvVendor = (TextView)view.findViewById(R.id.tvVendor_prebookFragment);
        tvSecurityName = (TextView)view.findViewById(R.id.tvSecurityName_prebookFragment);
        tvApartmentName = (TextView)view.findViewById(R.id.tvApartmentName_prebookFragment);
//        sPurposeOfMeet=(Spinner)view.findViewById(R.id.sPurposeOfMeet_prebookVisitor);
//        tvGuest=(TextView)view.findViewById(R.id.tvGuest_prebookVisitor);
//        tvVendor=(TextView)view.findViewById(R.id.tvVendor_prebookVisitor);

        prebookItem=new PreBookItem("","","","","","","","","",0,0);

        /*
            setting date
         */
        Date date=new Date();
        SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd");
        String dateString=sdf.format(date);

        tvDate.setText(dateString);

        /*
            setting purpose to meet
         */
//        purposesOfMeet = new String[]{"Personal", "Meeting", "Interview"};
//
//        Arrays.sort(purposesOfMeet);
//
//        ArrayAdapter<String> purposeOfMeetAdapter = new ArrayAdapter<String>(getContext(), R.layout.spinner_item, purposesOfMeet);
//
//        sPurposeOfMeet.setAdapter(purposeOfMeetAdapter);


        /*
            setting  time
         */
        Calendar mcurrentTime = Calendar.getInstance();
        int hour = mcurrentTime.get(Calendar.HOUR_OF_DAY);
        int minute = mcurrentTime.get(Calendar.MINUTE);

        String sHour=String.valueOf(hour);
        String sMinute=String.valueOf(minute);
        if(hour/10==0){
            sHour="0"+hour;
        }

        if(minute/10==0){
            sMinute="0"+minute;
        }
        tvTime.setText( sHour + ":" + sMinute);


        /*
            setting company and employee name
         */
        SharedPreferences preferences=getContext().getSharedPreferences("LoginDetails", Context.MODE_PRIVATE);
        prebookItem.setType("Guest");

        tvApartmentName.setText(preferences.getString("apartmentName", ""));
        tvSecurityName.setText(preferences.getString("securityName", ""));

        /*
            setting onclick listeners
         */
//        tvPurposeOfMeet.setOnClickListener(this);
        tvDate.setOnClickListener(this);
        tvTime.setOnClickListener(this);
        bSubmit.setOnClickListener(this);
        tvVendor.setOnClickListener(this);
        tvPersonal.setOnClickListener(this);
        tvOfficial.setOnClickListener(this);
//        tvVendor.setOnClickListener(this);
//        tvGuest.setOnClickListener(this);

        EventBus.getDefault().register(this);

        prebookItem.setPurposeOfMeet("Official");


        /*
            getting arguments and setting field values
         */
        prebookItem= (PreBookItem) getArguments().getSerializable("PreBookItem");

//        if(!prebookItem.getPurposeOfMeet().equals("")){
//            tvPurposeOfMeet.setVisibility(View.GONE);
//            sPurposeOfMeet.setVisibility(View.VISIBLE);
//            sPurposeOfMeet.setSelection(getSelectionIDForPurpose(prebookItem.getPurposeOfMeet()));
//        }
//
//        if(prebookItem.getType().equals("Vendor")){
//            tvVendor.setBackgroundColor(getResources().getColor(R.color.colorAccent));
//            tvVendor.setTextColor(getResources().getColor(android.R.color.white));
//            tvGuest.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
//            tvGuest.setTextColor(getResources().getColor(android.R.color.black));
//        }

        etVisitorName.setText(prebookItem.getVisitorName());
        etOrganisation.setText(prebookItem.getOrganisation());

        Log.d("prebookitem","idnum "+prebookItem.getIDNum());

        Thread focus =  new Thread(){
            public void run(){
                try {
                    sleep(500);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }finally {
                    getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            etVisitorName.requestFocus();
                            etVisitorName.clearFocus();

                        }
                    });
                }
            }
        };

        //focus.start();
    }

//    private int getSelectionIDForPurpose(String purpose){
//        for(int i =0;i<purposesOfMeet.length;i++){
//            if(purposesOfMeet[i].equals(purpose))
//                return i;
//        }
//        return 0;
//    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
//            case R.id.tvPurposeOfMeet_prebookVisitor:
//                tvPurposeOfMeet.setVisibility(View.GONE);
//                sPurposeOfMeet.setVisibility(View.VISIBLE);
//                sPurposeOfMeet.performClick();
//                break;

            case R.id.tvDate_prebookVisitor:

                String dateString=tvDate.getText().toString();

                String sYear=dateString.substring(0,dateString.indexOf("-"));
                String sMonthDate=dateString.substring(dateString.indexOf("-")+1);
                String sMonth=sMonthDate.substring(0,sMonthDate.indexOf("-"));
                String sDate=sMonthDate.substring(sMonthDate.indexOf("-")+1);

                DatePickerDialog datePickerDialog=new DatePickerDialog(getContext(), new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int i, int i1, int i2) {
                        i1++;
                        if (i2 / 10 != 0 && i1 / 10 != 0)
                            tvDate.setText(i+"-"+i1+"-"+i2);
                        else if (i2 / 10 == 0 && i1 / 10 != 0)
                            tvDate.setText(i+"-"+i1+"-0"+i2);
                        else if (i2 / 10 != 0 && i1 / 10 == 0)
                            tvDate.setText(i+"-0"+i1+"-"+i2);
                        else
                            tvDate.setText(i+"-0"+i1+"-0"+i2);
                    }
                },Integer.parseInt(sYear),Integer.parseInt(sMonth)-1,Integer.parseInt(sDate));
                datePickerDialog.show();
                break;

            case R.id.tvTime_prebookVisitor:
                Calendar mcurrentTime = Calendar.getInstance();
                int hour = mcurrentTime.get(Calendar.HOUR_OF_DAY);
                int minute = mcurrentTime.get(Calendar.MINUTE);
                TimePickerDialog mTimePicker;
                mTimePicker = new TimePickerDialog(getContext(), new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {
                        String hour=String.valueOf(selectedHour);
                        String minute=String.valueOf(selectedMinute);
                        if(selectedHour/10==0){
                            hour="0"+selectedHour;
                        }

                        if(selectedMinute/10==0){
                            minute="0"+selectedMinute;
                        }
                        tvTime.setText( hour + ":" + minute);
                    }
                }, hour, minute, true);//Yes 24 hour time
                mTimePicker.show();

                break;

            case R.id.bSubmit_prebookVisitor:
                String visitorName=etVisitorName.getText().toString();
                String organisation=etOrganisation.getText().toString();

                if(!visitorName.equals("")){
//                    if(tvPurposeOfMeet.getVisibility()==View.GONE){
                        if(!organisation.equals("")){
                            bSubmit.setEnabled(false);
                            bSubmit.setClickable(false);
                            bSubmit.setFocusable(false);

//                            String purposeToMeet=sPurposeOfMeet.getSelectedItem().toString();
                            String date=tvDate.getText().toString();
                            String time=tvTime.getText().toString();

                            prebookItem.setVisitorName(visitorName);
                            prebookItem.setOrganisation(organisation);
                            prebookItem.setDate(date);
                            prebookItem.setTime(time);

                            PreBookServerConnection prebookServerConnection=new PreBookServerConnection(getContext());
                            prebookServerConnection.sendPreBookRequest(prebookItem);
                        }else{
                            Toast.makeText(getContext(),"Please enter organisation",Toast.LENGTH_SHORT).show();
                        }
//                    }else{
//                        Toast.makeText(getContext(),"Please select purpose to meet",Toast.LENGTH_SHORT).show();
//                    }
                }else{
                    Toast.makeText(getContext(),"Please enter visitor name",Toast.LENGTH_SHORT).show();
                }
                break;

            case R.id.tvVendor_prebookFragment:
                tvVendor.setTextColor(getResources().getColor(R.color.colorPrimary));
                tvOfficial.setTextColor(getResources().getColor(android.R.color.darker_gray));
                tvPersonal.setTextColor(getResources().getColor(android.R.color.darker_gray));
                prebookItem.setPurposeOfMeet("Vendor");
                break;

            case R.id.tvOfficial_prebookFragment:
                prebookItem.setPurposeOfMeet("Official");
                tvOfficial.setTextColor(getResources().getColor(R.color.colorPrimary));
                tvVendor.setTextColor(getResources().getColor(android.R.color.darker_gray));
                tvPersonal.setTextColor(getResources().getColor(android.R.color.darker_gray));
                break;

            case R.id.tvPersonal_prebookFragment:
                prebookItem.setPurposeOfMeet("Personal");
                tvPersonal.setTextColor(getResources().getColor(R.color.colorPrimary));
                tvOfficial.setTextColor(getResources().getColor(android.R.color.darker_gray));
                tvVendor.setTextColor(getResources().getColor(android.R.color.darker_gray));
                break;

//            case R.id.tvGuest_prebookVisitor:
//                prebookItem.setType("Guest");
//                /*
//                    setting colors
//                 */
//                tvGuest.setBackgroundColor(getResources().getColor(R.color.colorAccent));
//                tvGuest.setTextColor(getResources().getColor(android.R.color.white));
//                tvVendor.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
//                tvVendor.setTextColor(getResources().getColor(android.R.color.white));
//                break;
//
//            case R.id.tvVendor_prebookVisitor:
//                prebookItem.setType("Vendor");
//                /*
//                    setting colors
//                 */
//                tvVendor.setBackgroundColor(getResources().getColor(R.color.colorAccent));
//                tvVendor.setTextColor(getResources().getColor(android.R.color.white));
//                tvGuest.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
//                tvGuest.setTextColor(getResources().getColor(android.R.color.white));
//                break;
        }
    }

    public void onEvent(PrebookReqSentEvent prebookReqSentEvent){

        bSubmit.setEnabled(true);
        bSubmit.setClickable(true);
        bSubmit.setFocusable(true);

        if(prebookReqSentEvent.isPrebookReqSent()){
                /*
                   prebook request feedback
                 */
            new LovelyStandardDialog(getContext(),R.style.myDialog)
                    .setTopColorRes(R.color.colorPrimary)
                    .setButtonsColorRes(R.color.black)
                    .setIcon(R.mipmap.done)
                    .setTitle(R.string.info)
                    .setMessage(R.string.prebooking_feedback)
                    .setPositiveButton(android.R.string.ok, new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            etVisitorName.setText("");
//                            tvPurposeOfMeet.setVisibility(View.VISIBLE);
//                            sPurposeOfMeet.setVisibility(View.GONE);
                            etOrganisation.setText("");
                            prebookItem=new PreBookItem();
                        }
                    })
                    .show();
        }
    }
}
