package nextgenentrycard.nextgenResident.Room;

import android.app.DatePickerDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.androidquery.AQuery;
import com.androidquery.callback.AjaxCallback;
import com.androidquery.callback.AjaxStatus;

import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;

import nextgenentrycard.nextgenResident.DbHelper.DbHelper;
import nextgenentrycard.nextgenResident.Models.RoomBookingSingleItem;
import nextgenentrycard.nextgenResident.Models.RoomItem;
import nextgenentrycard.nextgenResident.R;

/**
 * Created by Ashish Pawar(ashishpawar2015.ap@gmail.com) on 19/2/18.
 */

public class RoomBookingActivity extends AppCompatActivity implements View.OnClickListener{
    TextView tvDate, tvRoom;
    FloatingActionButton fabAdd, fabDone;
    RecyclerView rvRoomBookings;
    Spinner sRoom;

    AQuery aQuery;
    RoomAdapter adapter;

    String getBookings = "http://www.accesspoint.in/index.php/Rooms/getRoomBookings";

    ArrayList<RoomItem> roomItems;
    ArrayList<String> roomNames;
    ArrayList<RoomBookingSingleItem> roomBookingSingleItems;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_roombooking);

        initialize();
    }

    private void initialize() {
        tvDate = (TextView)findViewById(R.id.tvDate_activityRoomBooking);
        fabAdd = (FloatingActionButton)findViewById(R.id.fabAdd_activityRoomBooking);
        fabDone = (FloatingActionButton)findViewById(R.id.fabDone_activityRoomBooking);
        rvRoomBookings = (RecyclerView)findViewById(R.id.rvRoomBooking);
        tvRoom = (TextView)findViewById(R.id.tvRoom_activityRoomBooking);
        sRoom = (Spinner)findViewById(R.id.sRoom_activityRoomBooking);

        aQuery = new AQuery(getApplicationContext());
        roomItems = new ArrayList<>();
        roomNames = new ArrayList<>();
        roomBookingSingleItems = new ArrayList<>();

        DbHelper dbHelper = new DbHelper(getApplicationContext());
        roomItems = dbHelper.getRooms();

        for(int i = 0; i < roomItems.size(); i++)
            roomNames.add(roomItems.get(i).getName());

        Collections.sort(roomNames);

        ArrayAdapter<String> roomAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item, roomNames);
        sRoom.setAdapter(roomAdapter);


        Date date1 = new Date();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

        tvDate.setText(sdf.format(date1));

        adapter = new RoomAdapter(roomBookingSingleItems);

        rvRoomBookings.setAdapter(adapter);
        rvRoomBookings.setItemAnimator(new DefaultItemAnimator());
        rvRoomBookings.setLayoutManager(new LinearLayoutManager(getApplicationContext()));

        tvDate.setOnClickListener(this);
        fabDone.setOnClickListener(this);
        fabAdd.setOnClickListener(this);
        tvRoom.setOnClickListener(this);
    }

    private int getIDForSelectedRoom(String roomName) {
        for (int i = 0; i < roomNames.size(); i++) {
            if (roomItems.get(i).getName().equals(roomName))
                return roomItems.get(i).getRoomID();
        }
        return 0;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.tvDate_activityRoomBooking:
                String dateString = tvDate.getText().toString();

                String year = dateString.substring(0, dateString.indexOf("-"));
                String month_date = dateString.substring(dateString.indexOf("-") + 1);
                String month = month_date.substring(0, month_date.indexOf("-"));
                String date = month_date.substring(month_date.indexOf("-") + 1);
                final DatePickerDialog datePickerDialog = new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int i, int i1, int i2) {
                        i1++;
                        if (i2 / 10 != 0 && i1 / 10 != 0)
                            tvDate.setText(i + "-" + i1 + "-" + i2);
                        else if (i2 / 10 == 0 && i1 / 10 != 0)
                            tvDate.setText(i + "-" + i1 + "-0" + i2);
                        else if (i2 / 10 != 0 && i1 / 10 == 0)
                            tvDate.setText(i + "-0" + i1 + "-" + i2);
                        else
                            tvDate.setText(i + "-0" + i1 + "-0" + i2);
                    }
                }, Integer.parseInt(year), Integer.parseInt(month) - 1, Integer.parseInt(date));
                datePickerDialog.show();
                break;
            case R.id.fabAdd_activityRoomBooking:

                break;
            case R.id.fabDone_activityRoomBooking:
                if(tvRoom.getVisibility() == View.GONE) {
                    roomBookingSingleItems.clear();

                    SharedPreferences preferences = getSharedPreferences("LoginDetails", Context.MODE_PRIVATE);
                    String sessionID = preferences.getString("sessionID", null);

                    HashMap<String, Object> bookingsParams = new HashMap<>();
                    bookingsParams.put("sessionID", sessionID);
                    bookingsParams.put("date", tvDate.getText().toString());
                    bookingsParams.put("roomID", getIDForSelectedRoom(sRoom.getSelectedItem().toString()));

                    aQuery.ajax(getBookings, bookingsParams, JSONObject.class, new AjaxCallback<JSONObject>() {
                        @Override
                        public void callback(String url, JSONObject object, AjaxStatus status) {
                            String bookings, startTime, endTime, employeeName;
                            int bookingID, employeeID, num;

                            if (object != null) {
                                Log.d("getBookings", "object!=null");
                                try {
                                    bookings = object.getString("bookings");
                                    num = object.getInt("num");

                                    JSONObject reader = new JSONObject(bookings);

                                    for (int i = 0; i < num; i++) {
                                        JSONObject obj = reader.getJSONObject(String.valueOf(i));
                                        startTime = obj.getString("startTime");
                                        endTime = obj.getString("endTime");
                                        employeeName = obj.getString("employeeName");
                                        bookingID = obj.getInt("bookingID");
                                        employeeID = obj.getInt("employeeID");

                                        roomBookingSingleItems.add(new RoomBookingSingleItem(bookingID, employeeID, startTime, endTime, employeeName));
                                    }
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            } else {
                                Log.d("getBookings", "object=null");
                                Toast.makeText(getApplicationContext(), "Failed to get conference room bookings", Toast.LENGTH_SHORT).show();
                            }

                            if (status.getCode() == 200) {
                                adapter.update(roomBookingSingleItems);
                                fabAdd.setVisibility(View.VISIBLE);
                            }
                        }
                    });

                }else{
                    Toast.makeText(getApplicationContext(), "Select conference room first", Toast.LENGTH_SHORT).show();
                }
                break;
            case R.id.tvRoom_activityRoomBooking:
                tvRoom.setVisibility(View.GONE);
                sRoom.setVisibility(View.VISIBLE);
                sRoom.performClick();
                break;
        }
    }
}
