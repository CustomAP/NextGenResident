package nextgenentrycard.nextgenResident.Room;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;


import java.util.ArrayList;

import nextgenentrycard.nextgenResident.Models.RoomBookingSingleItem;
import nextgenentrycard.nextgenResident.R;

/**
 * Created by Ashish Pawar(ashishpawar2015.ap@gmail.com) on 19/2/18.
 */

public class RoomAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private ArrayList<RoomBookingSingleItem> roomBookingSingleItems;

    public RoomAdapter(ArrayList<RoomBookingSingleItem> roomBookingSingleItems){
        this.roomBookingSingleItems = roomBookingSingleItems;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.room_single_item, parent, false);
        return new RoomHolder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        RoomHolder roomHolder = (RoomHolder)holder;

        roomHolder.tvEndTime.setText(roomBookingSingleItems.get(position).getEndTime());
        roomHolder.tvStartTime.setText(roomBookingSingleItems.get(position).getStartTime());
        roomHolder.tvEmployeeName.setText(roomBookingSingleItems.get(position).getEmployeeName());

    }

    public void update(ArrayList<RoomBookingSingleItem> roomBookingSingleItems){
        this.roomBookingSingleItems = roomBookingSingleItems;
        this.notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return roomBookingSingleItems.size();
    }

    public class RoomHolder extends RecyclerView.ViewHolder {
        TextView tvEmployeeName, tvStartTime, tvEndTime;

        public RoomHolder(View itemView) {
            super(itemView);
            tvEmployeeName = (TextView) itemView.findViewById(R.id.tvEmployeeName_roomBookingSingleItem);
            tvStartTime = (TextView)itemView.findViewById(R.id.tvStartTime_roomBookingSingleItem);
            tvEndTime = (TextView)itemView.findViewById(R.id.tvEndTime_roomBookingSingleItem);
        }
    }
}
