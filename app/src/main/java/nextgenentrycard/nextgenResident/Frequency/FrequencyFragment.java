package nextgenentrycard.nextgenResident.Frequency;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.graphics.drawable.RoundedBitmapDrawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.AutoCompleteTextView;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.SimpleAdapter;
import android.widget.TextView;

import com.androidquery.AQuery;
import com.androidquery.callback.AjaxCallback;
import com.androidquery.callback.AjaxStatus;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.BitmapImageViewTarget;
import com.yarolegovich.lovelydialog.LovelyProgressDialog;

import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import nextgenentrycard.nextgenResident.Models.VisitorDetailsItem;
import nextgenentrycard.nextgenResident.Models.VisitorItem;
import nextgenentrycard.nextgenResident.R;


/**
 * Created by Ashish Pawar(ashishpawar2015.ap@gmail.com) on 16/1/18.
 */

public class FrequencyFragment extends Fragment {
    AutoCompleteTextView atvVisitorName;
    TextView tvStartDate, tvEndDate;
    ArrayList<VisitorItem> visitorItems;
    List<HashMap<String, String>> searchList;
    AQuery aQuery;
    RecyclerView rvFrequency;
    FrequencyAdapter frequencyAdapter;
    ArrayList<VisitorDetailsItem> visitorDetailsItems;

    FloatingActionButton fabDone;

    String getVisitors = "http://www.accesspoint.in/index.php/Admin/getVisitors";
    String getVisitorLogs = "http://www.accesspoint.in/index.php/Admin/getVisitorLogs";

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_frequency,container,false);
    }

    @Override
    public void onViewCreated(final View view, @Nullable Bundle savedInstanceState) {
        tvStartDate = (TextView) view.findViewById(R.id.tvStartDate_fragmentFrequency);
        tvEndDate = (TextView) view.findViewById(R.id.tvEndDate_fragmentFrequency);
        atvVisitorName = (AutoCompleteTextView) view.findViewById(R.id.atvVisitorName_fragmentFrequency);
        rvFrequency = (RecyclerView) view.findViewById(R.id.rvFrequency);
        fabDone = (FloatingActionButton)view.findViewById(R.id.fabDone_fragmentFrequency);

        getActivity().setTitle("Visitor Log");

        atvVisitorName.setHint("Visitor Name");

        visitorItems = new ArrayList<>();
        searchList = new ArrayList<>();
        visitorDetailsItems = new ArrayList<>();

        aQuery = new AQuery(getContext());

        Date date1 = new Date();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

        tvStartDate.setText(sdf.format(date1));
        tvEndDate.setText(sdf.format(date1));

        tvStartDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String dateString = tvStartDate.getText().toString();

                String year = dateString.substring(0, dateString.indexOf("-"));
                String month_date = dateString.substring(dateString.indexOf("-") + 1);
                String month = month_date.substring(0, month_date.indexOf("-"));
                String date = month_date.substring(month_date.indexOf("-") + 1);
                final DatePickerDialog datePickerDialog = new DatePickerDialog(getContext(), new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int i, int i1, int i2) {
                        i1++;
                        if (i2 / 10 != 0 && i1 / 10 != 0)
                            tvStartDate.setText(i + "-" + i1 + "-" + i2);
                        else if (i2 / 10 == 0 && i1 / 10 != 0)
                            tvStartDate.setText(i + "-" + i1 + "-0" + i2);
                        else if (i2 / 10 != 0 && i1 / 10 == 0)
                            tvStartDate.setText(i + "-0" + i1 + "-" + i2);
                        else
                            tvStartDate.setText(i + "-0" + i1 + "-0" + i2);
                    }
                }, Integer.parseInt(year), Integer.parseInt(month) - 1, Integer.parseInt(date));
                datePickerDialog.show();
            }
        });

        tvEndDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String dateString = tvEndDate.getText().toString();

                String year = dateString.substring(0, dateString.indexOf("-"));
                String month_date = dateString.substring(dateString.indexOf("-") + 1);
                String month = month_date.substring(0, month_date.indexOf("-"));
                String date = month_date.substring(month_date.indexOf("-") + 1);
                final DatePickerDialog datePickerDialog = new DatePickerDialog(getContext(), new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int i, int i1, int i2) {
                        i1++;
                        if (i2 / 10 != 0 && i1 / 10 != 0)
                            tvEndDate.setText(i + "-" + i1 + "-" + i2);
                        else if (i2 / 10 == 0 && i1 / 10 != 0)
                            tvEndDate.setText(i + "-" + i1 + "-0" + i2);
                        else if (i2 / 10 != 0 && i1 / 10 == 0)
                            tvEndDate.setText(i + "-0" + i1 + "-" + i2);
                        else
                            tvEndDate.setText(i + "-0" + i1 + "-0" + i2);
                    }
                }, Integer.parseInt(year), Integer.parseInt(month) - 1, Integer.parseInt(date));
                datePickerDialog.show();
            }
        });


        atvVisitorName.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                visitorItems.clear();
            }

            @Override
            public void afterTextChanged(Editable editable) {
                visitorItems.clear();

                SharedPreferences sharedPreferences = getContext().getSharedPreferences("LoginDetails", Context.MODE_PRIVATE);
                String sessionID = sharedPreferences.getString("adminSessionID",null);

                final String input = atvVisitorName.getText().toString();

                HashMap<String, Object> visitorParams = new HashMap<>();
                visitorParams.put("visitorName", input);
                visitorParams.put("startDate",tvStartDate.getText().toString());
                visitorParams.put("endDate",tvEndDate.getText().toString());
                visitorParams.put("sessionID",sessionID);

                aQuery.ajax(getVisitors, visitorParams, JSONObject.class, new AjaxCallback<JSONObject>(){
                    @Override
                    public void callback(String url, JSONObject object, AjaxStatus status) {
                        String visitorName;
                        int visitorID;

                        String visitors;
                        int num = 0;
                        if(object != null){
                            try{
                                Log.d("getVisitors","object != null");
                                num = object.getInt("num");

                                visitors = object.getString("newVisitors");

                                JSONObject reader = new JSONObject(visitors);

                                for(int i = 0; i < num; i++){
                                    JSONObject obj = reader.getJSONObject(String.valueOf(i));

                                    visitorName = obj.getString("visitorName");
                                    visitorID = obj.getInt("newVisitorID");

                                    visitorItems.add(new VisitorItem(visitorID, visitorName));
                                }
                            }catch (Exception e){
                                e.printStackTrace();
                            }
                        }else{
                            Log.d("getVisitors","object = null status:"+status.getCode());
                        }
                        if(status.getCode() == 200){
                            updateAdapter(visitorItems.size());
                        }
                    }
                });
            }
        });

        fabDone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Dialog dialog = new LovelyProgressDialog(getContext())
                        .setIcon(R.drawable.person)
                        .setTopColorRes(R.color.colorPrimary)
                        .setCancelable(true)
                        .show();


                visitorDetailsItems = new ArrayList<>();

                frequencyAdapter = new FrequencyAdapter(visitorDetailsItems);
                rvFrequency.setAdapter(frequencyAdapter);
                rvFrequency.setItemAnimator(new DefaultItemAnimator());
                rvFrequency.setLayoutManager(new LinearLayoutManager(getContext()));
                rvFrequency.setNestedScrollingEnabled(false);


                SharedPreferences preferences = getContext().getSharedPreferences("LoginDetails", Context.MODE_PRIVATE);
                String sessionID = preferences.getString("adminSessionID",null);


                HashMap<String, Object> visitorParams = new HashMap<>();
                visitorParams.put("sessionID",sessionID);
                visitorParams.put("startDate",tvStartDate.getText().toString());
                visitorParams.put("endDate",tvEndDate.getText().toString());
                visitorParams.put("visitorID",0);

                aQuery.ajax(getVisitorLogs, visitorParams, JSONObject.class, new AjaxCallback<JSONObject>(){
                    @Override
                    public void callback(String url, JSONObject object, AjaxStatus status) {
                        String visitors = null;
                        int num;

                        String visitorName, employeeName, inTime, outTime, date;
                        int visitorID, employeeID;

                        if(object != null){
                            try{
                                Log.d("getVisitorLogs","object != null");

                                visitors = object.getString("result");
                                num = object.getInt("num");

                                JSONObject reader = new JSONObject(visitors);

                                for(int i = 0; i < num; i++){
                                    JSONObject obj = reader.getJSONObject(String.valueOf(i));

                                    visitorName = obj.getString("visitorName");
                                    employeeName = obj.getString("userName");
                                    inTime = obj.getString("inTime");
                                    outTime = obj.getString("outTime");
                                    date = obj.getString("date");
                                    visitorID = obj.getInt("newVisitorID");
                                    employeeID = obj.getInt("userID");

                                    visitorDetailsItems.add(new VisitorDetailsItem(visitorID, visitorName, employeeName, inTime, outTime, date, employeeID));
                                }

                            }catch (Exception e){
                                e.printStackTrace();
                            }
                        }else{
                            Log.d("getVisitorLogs","object = null status:"+status.getCode());
                        }

                        if(status.getCode() == 200){
                            /*
                                displaying in recyclerview
                             */
                            frequencyAdapter.update(visitorDetailsItems);
                        }

                        dialog.dismiss();

                    }
                });
            }
        });
    }

    private void updateAdapter(int num){
        searchList.clear();

        for(int i = 0; i < num; i++){
            HashMap<String, String> hm = new HashMap<>();
            hm.put("visitorName", visitorItems.get(i).getVisitorName());
            hm.put("visitorID", String.valueOf(visitorItems.get(i).getVisitorID()));
            searchList.add(hm);
        }

        final String[] from = {"visitorName", "visitorID"};

        int[] to ={R.id.tvVisitorName_visitorSearchSingleItem, R.id.ivPhoto_visitorSearchSingleItem};

        /*
            setting adapter
         */
        final Drawable defaultProfPic=getContext().getResources().getDrawable(R.drawable.generic_profile);

        SimpleAdapter adapter = new SimpleAdapter(getContext(), searchList, R.layout.visitor_search_single_item, from, to){
            @Override
            public void setViewImage(final ImageView v, String value) {
                Glide.with(getContext()).load("http://www.accesspoint.in/newVisitors/"+value+".JPG").asBitmap().centerCrop().error(defaultProfPic).into(new BitmapImageViewTarget(v) {
                    @Override
                    protected void setResource(Bitmap resource) {
                        try {
                            RoundedBitmapDrawable circularBitmapDrawable =
                                    RoundedBitmapDrawableFactory.create(getContext().getResources(), resource);
                            circularBitmapDrawable.setCircular(true);
                            v.setImageDrawable(circularBitmapDrawable);
                        }catch (Exception e){
                            e.printStackTrace();
                        }
                    }
                });
            }
        };

        atvVisitorName.setAdapter(adapter);
        adapter.notifyDataSetChanged();

        atvVisitorName.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                HashMap<String, String> item = (HashMap<String, String>) adapterView.getItemAtPosition(i);

                atvVisitorName.setText(item.get("visitorName"));

                /*
                     Hiding the keyboard
                */
                InputMethodManager imm = (InputMethodManager) getContext().getSystemService(Context.INPUT_METHOD_SERVICE);//hide keyboard
                imm.hideSoftInputFromWindow(atvVisitorName.getWindowToken(), 0);//hide keyboard

                /*
                    getting visitor details from server
                 */
                final Dialog dialog = new LovelyProgressDialog(getContext())
                        .setIcon(R.drawable.person)
                        .setTopColorRes(R.color.colorPrimary)
                        .setCancelable(true)
                        .show();


                visitorDetailsItems = new ArrayList<>();

                frequencyAdapter = new FrequencyAdapter(visitorDetailsItems);
                rvFrequency.setAdapter(frequencyAdapter);
                rvFrequency.setItemAnimator(new DefaultItemAnimator());
                rvFrequency.setLayoutManager(new LinearLayoutManager(getContext()));
                rvFrequency.setNestedScrollingEnabled(false);


                SharedPreferences preferences = getContext().getSharedPreferences("LoginDetails", Context.MODE_PRIVATE);
                String sessionID = preferences.getString("adminSessionID",null);


                HashMap<String, Object> visitorParams = new HashMap<>();
                visitorParams.put("sessionID",sessionID);
                visitorParams.put("startDate",tvStartDate.getText().toString());
                visitorParams.put("endDate",tvEndDate.getText().toString());
                visitorParams.put("visitorID",item.get("visitorID"));

                aQuery.ajax(getVisitorLogs, visitorParams, JSONObject.class, new AjaxCallback<JSONObject>(){
                    @Override
                    public void callback(String url, JSONObject object, AjaxStatus status) {
                        String visitors = null;
                        int num;

                        String visitorName, employeeName, inTime, outTime, date;
                        int visitorID, employeeID;

                        if(object != null){
                            try{
                                Log.d("getVisitorLogs","object != null");

                                visitors = object.getString("result");
                                num = object.getInt("num");

                                JSONObject reader = new JSONObject(visitors);

                                for(int i = 0; i < num; i++){
                                    JSONObject obj = reader.getJSONObject(String.valueOf(i));

                                    visitorName = obj.getString("visitorName");
                                    employeeName = obj.getString("userName");
                                    inTime = obj.getString("inTime");
                                    outTime = obj.getString("outTime");
                                    date = obj.getString("date");
                                    visitorID = obj.getInt("newVisitorID");
                                    employeeID = obj.getInt("userID");

                                    visitorDetailsItems.add(new VisitorDetailsItem(visitorID, visitorName, employeeName, inTime, outTime, date, employeeID));
                                }

                            }catch (Exception e){
                                e.printStackTrace();
                            }
                        }else{
                            Log.d("getVisitorLogs","object = null status:"+status.getCode());
                        }

                        if(status.getCode() == 200){
                            /*
                                displaying in recyclerview
                             */
                            frequencyAdapter.update(visitorDetailsItems);
                        }

                        dialog.dismiss();

                    }
                });

            }
        });
    }
}
