package nextgenentrycard.nextgenResident.Frequency;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.BitmapImageViewTarget;
import com.github.siyamed.shapeimageview.CircularImageView;

import java.util.ArrayList;

import nextgenentrycard.nextgenResident.Models.VisitorDetailsItem;
import nextgenentrycard.nextgenResident.R;

/**
 * Created by Ashish Pawar(ashishpawar2015.ap@gmail.com) on 19/1/18.
 */

public class FrequencyAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private ArrayList<VisitorDetailsItem> visitorDetailsItems;
    private Context context;

    public FrequencyAdapter(ArrayList<VisitorDetailsItem> visitorDetailsItems) {
        this.visitorDetailsItems = visitorDetailsItems;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.frequency_single_item, parent, false);
        return new FrequencyHolder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        final FrequencyHolder frequencyHolder = (FrequencyHolder) holder;
        context = frequencyHolder.ivPhoto.getContext();

        frequencyHolder.tvVisitorName.setText(visitorDetailsItems.get(position).getVisitorName());
        frequencyHolder.tvEmployeeName.setText(visitorDetailsItems.get(position).getEmployeeName());
        frequencyHolder.tvInTime.setText(visitorDetailsItems.get(position).getInTime());
        if (!visitorDetailsItems.get(position).getOutTime().equals("null"))
            frequencyHolder.tvOutTime.setText(visitorDetailsItems.get(position).getOutTime());
        else
            frequencyHolder.tvOutTime.setText("-");
        frequencyHolder.tvDate.setText(visitorDetailsItems.get(position).getDate());


        final Drawable defaultProfPic = context.getResources().getDrawable(R.drawable.generic_profile);

        Glide.with(context).load("http://www.accesspoint.in/newVisitors/" + visitorDetailsItems.get(frequencyHolder.getAdapterPosition()).getVisitorID() + ".JPG").asBitmap().centerCrop().error(defaultProfPic).into(new BitmapImageViewTarget(frequencyHolder.ivPhoto) {
            @Override
            protected void setResource(Bitmap resource) {
                try {
                    RoundedBitmapDrawable circularBitmapDrawable =
                            RoundedBitmapDrawableFactory.create(context.getResources(), resource);
                    circularBitmapDrawable.setCircular(true);
                    frequencyHolder.ivPhoto.setImageDrawable(circularBitmapDrawable);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

    }

    @Override
    public int getItemCount() {
        return visitorDetailsItems.size();
    }

    class FrequencyHolder extends RecyclerView.ViewHolder {
        TextView tvVisitorName, tvInTime, tvOutTime, tvEmployeeName, tvDate;
        CircularImageView ivPhoto;

        public FrequencyHolder(View itemView) {
            super(itemView);
            tvVisitorName = (TextView) itemView.findViewById(R.id.tvVisitorName_frequencySingleItem);
            tvEmployeeName = (TextView) itemView.findViewById(R.id.tvEmployeeName_frequencySingleItem);
            tvInTime = (TextView) itemView.findViewById(R.id.tvInTime_frequencySingleItem);
            tvOutTime = (TextView) itemView.findViewById(R.id.tvOutTime_frequencySingleItem);
            ivPhoto = (CircularImageView) itemView.findViewById(R.id.ivPhoto_frequencySingleItem);
            tvDate = (TextView) itemView.findViewById(R.id.tvDate_frequencySingleItem);
        }
    }

    public void update(ArrayList<VisitorDetailsItem> visitorDetailsItems) {
        this.visitorDetailsItems = visitorDetailsItems;
        this.notifyDataSetChanged();
    }
}
