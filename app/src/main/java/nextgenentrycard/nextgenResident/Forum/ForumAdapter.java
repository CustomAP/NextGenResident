package nextgenentrycard.nextgenResident.Forum;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.androidquery.AQuery;
import com.androidquery.callback.AjaxCallback;
import com.androidquery.callback.AjaxStatus;
import com.yarolegovich.lovelydialog.LovelyCustomDialog;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

import nextgenentrycard.nextgenResident.R;

/**
 * Created by Ashish Pawar(ashishpawar2015.ap@gmail.com) on 6/4/18.
 */

public class ForumAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private Context context;
    private ArrayList<ForumItem> forumItems;
    private Dialog viewForumDialog;
    private TextView tvHeading, tvDescription, tvUserName, tvWingFlatNum, tvCall, tvOption1, tvOption2, tvResult;
    private LinearLayout llOptions;

    private String vote = "http://www.accesspoint.in/index.php/Forum/vote";
    private AQuery aQuery;

    public ForumAdapter(ArrayList<ForumItem> forumItems){
        this.forumItems = forumItems;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.forum_single_item, parent, false);
        return new ForumHolder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        final ForumHolder forumHolder = (ForumHolder)holder;
        context = forumHolder.llForum.getContext();
        aQuery = new AQuery(context);

        forumHolder.tvHeading.setText(forumItems.get(forumHolder.getAdapterPosition()).getHeading());
        forumHolder.tvUserName.setText(forumItems.get(forumHolder.getAdapterPosition()).getUserName());

        forumHolder.llForum.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                viewForumDialog = new LovelyCustomDialog(context)
                        .setView(R.layout.fragment_view_forum_item)
                        .setTopColorRes(R.color.colorPrimary)
                        .setIcon(R.drawable.forum)
                        .configureView(new LovelyCustomDialog.ViewConfigurator() {
                            @Override
                            public void configureView(View v) {
                                tvHeading = (TextView)v.findViewById(R.id.tvHeading_fragmentViewForum);
                                tvDescription = (TextView)v.findViewById(R.id.tvDescription_fragmentViewForum);
                                tvUserName = (TextView)v.findViewById(R.id.tvUserName_fragmentViewForum);
                                tvWingFlatNum = (TextView)v.findViewById(R.id.tvWingFlatNum_fragmentViewForum);
                                tvCall = (TextView)v.findViewById(R.id.tvCall_fragmentViewForum);
                                tvOption1 = (TextView)v.findViewById(R.id.tvOption1_fragmentViewForum);
                                tvOption2 = (TextView)v.findViewById(R.id.tvOption2_fragmentViewForum);
                                llOptions = (LinearLayout)v.findViewById(R.id.llOptions_fragmentViewForum);
                                tvResult = (TextView)v.findViewById(R.id.tvResult_fragmentViewForum);

                                tvHeading.setText(forumItems.get(forumHolder.getAdapterPosition()).getHeading());
                                tvDescription.setText(forumItems.get(forumHolder.getAdapterPosition()).getDescription());
                                tvUserName.setText(forumItems.get(forumHolder.getAdapterPosition()).getUserName());
                                tvWingFlatNum.setText(String.format("%s %s", forumItems.get(forumHolder.getAdapterPosition()).getWing(), forumItems.get(forumHolder.getAdapterPosition()).getFlatNum()));

                                if(forumItems.get(forumHolder.getAdapterPosition()).getPoll() == 1){
                                    llOptions.setVisibility(View.VISIBLE);
                                    tvOption2.setText(forumItems.get(forumHolder.getAdapterPosition()).getOption2());
                                    tvOption1.setText(forumItems.get(forumHolder.getAdapterPosition()).getOption1());
                                }else{
                                    llOptions.setVisibility(View.GONE);
                                    Log.d("forum", "poll = false");
                                }

                                SharedPreferences sharedPreferences = context.getSharedPreferences("LoginDetails", Context.MODE_PRIVATE);
                                int userID = sharedPreferences.getInt("userID", 0);

                                if(forumItems.get(forumHolder.getAdapterPosition()).getUserID() == userID && forumItems.get(forumHolder.getAdapterPosition()).getPoll() == 1){
                                    tvResult.setVisibility(View.VISIBLE);
                                }else{
                                    tvResult.setVisibility(View.GONE);
                                }
                            }
                        })
                        .setListener(R.id.tvResult_fragmentViewForum, new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                Intent i = new Intent(context, PollResultActivity.class);
                                i.putExtra("ForumItem", forumItems.get(forumHolder.getAdapterPosition()));
                                context.startActivity(i);
                            }
                        })
                        .setListener(R.id.tvCall_fragmentViewForum, new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                Intent intent = new Intent(Intent.ACTION_DIAL, Uri.parse("tel:" +forumItems.get(forumHolder.getAdapterPosition()).getMobileNum()));
                                context.startActivity(intent);
                            }
                        })
                        .setListener(R.id.tvOption1_fragmentViewForum, new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                SharedPreferences sharedPreferences = context.getSharedPreferences("LoginDetails", Context.MODE_PRIVATE);
                                String sessionID = sharedPreferences.getString("sessionID", null);

                                HashMap<String, Object> voteParams = new HashMap<>();
                                voteParams.put("sessionID", sessionID);
                                voteParams.put("optionNum", 1);
                                voteParams.put("forumID", forumItems.get(forumHolder.getAdapterPosition()).getForumID());

                                aQuery.ajax(vote, voteParams, JSONObject.class, new AjaxCallback<JSONObject>(){
                                    @Override
                                    public void callback(String url, JSONObject object, AjaxStatus status) {
                                        int result = 0;
                                        if(object != null){
                                            try{
                                                Log.d("vote", "object != null");
                                                result = object.getInt("result");
                                            }catch (Exception e){
                                                e.printStackTrace();
                                            }
                                        }else{
                                            Log.d("vote", "object = null status:" + status.getCode());
                                            Toast.makeText(context, "Failed to add vote", Toast.LENGTH_SHORT).show();
                                        }

                                        if(status.getCode() == 200){
                                            if(result == 0){
                                                Toast.makeText(context, "Vote added successfully", Toast.LENGTH_SHORT).show();
                                                viewForumDialog.dismiss();
                                            }
                                        }
                                    }
                                });
                            }
                        })
                        .setListener(R.id.tvOption2_fragmentViewForum, new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {

                                SharedPreferences sharedPreferences = context.getSharedPreferences("LoginDetails", Context.MODE_PRIVATE);
                                String sessionID = sharedPreferences.getString("sessionID", null);

                                HashMap<String, Object> voteParams = new HashMap<>();
                                voteParams.put("sessionID", sessionID);
                                voteParams.put("optionNum", 2);
                                voteParams.put("forumID", forumItems.get(forumHolder.getAdapterPosition()).getForumID());

                                aQuery.ajax(vote, voteParams, JSONObject.class, new AjaxCallback<JSONObject>(){
                                    @Override
                                    public void callback(String url, JSONObject object, AjaxStatus status) {
                                        int result = 0;
                                        if(object != null){
                                            try{
                                                Log.d("vote", "object != null");
                                                result = object.getInt("result");
                                            }catch (Exception e){
                                                e.printStackTrace();
                                            }
                                        }else{
                                            Log.d("vote", "object = null status:" + status.getCode());
                                            Toast.makeText(context, "Failed to add vote", Toast.LENGTH_SHORT).show();
                                        }

                                        if(status.getCode() == 200){
                                            if(result == 0){
                                                Toast.makeText(context, "Vote added successfully", Toast.LENGTH_SHORT).show();
                                                viewForumDialog.dismiss();
                                            }
                                        }
                                    }
                                });
                            }
                        })
                        .show();
            }
        });
    }

    @Override
    public int getItemCount() {
        return forumItems.size();
    }

    public class ForumHolder extends RecyclerView.ViewHolder {
        TextView tvHeading, tvUserName;
        LinearLayout llForum;
        public ForumHolder(View itemView) {
            super(itemView);
            tvHeading = (TextView)itemView.findViewById(R.id.tvHeading_forumSingleItem);
            tvUserName = (TextView)itemView.findViewById(R.id.tvUserName_forumSingleItem);
            llForum = (LinearLayout)itemView.findViewById(R.id.llForumSingleItem);
        }
    }

    public void update(ArrayList<ForumItem> forumItems){
        this.forumItems = forumItems;
        this.notifyDataSetChanged();
    }
}
