package nextgenentrycard.nextgenResident.Forum;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

import nextgenentrycard.nextgenResident.Models.VoteItem;
import nextgenentrycard.nextgenResident.R;

/**
 * Created by Ashish Pawar(ashishpawar2015.ap@gmail.com) on 19/5/18.
 */

public class PollResultAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private ArrayList<VoteItem> voteItems;
    private String option1, option2;

    public PollResultAdapter(ArrayList<VoteItem> voteItems, String option1, String option2){
        this.voteItems = voteItems;
        this.option1 = option1;
        this.option2 = option2;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.poll_result_single_item, parent, false);
        return new PollResultHolder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        PollResultHolder pollResultHolder = (PollResultHolder)holder;

        pollResultHolder.tvWingFlatNum.setText(voteItems.get(pollResultHolder.getAdapterPosition()).getWing() + " " + voteItems.get(pollResultHolder.getAdapterPosition()).getFlatNum());
        pollResultHolder.tvUserName.setText(voteItems.get(pollResultHolder.getAdapterPosition()).getUserName());
        pollResultHolder.tvOption.setText(voteItems.get(pollResultHolder.getAdapterPosition()).getOptionNum() == 1 ? option1: option2);
    }

    @Override
    public int getItemCount() {
        return voteItems.size();
    }

    private class PollResultHolder extends RecyclerView.ViewHolder {
        TextView tvUserName, tvWingFlatNum, tvOption;

        public PollResultHolder(View itemView) {
            super(itemView);
            tvUserName = (TextView)itemView.findViewById(R.id.tvUserName_pollResultSingleItem);
            tvWingFlatNum = (TextView)itemView.findViewById(R.id.tvWingFlatNum_pollResultSingleItem);
            tvOption = (TextView)itemView.findViewById(R.id.tvOption_pollResultSingleItem);
        }
    }

    public void update(ArrayList<VoteItem> voteItems){
        this.voteItems = voteItems;
        this.notifyDataSetChanged();
    }
}
