package nextgenentrycard.nextgenResident.Forum;

import android.app.Dialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.widget.TextView;
import android.widget.Toast;

import com.androidquery.AQuery;
import com.androidquery.callback.AjaxCallback;
import com.androidquery.callback.AjaxStatus;
import com.github.mikephil.charting.animation.Easing;
import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.components.Description;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;
import com.github.mikephil.charting.data.PieEntry;
import com.github.mikephil.charting.utils.ColorTemplate;
import com.yarolegovich.lovelydialog.LovelyProgressDialog;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import nextgenentrycard.nextgenResident.Models.VoteItem;
import nextgenentrycard.nextgenResident.R;
import nextgenentrycard.nextgenResident.Statistics.StatisticsFragment;

/**
 * Created by Ashish Pawar(ashishpawar2015.ap@gmail.com) on 19/5/18.
 */

public class PollResultActivity extends AppCompatActivity {
    TextView tvHeading, tvDescription;
    PieChart pPollResult;
    RecyclerView rvPollResult;

    PollResultAdapter pollResultAdapter;
    ArrayList<VoteItem> voteItems;

    ForumItem forumItem;
    int option1 = 0, option2 = 0;

    Dialog progressDialog;

    AQuery aQuery;

    String getPollResult = "http://www.accesspoint.in/index.php/Forum/getPollResult";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_poll_result);
        setTitle("Poll Result");

        forumItem = (ForumItem) getIntent().getSerializableExtra("ForumItem");

        progressDialog = new LovelyProgressDialog(this)
                .setTitle("Getting poll result")
                .setIcon(R.drawable.wait)
                .setTopColorRes(R.color.colorPrimary)
                .show();

        initialize();
    }

    private void initialize() {
        tvDescription = (TextView)findViewById(R.id.tvDescription_pollResult);
        tvHeading = (TextView)findViewById(R.id.tvHeading_pollResult);
        pPollResult = (PieChart)findViewById(R.id.pPollResult_pollResult);
        rvPollResult = (RecyclerView)findViewById(R.id.rvPollResult_pollResult);

        aQuery = new AQuery(getApplicationContext());
        voteItems = new ArrayList<>();

        pollResultAdapter = new PollResultAdapter(voteItems, forumItem.getOption1(), forumItem.getOption2());

        rvPollResult.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
        rvPollResult.setItemAnimator(new DefaultItemAnimator());
        rvPollResult.setAdapter(pollResultAdapter);

        SharedPreferences sharedPreferences = getSharedPreferences("LoginDetails", Context.MODE_PRIVATE);
        String sessionID = sharedPreferences.getString("sessionID", null);

        HashMap<String, Object> pollResultParams = new HashMap<>();
        pollResultParams.put("sessionID", sessionID);
        pollResultParams.put("forumID", forumItem.getForumID());

        aQuery.ajax(getPollResult, pollResultParams, JSONObject.class, new AjaxCallback<JSONObject>(){
            @Override
            public void callback(String url, JSONObject object, AjaxStatus status) {
                int num;

                String userName, wing, flatNum;
                int userID, optionNum, voteID;

                if(object != null){
                    try{
                        Log.d("poll result", "object != null");

                        String result = object.getString("result");
                        num = object.getInt("num");

                        JSONObject reader = new JSONObject(result);

                        for(int i = 0; i < num; i++){
                            JSONObject obj = reader.getJSONObject(String.valueOf(i));

                            userName = obj.getString("userName");
                            wing = obj.getString("wing");
                            flatNum = obj.getString("flatNum");
                            optionNum = obj.getInt("optionNum");
                            voteID = obj.getInt("voteID");

                            if(optionNum == 1)
                                option1++;
                            else
                                option2++;

                            voteItems.add(new VoteItem(voteID, optionNum, userName, wing, flatNum));
                        }
                    }catch (Exception e){
                        e.printStackTrace();
                    }
                }else{
                    Log.d("poll result", "object = null status:" + status.getCode());
                    Toast.makeText(getApplicationContext(), "Failed to get Poll result", Toast.LENGTH_SHORT).show();
                    progressDialog.dismiss();
                }

                if(status.getCode() == 200){
                    List<PieEntry> voteEntries = new ArrayList<>();
                    voteEntries.add(new PieEntry((float)option1/(option2+option1)*100, forumItem.getOption1()));
                    voteEntries.add(new PieEntry((float)option2/(option2+option1)*100, forumItem.getOption2()));

                    PieDataSet voteEntriesDataSet = new PieDataSet(voteEntries, "");
                    voteEntriesDataSet.setColors(ColorTemplate.VORDIPLOM_COLORS);
                    voteEntriesDataSet.setValueTextSize(10f);


                    PieData preBookNewPieData = new PieData(voteEntriesDataSet);

                    pPollResult.setData(preBookNewPieData);
                    pPollResult.animateX(800, Easing.EasingOption.EaseInOutCirc);
                    pPollResult.animateY(800, Easing.EasingOption.EaseInOutCirc);
                    pPollResult.setEntryLabelColor(R.color.colorAccent);
                    pPollResult.setEntryLabelTextSize(10f);
                    pPollResult.setRotationEnabled(true);
                    pPollResult.invalidate();

                    Legend pPollResultLegend = pPollResult.getLegend();
                    pPollResultLegend.setPosition(Legend.LegendPosition.BELOW_CHART_CENTER);
                    pPollResultLegend.setForm(Legend.LegendForm.CIRCLE);
                    pPollResultLegend.setTextSize(10);
                    pPollResultLegend.setWordWrapEnabled(true);

                    pPollResult.getDescription().setEnabled(false);

                    pollResultAdapter.update(voteItems);
                    progressDialog.dismiss();
                }
            }
        });
    }
}
