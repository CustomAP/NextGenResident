package nextgenentrycard.nextgenResident.Forum;

import android.app.Dialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.androidquery.AQuery;
import com.androidquery.callback.AjaxCallback;
import com.androidquery.callback.AjaxStatus;
import com.rengwuxian.materialedittext.MaterialEditText;
import com.yarolegovich.lovelydialog.LovelyCustomDialog;
import com.yarolegovich.lovelydialog.LovelyProgressDialog;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

import nextgenentrycard.nextgenResident.R;

/**
 * Created by Ashish Pawar(ashishpawar2015.ap@gmail.com) on 6/4/18.
 */

public class ForumActivity extends AppCompatActivity{
    RecyclerView rvForum;
    FloatingActionButton fabAdd;

    ArrayList<ForumItem> forumItems;
    ForumAdapter forumAdapter;
    AQuery aQuery;
    Context context;
    Dialog progressDialog, dialog;
    TextView tvAdd;

    CheckBox cbPoll;
    MaterialEditText etHeading, etDescription, etOption1, etOption2;
    LinearLayout llOptions;

    String getForumList = "http://www.accesspoint.in/index.php/Forum/getForumItems";
    String addForumItem = "http://www.accesspoint.in/index.php/Forum/addForumItem";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forum);
        setTitle("Forum");
        initialize();
    }

    private void initialize() {
        rvForum = (RecyclerView) findViewById(R.id.rvForum);
        fabAdd = (FloatingActionButton)findViewById(R.id.fabAdd_forumActivity);

        aQuery = new AQuery(getApplicationContext());
        forumItems = new ArrayList<>();

        forumAdapter = new ForumAdapter(forumItems);
        context = this;

        rvForum.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
        rvForum.setItemAnimator(new DefaultItemAnimator());
        rvForum.setAdapter(forumAdapter);

        progressDialog = new LovelyProgressDialog(this)
                .setTitle("Refreshing Forum")
                .setIcon(R.drawable.wait)
                .setTopColorRes(R.color.colorPrimary)
                .show();

        getForumItems();

        fabAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog = new LovelyCustomDialog(context)
                        .setTopColorRes(R.color.colorPrimary)
                        .setTitle("Add Forum Topic")
                        .setIcon(R.drawable.forum)
                        .setView(R.layout.fragment_add_forum)
                        .configureView(new LovelyCustomDialog.ViewConfigurator() {
                            @Override
                            public void configureView(View v) {
                                etHeading = (MaterialEditText)v. findViewById(R.id.etHeading_addForum);
                                etDescription = (MaterialEditText)v. findViewById(R.id.etDescription_addForum);
                                tvAdd = (TextView)v. findViewById(R.id.tvAdd_fragmentAddForum);
                                etOption1 = (MaterialEditText)v.findViewById(R.id.etOption1_addForum);
                                etOption2 = (MaterialEditText)v.findViewById(R.id.etOption2_addForum);
                                cbPoll = (CheckBox)v.findViewById(R.id.cbIsPoll_addForum);
                                llOptions = (LinearLayout)v.findViewById(R.id.llOptions_addForum);

                                cbPoll.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                                    @Override
                                    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                                        if(isChecked){
                                            llOptions.setVisibility(View.VISIBLE);
                                        }else {
                                            llOptions.setVisibility(View.GONE);
                                        }
                                    }
                                });
                            }
                        })
                        .setListener(R.id.tvAdd_fragmentAddForum, new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                String heading = etHeading.getText().toString();
                                String description = etDescription.getText().toString();
                                String option1 = etOption1.getText().toString();
                                String option2 = etOption2.getText().toString();

                                if(!heading.equals("")){
                                    if(!description.equals("")){
                                        if((cbPoll.isChecked() && !option1.equals("") && !option2.equals("")) || !cbPoll.isChecked()) {
                                            SharedPreferences sharedPreferences = getSharedPreferences("LoginDetails", Context.MODE_PRIVATE);
                                            String sessionID = sharedPreferences.getString("sessionID", null);

                                            HashMap<String, Object> forumParams = new HashMap<>();
                                            forumParams.put("sessionID", sessionID);
                                            forumParams.put("heading", heading);
                                            forumParams.put("description", description);
                                            forumParams.put("poll", cbPoll.isChecked()?1:0);
                                            forumParams.put("option1", option1);
                                            forumParams.put("option2", option2);

                                            aQuery.ajax(addForumItem, forumParams, JSONObject.class, new AjaxCallback<JSONObject>() {
                                                @Override
                                                public void callback(String url, JSONObject object, AjaxStatus status) {
                                                    int result = 0;
                                                    if (object != null) {
                                                        try {
                                                            Log.d("addForumItem", "object != null");
                                                            result = object.getInt("result");
                                                        } catch (Exception e) {
                                                            e.printStackTrace();
                                                        }
                                                    } else {
                                                        Log.d("addForumItem", "object = null status:" + status.getCode());
                                                        Toast.makeText(getApplicationContext(), "Failed to add forum item", Toast.LENGTH_SHORT).show();
                                                    }

                                                    if (status.getCode() == 200) {
                                                        if (result == 0) {
                                                            dialog.dismiss();
                                                            forumItems.clear();
                                                            progressDialog.show();
                                                            getForumItems();
                                                        }
                                                    }
                                                }
                                            });
                                        }else{
                                            Toast.makeText(getApplicationContext(), "Please fill both the options", Toast.LENGTH_SHORT).show();
                                        }
                                    }else{
                                        Toast.makeText(getApplicationContext(), "Description cannot be empty", Toast.LENGTH_SHORT).show();
                                    }
                                } else {
                                    Toast.makeText(getApplicationContext(), "Heading cannot be empty", Toast.LENGTH_SHORT).show();
                                }
                            }
                        })
                        .show();

            }
        });
    }

    private void getForumItems(){
        SharedPreferences sharedPreferences = getSharedPreferences("LoginDetails", Context.MODE_PRIVATE);
        String sessionID = sharedPreferences.getString("sessionID", null);

        HashMap<String, Object> forumParams = new HashMap<>();
        forumParams.put("sessionID", sessionID);

        aQuery.ajax(getForumList, forumParams, JSONObject.class, new AjaxCallback<JSONObject>(){
            @Override
            public void callback(String url, JSONObject object, AjaxStatus status) {
                String forum;
                int num;

                String userName, heading, description, mobNum, date, time, wing, flatNum, option1, option2;
                int userID, forumID;
                int poll;
                if(object != null){
                    try{
                        Log.d("getForumList", "object != null");
                        forum = object.getString("forum");
                        num = object.getInt("num");

                        JSONObject reader = new JSONObject(forum);

                        for(int i = 0; i < num; i++){
                            JSONObject obj = reader.getJSONObject(String.valueOf(i));
                            userName = obj.getString("userName");
                            heading = obj.getString("heading");
                            description = obj.getString("description");
                            mobNum = obj.getString("userMobNum");
                            userID = obj.getInt("userID");
                            forumID = obj.getInt("forumID");
                            date = obj.getString("date");
                            time = obj.getString("time");
                            wing = obj.getString("wing");
                            flatNum = obj.getString("flatNum");
                            poll = obj.getInt("poll");
                            option1 = obj.getString("option1");
                            option2 = obj.getString("option2");

                            Log.d("poll", poll + "");
                            forumItems.add(new ForumItem(forumID, userID, heading, description, userName, mobNum, date, time, flatNum, wing, poll, option1, option2));
                        }
                    }catch (Exception e){
                        e.printStackTrace();
                    }
                } else {
                    Log.d("getForumList", "object = null status :" + status.getCode());
                    Toast.makeText(getApplicationContext(), "Failed to get Forum list", Toast.LENGTH_SHORT).show();
                    progressDialog.dismiss();
                }

                if(status.getCode() == 200){
                    progressDialog.dismiss();
                    forumAdapter.update(forumItems);
                }
            }
        });
    }
}
