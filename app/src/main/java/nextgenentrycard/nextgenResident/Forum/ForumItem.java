package nextgenentrycard.nextgenResident.Forum;

import java.io.Serializable;

/**
 * Created by Ashish Pawar(ashishpawar2015.ap@gmail.com) on 6/4/18.
 */

public class ForumItem implements Serializable{
    private int forumID, userID, poll;
    private String heading, description, userName, mobileNum, date, time, flatNum, wing, option1, option2;

    public int getForumID() {
        return forumID;
    }

    public int getUserID() {
        return userID;
    }

    public String getHeading() {
        return heading;
    }

    public String getDescription() {
        return description;
    }

    public String getUserName() {
        return userName;
    }

    public String getMobileNum() {
        return mobileNum;
    }

    public String getDate() {
        return date;
    }

    public String getTime() {
        return time;
    }

    public String getFlatNum() {
        return flatNum;
    }

    public String getWing() {
        return wing;
    }

    public int getPoll() {
        return poll;
    }

    public String getOption1() {
        return option1;
    }

    public String getOption2() {
        return option2;
    }

    public ForumItem(int forumID, int userID, String heading, String description, String userName, String mobileNum, String date, String time, String flatNum, String wing, int poll, String option1, String option2) {

        this.forumID = forumID;
        this.userID = userID;
        this.heading = heading;
        this.description = description;
        this.userName = userName;
        this.mobileNum = mobileNum;
        this.date = date;
        this.time = time;
        this.wing = wing;
        this.flatNum = flatNum;
        this.option1 = option1;
        this.option2 = option2;
        this.poll = poll;
    }
}
