package nextgenentrycard.nextgenResident.ChatBot;

import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

import nextgenentrycard.nextgenResident.Models.ChatItem;
import nextgenentrycard.nextgenResident.R;

public class ChatAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private ArrayList<ChatItem> chatItems;

    public ChatAdapter(ArrayList<ChatItem> chatItems){
        this.chatItems = chatItems;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.msglist, parent, false);
        return new ChatHolder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        ChatHolder viewHolder = (ChatHolder)holder;

        if (chatItems.get(position).getMsgUser().equals("user")) {

            viewHolder.rightText.setText(chatItems.get(position).getMsgText());

            viewHolder.rightText.setVisibility(View.VISIBLE);
            viewHolder.leftText.setVisibility(View.GONE);
        }
        else {
            viewHolder.leftText.setText(chatItems.get(position).getMsgText());

            viewHolder.rightText.setVisibility(View.GONE);
            viewHolder.leftText.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public int getItemCount() {
        return chatItems.size();
    }

    public class ChatHolder extends RecyclerView.ViewHolder {
        TextView leftText,rightText;

        public ChatHolder(View itemView) {
            super(itemView);
            leftText = (TextView)itemView.findViewById(R.id.leftText);
            rightText = (TextView)itemView.findViewById(R.id.rightText);
        }
    }

    public void addItem(ChatItem chatItem){
        chatItems.add(chatItem);
        this.notifyItemInserted(chatItems.size() - 1);
    }
}
