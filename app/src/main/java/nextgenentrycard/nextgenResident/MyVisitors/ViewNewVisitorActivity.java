package nextgenentrycard.nextgenResident.MyVisitors;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import nextgenentrycard.nextgenResident.Models.NewVisitorItem;
import nextgenentrycard.nextgenResident.R;

/**
 * Created by Ashish Pawar(ashishpawar2015.ap@gmail.com) on 27/10/17.
 */

public class ViewNewVisitorActivity extends AppCompatActivity {
    TextView tvName, tvPurpose, tvOrganisation, tvDate, tvTime,tvApartmentName, tvSecurityName;
    ImageButton bOkay;
    ImageView ivPhoto;
    NewVisitorItem newVisitorItem;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_new_visitor);
        setTitle("My Visitor");

        newVisitorItem = (NewVisitorItem) getIntent().getSerializableExtra("NewVisitorItem");

        /*
            initializing views
         */
        tvDate = (TextView) findViewById(R.id.tvDate_viewNewVisitor);
        tvTime = (TextView) findViewById(R.id.tvTime_viewNewVisitor);
        tvOrganisation = (TextView) findViewById(R.id.tvOrganisationName_viewNewVisitor);
        tvPurpose = (TextView) findViewById(R.id.tvPurposeOfMeet_viewNewVisitor);
//        tvType = (TextView) findViewById(R.id.tvGuestType_viewNewVisitor);
        tvName = (TextView) findViewById(R.id.tvName_viewNewVisitor);
        bOkay = (ImageButton) findViewById(R.id.bOkay_viewNewVisitor);
        ivPhoto = (ImageView) findViewById(R.id.ivPhoto_viewNewVisitor);
        tvApartmentName  = (TextView)findViewById(R.id.tvApartmentName_viewNewVisitor);
        tvSecurityName = (TextView)findViewById(R.id.tvSecurityName_viewNewVisitor);

        tvDate.setText(newVisitorItem.getDate());
        tvTime.setText(newVisitorItem.getInTime());
        tvOrganisation.setText(newVisitorItem.getOrganisation());
        tvPurpose.setText(newVisitorItem.getPurposeOfMeet());
//        tvType.setText(newVisitorItem.getType());
        tvName.setText(newVisitorItem.getName());

        SharedPreferences sharedPreferences = getSharedPreferences("LoginDetails", Context.MODE_PRIVATE);
        tvApartmentName.setText(sharedPreferences.getString("apartmentName", ""));
        tvSecurityName.setText(sharedPreferences.getString("securityName", ""));


        bOkay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        final Drawable defaultProfPic = getApplicationContext().getResources().getDrawable(R.drawable.generic_profile);

        Glide.with(getApplicationContext()).load("http://www.accesspoint.in/newVisitors/" + newVisitorItem.getVisitorID() + ".JPG").asBitmap().centerCrop().error(defaultProfPic).into(ivPhoto);
    }
}
