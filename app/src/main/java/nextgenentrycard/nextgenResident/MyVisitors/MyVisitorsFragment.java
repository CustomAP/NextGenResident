package nextgenentrycard.nextgenResident.MyVisitors;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.CardView;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.Collections;

import nextgenentrycard.nextgenResident.DbHelper.DbHelper;
import nextgenentrycard.nextgenResident.Models.NewVisitorItem;
import nextgenentrycard.nextgenResident.R;

/**
 * Created by Ashish Pawar(ashishpawar2015.ap@gmail.com) on 27/10/17.
 */

public class MyVisitorsFragment extends Fragment {
    RecyclerView rvMyVisitors;
    ArrayList<NewVisitorItem> newVisitorItems;
    CardView cvNoVisitors;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_my_visitors,container,false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        rvMyVisitors=(RecyclerView)view.findViewById(R.id.rvMyVisitors);
        cvNoVisitors=(CardView)view.findViewById(R.id.cvNoVisitors);

        DbHelper helper=new DbHelper(getContext());
        newVisitorItems=new ArrayList<>();

        newVisitorItems=helper.getAllNewVisitors();

        Collections.reverse(newVisitorItems);

        if(newVisitorItems.size()!=0)
            cvNoVisitors.setVisibility(View.GONE);

        MyVisitorsAdapter adapter=new MyVisitorsAdapter(newVisitorItems);
        rvMyVisitors.setAdapter(adapter);
        rvMyVisitors.setLayoutManager(new LinearLayoutManager(getContext()));
        rvMyVisitors.setItemAnimator(new DefaultItemAnimator());
    }
}
