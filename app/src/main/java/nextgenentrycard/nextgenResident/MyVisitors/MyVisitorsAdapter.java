package nextgenentrycard.nextgenResident.MyVisitors;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.androidquery.AQuery;
import com.androidquery.callback.AjaxCallback;
import com.androidquery.callback.AjaxStatus;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

import nextgenentrycard.nextgenResident.DbHelper.DbHelper;
import nextgenentrycard.nextgenResident.Models.NewVisitorItem;
import nextgenentrycard.nextgenResident.Models.PreBookItem;
import nextgenentrycard.nextgenResident.PreBook.PreBookActivity;
import nextgenentrycard.nextgenResident.R;

/**
 * Created by Ashish Pawar(ashishpawar2015.ap@gmail.com) on 27/10/17.
 */

public class MyVisitorsAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private ArrayList<NewVisitorItem> newVisitorItems;
    private Context context;
    private String exitVisitorByEmployee = "http://www.accesspoint.in/index.php/NewVisitor/exitVisitorByEmployee";
    private AQuery aQuery;
    private int exited = 0;

    public MyVisitorsAdapter(ArrayList<NewVisitorItem> newVisitorItems){
        this.newVisitorItems=newVisitorItems;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(parent.getContext()).inflate(R.layout.my_visitors_single_item,parent,false);
        return new MyVisitorsHolder(view);
    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, int position) {

        final MyVisitorsHolder myVisitorsHolder=(MyVisitorsHolder)holder;

        myVisitorsHolder.tvVisitorName.setText(newVisitorItems.get(position).getName());
        myVisitorsHolder.tvDate.setText(newVisitorItems.get(position).getDate());
        myVisitorsHolder.tvInTime.setText(newVisitorItems.get(position).getInTime());

        context=myVisitorsHolder.llMyVisitor.getContext();

        aQuery = new AQuery(context);


        DbHelper dbHelper = new DbHelper(context);

        exited = dbHelper.getExited(newVisitorItems.get(myVisitorsHolder.getAdapterPosition()).getVisitorID());

        if (exited == 0)
            myVisitorsHolder.bRebook_Exit.setText("EXIT");

        myVisitorsHolder.bRebook_Exit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DbHelper dbHelper = new DbHelper(context);

                exited = dbHelper.getExited(newVisitorItems.get(myVisitorsHolder.getAdapterPosition()).getVisitorID());

                if (exited == 1) {
                    /*
                        if booked already so pass that visitor id so that will be used to get the image in perbooking at security app
                    */
                    PreBookItem preBookItem = new PreBookItem(
                            newVisitorItems.get(myVisitorsHolder.getAdapterPosition()).getName(),
                            newVisitorItems.get(myVisitorsHolder.getAdapterPosition()).getPurposeOfMeet(),
                            newVisitorItems.get(myVisitorsHolder.getAdapterPosition()).getOrganisation(),
                            newVisitorItems.get(myVisitorsHolder.getAdapterPosition()).getDate(),
                            newVisitorItems.get(myVisitorsHolder.getAdapterPosition()).getInTime(),
                            newVisitorItems.get(myVisitorsHolder.getAdapterPosition()).getGovtID(),
                            newVisitorItems.get(myVisitorsHolder.getAdapterPosition()).getIDNum(),
                            newVisitorItems.get(myVisitorsHolder.getAdapterPosition()).getMobNum(),
                            newVisitorItems.get(myVisitorsHolder.getAdapterPosition()).getType(),
                            0,
                            newVisitorItems.get(myVisitorsHolder.getAdapterPosition()).getVisitorID()
                    );

                    Intent i = new Intent(context, PreBookActivity.class);
                    i.putExtra("PreBookItem", preBookItem);
                    context.startActivity(i);
                }else{
                    /*
                        Exit the visitor
                     */
                    SharedPreferences preferences=context.getSharedPreferences("LoginDetails", Context.MODE_PRIVATE);
                    String sessionID=preferences.getString("sessionID",null);

                    HashMap<String,Object> exitParams = new HashMap<String, Object>();
                    exitParams.put("sessionID",sessionID);
                    exitParams.put("visitorID",newVisitorItems.get(myVisitorsHolder.getAdapterPosition()).getVisitorID());

                    aQuery.ajax(exitVisitorByEmployee,exitParams, JSONObject.class,new AjaxCallback<JSONObject>(){
                        @Override
                        public void callback(String url, JSONObject object, AjaxStatus status) {
                            int result = 0;

                            if(object != null){
                                Log.d("exitVisitor","object != null");
                                try{
                                    result = object.getInt("result");
                                }catch (Exception e){
                                    e.printStackTrace();
                                }

                                if(status.getCode() == 200){
                                    if(result == 0){
                                        DbHelper dbHelper = new DbHelper(context);
                                        dbHelper.exitVisitor(newVisitorItems.get(myVisitorsHolder.getAdapterPosition()).getVisitorID());
                                        Toast.makeText(context,"Visitor Exited",Toast.LENGTH_SHORT).show();
                                        newVisitorItems.get(myVisitorsHolder.getAdapterPosition()).setExited(1);
                                        myVisitorsHolder.bRebook_Exit.setText("RE-BOOK");
                                    }
                                }
                            }else{
                                Log.d("exit visitor","object =  null, status : "+status.getCode());
                                Toast.makeText(context,"Failed to exit Visitor",Toast.LENGTH_SHORT).show();
                            }
                        }
                    });
                }
            }
        });

        myVisitorsHolder.llMyVisitor.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(context,ViewNewVisitorActivity.class);
                i.putExtra("NewVisitorItem",newVisitorItems.get(myVisitorsHolder.getAdapterPosition()));
                context.startActivity(i);
            }
        });
    }

    @Override
    public int getItemCount() {
        return newVisitorItems.size();
    }

    private class MyVisitorsHolder extends RecyclerView.ViewHolder {
        TextView tvVisitorName, tvDate,tvInTime;
        TextView bRebook_Exit;
        LinearLayout llMyVisitor;
        public MyVisitorsHolder(View itemView) {
            super(itemView);
            tvVisitorName=(TextView)itemView.findViewById(R.id.tvVisitorName_MyVisitorsSingleItem);
            tvDate=(TextView)itemView.findViewById(R.id.tvDate_MyVisitorsSingleItem);
            tvInTime=(TextView)itemView.findViewById(R.id.tvInTime_MyVisitorsSingleItem);
            bRebook_Exit =(TextView) itemView.findViewById(R.id.brebook_MyVisitorsSingleItem);
            llMyVisitor=(LinearLayout)itemView.findViewById(R.id.llMyVisitorSingleItem);
        }
    }
}
