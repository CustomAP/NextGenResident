package nextgenentrycard.nextgenResident;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.LinearLayout;

/**
 * Created by Ashish Pawar(ashishpawar2015.ap@gmail.com) on 6/11/17.
 */

public class AboutActivity extends AppCompatActivity {
    LinearLayout llDeveloper,llContactUs;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.fragment_about);
        setTitle("About");

        llDeveloper=(LinearLayout)findViewById(R.id.llDeveloper);
        llContactUs=(LinearLayout)findViewById(R.id.llContactUs);

        llDeveloper.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(Intent.ACTION_SEND);
                i.putExtra(Intent.EXTRA_EMAIL,new String[]{"developer.ashishpawar@gmail.com"});
                i.setType("Plain/Text");
                startActivity(i);
            }
        });

        llContactUs.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(Intent.ACTION_SEND);
                i.putExtra(Intent.EXTRA_EMAIL,new String[]{"nextgenentrycard@gmail.com"});
                i.setType("Plain/Text");
                startActivity(i);
            }
        });
    }
}
