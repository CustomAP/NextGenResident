package nextgenentrycard.nextgenResident.EventBus;

import nextgenentrycard.nextgenResident.Models.ResidentItem;

/**
 * Created by Ashish Pawar(ashishpawar2015.ap@gmail.com) on 25/10/17.
 */

public class LoginEvent {
    private String sessionID;
    private boolean loginSucceeded;
    private ResidentItem residentItem;

    public String getSessionID() {
        return sessionID;
    }

    public boolean isLoginSucceeded() {
        return loginSucceeded;
    }

    public LoginEvent(String sessionID, boolean loginSucceeded, ResidentItem residentItem) {
        this.sessionID = sessionID;
        this.loginSucceeded = loginSucceeded;
        this.residentItem = residentItem;
    }

    public ResidentItem getResidentItem() {
        return residentItem;
    }
}
