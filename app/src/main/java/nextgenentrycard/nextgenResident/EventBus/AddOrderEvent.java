package nextgenentrycard.nextgenResident.EventBus;

/**
 * Created by Ashish Pawar(ashishpawar2015.ap@gmail.com) on 5/4/18.
 */

public class AddOrderEvent {
    private int orderID;

    public int getOrderID() {
        return orderID;
    }

    public AddOrderEvent(int orderID) {

        this.orderID = orderID;
    }
}
