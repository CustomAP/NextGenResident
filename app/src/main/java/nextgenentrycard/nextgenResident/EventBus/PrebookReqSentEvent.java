package nextgenentrycard.nextgenResident.EventBus;

/**
 * Created by Ashish Pawar(ashishpawar2015.ap@gmail.com) on 26/10/17.
 */

public class PrebookReqSentEvent {
    private boolean prebookReqSent;

    public boolean isPrebookReqSent() {
        return prebookReqSent;
    }

    public PrebookReqSentEvent(boolean prebookReqSent) {

        this.prebookReqSent = prebookReqSent;
    }
}
