package nextgenentrycard.nextgenResident.EventBus;

import java.util.ArrayList;

import nextgenentrycard.nextgenResident.Models.ApartmentItem;

/**
 * Created by Ashish Pawar(ashishpawar2015.ap@gmail.com) on 25/10/17.
 */

public class ApartmentsFetchEvent {
    private boolean fetchComplete;
    private ArrayList<ApartmentItem> apartmentItems;

    public boolean isFetchComplete() {
        return fetchComplete;
    }

    public ArrayList<ApartmentItem> getApartmentItems() {
        return apartmentItems;
    }

    public ApartmentsFetchEvent(boolean fetchComplete, ArrayList<ApartmentItem> apartmentItems) {

        this.fetchComplete = fetchComplete;
        this.apartmentItems = apartmentItems;
    }
}
