package nextgenentrycard.nextgenResident.NearbyUtilities;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;

import nextgenentrycard.nextgenResident.Models.NearbyUtilitiesItem;
import nextgenentrycard.nextgenResident.R;

/**
 * Created by Ashish Pawar(ashishpawar2015.ap@gmail.com) on 17/3/18.
 */

public class NearbyUtilitiesAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private ArrayList<NearbyUtilitiesItem> nearbyUtilitiesItems;
    private Context context;

    public NearbyUtilitiesAdapter(ArrayList<NearbyUtilitiesItem> nearbyUtilitiesItems){
        this.nearbyUtilitiesItems = nearbyUtilitiesItems;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.nearby_utilities_single_item, parent, false);
        return new NearbyUtilitiesHolder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        final NearbyUtilitiesHolder nearbyUtilitiesHolder = (NearbyUtilitiesHolder)holder;
        this.context = nearbyUtilitiesHolder.tvStoreName.getContext();

        nearbyUtilitiesHolder.tvStoreType.setText(nearbyUtilitiesItems.get(nearbyUtilitiesHolder.getAdapterPosition()).getProprietorName());
        nearbyUtilitiesHolder.tvStoreName.setText(nearbyUtilitiesItems.get(nearbyUtilitiesHolder.getAdapterPosition()).getStoreName());
        nearbyUtilitiesHolder.tvCall.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_DIAL, Uri.parse("tel:" +nearbyUtilitiesItems.get(nearbyUtilitiesHolder.getAdapterPosition()).getMobileNum()));
                context.startActivity(intent);
            }
        });

        nearbyUtilitiesHolder.llNearbyUtilities.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(context, OrdersActivity.class);
                i.putExtra("utilityID", nearbyUtilitiesItems.get(nearbyUtilitiesHolder.getAdapterPosition()).getUtilityID());
                i.putExtra("delivery", nearbyUtilitiesItems.get(nearbyUtilitiesHolder.getAdapterPosition()).getDelivery());
                context.startActivity(i);
            }
        });
    }

    @Override
    public int getItemCount() {
        return nearbyUtilitiesItems.size();
    }

    public class NearbyUtilitiesHolder extends RecyclerView.ViewHolder {
        TextView tvStoreName, tvStoreType, tvCall;
        LinearLayout llNearbyUtilities;
        public NearbyUtilitiesHolder(View itemView) {
            super(itemView);
            tvStoreName = (TextView)itemView.findViewById(R.id.tvStoreName_nearbyUtilitiesSingleItem);
            tvStoreType = (TextView)itemView.findViewById(R.id.tvStoreType_nearbyUtilitiesSingleItem);
            tvCall = (TextView)itemView.findViewById(R.id.tvCall_nearbyUtilitiesSingleItem);
            llNearbyUtilities  = (LinearLayout)itemView.findViewById(R.id.llNearbyUtilities);
        }
    }

    public void update(ArrayList<NearbyUtilitiesItem> nearbyUtilitiesItems){
        this.nearbyUtilitiesItems = nearbyUtilitiesItems;
        this.notifyDataSetChanged();
    }
}
