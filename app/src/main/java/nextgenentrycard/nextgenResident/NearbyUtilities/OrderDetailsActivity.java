package nextgenentrycard.nextgenResident.NearbyUtilities;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;

import nextgenentrycard.nextgenResident.R;

/**
 * Created by Ashish Pawar(ashishpawar2015.ap@gmail.com) on 4/4/18.
 */

public class OrderDetailsActivity extends AppCompatActivity {
    ImageView ivOrder;
    TextView tvAmount, tvPaymentMode, tvOrderType;
    OrderItem orderItem;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_details);
        initialize();
    }

    private void initialize() {
        ivOrder = (ImageView)findViewById(R.id.ivOrderDetails);
        tvAmount = (TextView)findViewById(R.id.tvAmount_orderDetails);
        tvPaymentMode = (TextView)findViewById(R.id.tvPaymentMode_orderDetails);
        tvOrderType = (TextView)findViewById(R.id.tvOrderType_orderDetails);

        orderItem = (OrderItem) getIntent().getSerializableExtra("OrderItem");

        tvAmount.setText(orderItem.getAmount());
        tvOrderType.setText(orderItem.getOrderType());
        tvPaymentMode.setText(orderItem.getPaymentType());

        Glide.with(getApplicationContext()).load("http://www.accesspoint.in/orders/"+orderItem.getOrderID()+".JPG").asBitmap().diskCacheStrategy(DiskCacheStrategy.ALL).into(ivOrder);
    }
}
