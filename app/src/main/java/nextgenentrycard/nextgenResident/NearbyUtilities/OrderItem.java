package nextgenentrycard.nextgenResident.NearbyUtilities;

import java.io.Serializable;

/**
 * Created by Ashish Pawar(ashishpawar2015.ap@gmail.com) on 4/4/18.
 */

public class OrderItem implements Serializable{
    private int orderID;
    private String date;
    private String time;
    private String amount;
    private String paymentType;
    private String orderType;

    public String getOrderType() {
        return orderType;
    }

    public String getPaymentType() {

        return paymentType;
    }

    public int getOrderID() {
        return orderID;
    }

    public String getDate() {
        return date;
    }

    public String getTime() {
        return time;
    }

    public String getAmount() {
        return amount;
    }

    public OrderItem(int orderID, String date, String time, String amount, String paymentType, String orderType ) {
        this.orderID = orderID;
        this.date = date;
        this.time = time;
        this.amount = amount;
        this.paymentType = paymentType;
        this.orderType = orderType;
    }
}
