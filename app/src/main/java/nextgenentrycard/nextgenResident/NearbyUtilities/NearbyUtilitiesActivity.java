package nextgenentrycard.nextgenResident.NearbyUtilities;

import android.app.Dialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.androidquery.AQuery;
import com.androidquery.callback.AjaxCallback;
import com.androidquery.callback.AjaxStatus;
import com.rengwuxian.materialedittext.MaterialEditText;
import com.yarolegovich.lovelydialog.LovelyCustomDialog;
import com.yarolegovich.lovelydialog.LovelyProgressDialog;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

import nextgenentrycard.nextgenResident.DbHelper.DbHelper;
import nextgenentrycard.nextgenResident.Models.NearbyUtilitiesItem;
import nextgenentrycard.nextgenResident.R;

/**
 * Created by Ashish Pawar(ashishpawar2015.ap@gmail.com) on 17/3/18.
 */

public class NearbyUtilitiesActivity extends AppCompatActivity {
    RecyclerView rvUtilities;
    NearbyUtilitiesAdapter nearbyUtilitiesAdapter;

    ArrayList<NearbyUtilitiesItem> nearbyUtilitiesItems;

    Dialog nearByUtilitiesProgress;
    AQuery aQuery;

    Context context;

    private String getNearbyUtilities = "http://www.accesspoint.in/index.php/Utilities/getUtilities";
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_nearby_utilities);
        setTitle("Nearby Utilities");
        context = this;
        initialize();
    }

    private void initialize() {
        rvUtilities = (RecyclerView)findViewById(R.id.rvNearbyUtilities);

        nearByUtilitiesProgress = new LovelyProgressDialog(this)
                .setTopColorRes(R.color.colorPrimary)
                .setIcon(R.drawable.wait)
                .setCancelable(true)
                .setTitle("Getting Nearby Utilities")
                .show();

        aQuery = new AQuery(getApplicationContext());
        nearbyUtilitiesItems = new ArrayList<>();

        SharedPreferences sharedPreferences = getSharedPreferences("LoginDetails", Context.MODE_PRIVATE);
        String sessionID = sharedPreferences.getString("sessionID", null);

        HashMap<String , Object> getNearbyUtilitiesParams = new HashMap<>();
        getNearbyUtilitiesParams.put("sessionID", sessionID);

        aQuery.ajax(getNearbyUtilities, getNearbyUtilitiesParams, JSONObject.class, new AjaxCallback<JSONObject>(){
            @Override
            public void callback(String url, JSONObject object, AjaxStatus status) {
                String utilities, utilityName, proprietorName, mobileNum, secondaryMobNum;
                int num, utilityID, delivery;

                if(object != null){
                    Log.d("getUtilities", "object != null");
                    try {
                        utilities = object.getString("utilities");
                        num = object.getInt("num");

                        JSONObject reader = new JSONObject(utilities);

                        for(int i = 0; i < num; i++){
                            JSONObject obj = reader.getJSONObject(String.valueOf(i));

                            utilityName = obj.getString("storeName");
                            proprietorName = obj.getString("proprietorName");
                            mobileNum = obj.getString("mobileNum");
                            utilityID = obj.getInt("utilityID");
                            delivery = obj.getInt("delivery");
                            secondaryMobNum = obj.getString("secondaryMobNum");

                            nearbyUtilitiesItems.add(new NearbyUtilitiesItem(utilityID, utilityName, proprietorName, mobileNum, secondaryMobNum, delivery));
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }else{
                    Log.d("getUtilities", "object = null status:" +status.getCode());
                    Toast.makeText(getApplicationContext(), "Failed to get Utilities", Toast.LENGTH_SHORT).show();
                }

                if(status.getCode() == 200){

                    nearByUtilitiesProgress.dismiss();

                    nearbyUtilitiesAdapter = new NearbyUtilitiesAdapter(nearbyUtilitiesItems);

                    rvUtilities.setAdapter(nearbyUtilitiesAdapter);
                    rvUtilities.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
                    rvUtilities.setItemAnimator(new DefaultItemAnimator());
                }
            }
        });

    }
}
