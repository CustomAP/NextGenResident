package nextgenentrycard.nextgenResident.NearbyUtilities;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;

import nextgenentrycard.nextgenResident.R;

/**
 * Created by Ashish Pawar(ashishpawar2015.ap@gmail.com) on 4/4/18.
 */

public class OrderAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private Context context;
    private ArrayList<OrderItem> orderItems;

    public OrderAdapter(ArrayList<OrderItem> orderItems){
        this.orderItems = orderItems;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.order_single_item, parent, false);
        return new OrderHolder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        final OrderHolder orderHolder = (OrderHolder)holder;
        context = orderHolder.llOrder.getContext();

        orderHolder.tvOrderID.setText("Order ID #" + orderItems.get(orderHolder.getAdapterPosition()).getOrderID());
        orderHolder.tvDate.setText(orderItems.get(orderHolder.getAdapterPosition()).getDate());
        orderHolder.tvTime.setText(orderItems.get(orderHolder.getAdapterPosition()).getTime());

        orderHolder.llOrder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(context, OrderDetailsActivity.class);
                i.putExtra("OrderItem", orderItems.get(orderHolder.getAdapterPosition()));
                context.startActivity(i);
            }
        });
    }

    @Override
    public int getItemCount() {
        return orderItems.size();
    }

    public class OrderHolder extends RecyclerView.ViewHolder {
        TextView tvOrderID, tvDate, tvTime;
        LinearLayout llOrder;

        public OrderHolder(View itemView) {
            super(itemView);
            tvDate = (TextView)itemView.findViewById(R.id.tvDate_orderSingleItem);
            tvTime = (TextView)itemView.findViewById(R.id.tvTime_orderSingleItem);
            tvOrderID = (TextView)itemView.findViewById(R.id.tvOrderID_orderSingleItem);
            llOrder = (LinearLayout)itemView.findViewById(R.id.llOrder);
        }
    }
}
