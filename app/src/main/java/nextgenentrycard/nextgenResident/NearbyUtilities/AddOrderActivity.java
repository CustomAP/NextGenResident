package nextgenentrycard.nextgenResident.NearbyUtilities;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.androidquery.AQuery;
import com.androidquery.callback.AjaxCallback;
import com.androidquery.callback.AjaxStatus;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.target.BitmapImageViewTarget;
import com.github.jorgecastilloprz.FABProgressCircle;

import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.util.HashMap;

import de.greenrobot.event.EventBus;
import nextgenentrycard.nextgenResident.EventBus.AddOrderEvent;
import nextgenentrycard.nextgenResident.R;
import permissions.dispatcher.NeedsPermission;
import permissions.dispatcher.OnNeverAskAgain;
import permissions.dispatcher.OnPermissionDenied;
import permissions.dispatcher.RuntimePermissions;

/**
 * Created by Ashish Pawar(ashishpawar2015.ap@gmail.com) on 5/4/18.
 */

@RuntimePermissions
public class AddOrderActivity extends AppCompatActivity {
    TextView tvCOD, tvWallet, tvDelivery, tvPickUp;
    ImageView ivAddOrder;
    FloatingActionButton fabUpload;
    FABProgressCircle fpcUpload;
    Uri photoImage = null;
    AQuery aQuery;
    int utilityID = 0;
    int delivery = 0;

    String paymentMode, orderType;
    Bitmap orderPhoto;

    private static final int PHOTO_REQUEST = 1888;

    String addOrder = "http://www.accesspoint.in/index.php/Utilities/addOrder";
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_order);
        setTitle("Place an Order");
        initialize();
    }

    private void initialize() {
        tvCOD = (TextView)findViewById(R.id.tvCOD_addOrderActivity);
        tvWallet =  (TextView)findViewById(R.id.tvWallet_addOrderActivity);
        tvDelivery = (TextView)findViewById(R.id.tvDelivery_addOrderActivity);
        tvPickUp = (TextView)findViewById(R.id.tvPickUp_addOrderActivity);
        ivAddOrder = (ImageView)findViewById(R.id.ivAddOrder_addOrderActivity);
        fabUpload = (FloatingActionButton)findViewById(R.id.fabUpload_addOrderActivity);
        fpcUpload = (FABProgressCircle)findViewById(R.id.fpcUpload_addOrderActivity);

        aQuery = new AQuery(getApplicationContext());

        utilityID = getIntent().getIntExtra("utilityID", 0);
        delivery = getIntent().getIntExtra("delivery", 0);

        paymentMode = "COD";
        orderType = "Pick Up";

        if(delivery == 0)
            tvDelivery.setVisibility(View.GONE);

        ivAddOrder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AddOrderActivityPermissionsDispatcher.getPhotoWithPermissionCheck(AddOrderActivity.this);
            }
        });

        fabUpload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(photoImage != null){
                    fpcUpload.show();

                    try {

                        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                        orderPhoto.compress(Bitmap.CompressFormat.JPEG, 100, byteArrayOutputStream);
                        String encodedImage = Base64.encodeToString(byteArrayOutputStream.toByteArray(), Base64.DEFAULT);

                        Log.d("encoded image", encodedImage);

                        SharedPreferences sharedPreferences = getSharedPreferences("LoginDetails", Context.MODE_PRIVATE);
                        String sessionID = sharedPreferences.getString("sessionID", null);

                        HashMap<String, Object> orderParams = new HashMap<>();
                        orderParams.put("sessionID", sessionID);
                        orderParams.put("paymentMode", paymentMode);
                        orderParams.put("orderType", orderType);
                        orderParams.put("orderImage", encodedImage);
                        orderParams.put("utilityID", utilityID);

                        aQuery.ajax(addOrder, orderParams, JSONObject.class, new AjaxCallback<JSONObject>(){
                            @Override
                            public void callback(String url, JSONObject object, AjaxStatus status) {
                                int orderID = 0;

                                if(object != null){
                                    Log.d("addOrder", "object != null");
                                    try{
                                        orderID = object.getInt("orderID");
                                    }catch (Exception e){
                                        e.printStackTrace();
                                    }
                                }else{
                                    Log.d("addOrder", "object = null status:" + status.getCode());
                                    Toast.makeText(getApplicationContext(), "Failed to add Order", Toast.LENGTH_SHORT).show();
                                }

                                if(status.getCode() == 200){
                                    fpcUpload.hide();
                                    EventBus.getDefault().post(new AddOrderEvent(orderID));
                                    finish();
                                }
                            }
                        });
                    }catch (Exception e){
                        e.printStackTrace();
                    }
                }else{
                    Toast.makeText(getApplicationContext(), "Please click order photo", Toast.LENGTH_SHORT).show();
                }
            }
        });

        tvCOD.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                paymentMode = "COD";
                tvCOD.setTextColor(getResources().getColor(R.color.colorPrimary));
                tvWallet.setTextColor(getResources().getColor(android.R.color.darker_gray));
            }
        });

        tvWallet.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                paymentMode = "Wallet";
                tvCOD.setTextColor(getResources().getColor(android.R.color.darker_gray));
                tvWallet.setTextColor(getResources().getColor(R.color.colorPrimary));
            }
        });

        tvDelivery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                orderType = "Delivery";
                tvDelivery.setTextColor(getResources().getColor(R.color.colorPrimary));
                tvPickUp.setTextColor(getResources().getColor(android.R.color.darker_gray));
            }
        });

        tvPickUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                orderType = "Pick Up";
                tvDelivery.setTextColor(getResources().getColor(android.R.color.darker_gray));
                tvPickUp.setTextColor(getResources().getColor(R.color.colorPrimary));
            }
        });

    }

    @NeedsPermission({Manifest.permission.WRITE_EXTERNAL_STORAGE})
    public void getPhoto() {
        /*
           opening camera
           */
        Intent cameraIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
        startActivityForResult(cameraIntent, PHOTO_REQUEST);

    }

    @OnPermissionDenied(Manifest.permission.WRITE_EXTERNAL_STORAGE)
    void showDeniedForReadContacts() {
        Toast.makeText(this, "Permission was denied\nPlease give the required permissions and restart", Toast.LENGTH_SHORT).show();
    }

    @OnNeverAskAgain(Manifest.permission.WRITE_EXTERNAL_STORAGE)
    void showNeverAskForReadContacts() {
        Toast.makeText(this, "Please give required Permissions from Settings App", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        AddOrderActivityPermissionsDispatcher.onRequestPermissionsResult(this, requestCode, grantResults);
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        Log.d("in here", "got");
        /*
            Image received from the camera
         */
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == PHOTO_REQUEST && resultCode == Activity.RESULT_OK) {
            // photoImage = data.getData();
            try {
                photoImage = getImageUri(this, (Bitmap) data.getExtras().get("data"));
                orderPhoto = BitmapFactory.decodeStream(getContentResolver().openInputStream(photoImage), null, null);
                Log.d("int ", photoImage + "");
                ByteArrayOutputStream stream = new ByteArrayOutputStream();
                orderPhoto.compress(Bitmap.CompressFormat.PNG, 100, stream);
                Glide.with(this).load(stream.toByteArray()).centerCrop().diskCacheStrategy(DiskCacheStrategy.NONE)
                        .skipMemoryCache(true).into(ivAddOrder);
            }catch (Exception e){
                e.printStackTrace();
            }
        }
    }

    private Uri getImageUri(Context inContext, Bitmap inImage) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        inImage.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
        String path = MediaStore.Images.Media.insertImage(inContext.getContentResolver(), inImage, "title", null);
        return Uri.parse(path);
    }

}
