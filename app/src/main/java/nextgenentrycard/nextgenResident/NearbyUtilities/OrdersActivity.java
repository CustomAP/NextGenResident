package nextgenentrycard.nextgenResident.NearbyUtilities;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.androidquery.AQuery;
import com.androidquery.callback.AjaxCallback;
import com.androidquery.callback.AjaxStatus;
import com.yarolegovich.lovelydialog.LovelyProgressDialog;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

import de.greenrobot.event.EventBus;
import nextgenentrycard.nextgenResident.EventBus.AddOrderEvent;
import nextgenentrycard.nextgenResident.R;

/**
 * Created by Ashish Pawar(ashishpawar2015.ap@gmail.com) on 4/4/18.
 */

public class OrdersActivity extends AppCompatActivity {
    RecyclerView rvOrders;
    OrderAdapter orderAdapter;
    ArrayList<OrderItem> orderItems;
    FloatingActionButton fabOrder;
    AQuery aQuery;
    int utilityID, delivery;
    CardView cvOrders;

    Dialog orderProgress;

    String getOrders = "http://www.accesspoint.in/index.php/Utilities/getOrders";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_orders);
        initialize();
        EventBus.getDefault().register(this);
    }

    private void initialize() {
        rvOrders = (RecyclerView) findViewById(R.id.rvOrders);
        fabOrder = (FloatingActionButton)findViewById(R.id.bAddOrder_ordersActivity);
        cvOrders = (CardView)findViewById(R.id.cvNoOrders);

        utilityID = getIntent().getIntExtra("utilityID", 0);
        delivery = getIntent().getIntExtra("delivery", 0);

        orderItems = new ArrayList<>();
        aQuery = new AQuery(getApplicationContext());

        orderProgress = new LovelyProgressDialog(this)
                .setTopColorRes(R.color.colorPrimary)
                .setIcon(R.drawable.wait)
                .setCancelable(true)
                .setTitle("Getting Nearby Utilities")
                .show();

        fetchOrders();

        fabOrder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getApplicationContext(), AddOrderActivity.class);
                i.putExtra("utilityID", utilityID);
                i.putExtra("delivery", delivery);
                startActivity(i);
            }
        });
    }

    private void fetchOrders() {

        SharedPreferences sharedPreferences = getSharedPreferences("LoginDetails", Context.MODE_PRIVATE);
        String sessionID = sharedPreferences.getString("sessionID", null);

        HashMap<String, Object> orderParams = new HashMap<>();
        orderParams.put("sessionID", sessionID);
        orderParams.put("utilityID", utilityID);

        aQuery.ajax(getOrders, orderParams, JSONObject.class, new AjaxCallback<JSONObject>(){
            @Override
            public void callback(String url, JSONObject object, AjaxStatus status) {
                String orders, date, time, paymentMode, orderType;
                int orderID, num, amount;
                if(object != null){
                    Log.d("getOrders", "object != null");
                    try{
                        orders = object.getString("orders");
                        num = object.getInt("num");

                        JSONObject reader = new JSONObject(orders);

                        for(int i = 0; i < num; i++ ){
                            JSONObject obj = reader.getJSONObject(String.valueOf(i));
                            date = obj.getString("date");
                            time = obj.getString("time");
                            amount = obj.getInt("amount");
                            orderID = obj.getInt("orderID");
                            paymentMode = obj.getString("paymentMode");
                            orderType = obj.getString("orderType");

                            orderItems.add(new OrderItem(orderID, date, time, amount+"", paymentMode, orderType));
                        }
                    }catch (Exception e){
                        e.printStackTrace();
                    }
                }else{
                    Log.d("getOrders", "object = null status:" + status.getCode());
                    Toast.makeText(getApplicationContext(), "Failed to get Orders", Toast.LENGTH_SHORT).show();
                    orderProgress.dismiss();
                }

                if(status.getCode() == 200){
                    orderProgress.dismiss();

                    if(orderItems.size() != 0)
                        cvOrders.setVisibility(View.GONE);

                    orderAdapter = new OrderAdapter(orderItems);
                    rvOrders.setAdapter(orderAdapter);
                    rvOrders.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
                    rvOrders.setItemAnimator(new DefaultItemAnimator());
                }
            }
        });
    }

    public void onEvent(AddOrderEvent addOrderEvent){
        orderProgress.show();
        orderItems.clear();
        fetchOrders();
    }
}
