package nextgenentrycard.nextgenResident.Login;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;
import android.widget.Toast;

import com.androidquery.AQuery;
import com.androidquery.callback.AjaxCallback;
import com.androidquery.callback.AjaxStatus;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

import de.greenrobot.event.EventBus;
import nextgenentrycard.nextgenResident.EventBus.ApartmentsFetchEvent;
import nextgenentrycard.nextgenResident.EventBus.LoginEvent;
import nextgenentrycard.nextgenResident.Models.ApartmentItem;
import nextgenentrycard.nextgenResident.Models.ResidentItem;

/**
 * Created by Ashish Pawar(ashishpawar2015.ap@gmail.com) on 25/10/17.
 */

public class LoginServerConnection {
    private AQuery aQuery;
    private Context context;
    private ArrayList<ApartmentItem> apartmentItems;


    String getApartments ="http://www.accesspoint.in/index.php/Login/getApartments";
    String employeeLogin="http://www.accesspoint.in/index.php/Login/residentLogin";

    public LoginServerConnection(Context context){
        this.context=context;
        aQuery=new AQuery(context);
    }

    public void getApartments(){
        apartmentItems =new ArrayList<>();
        aQuery.ajax(getApartments,null, JSONObject.class,new AjaxCallback<JSONObject>(){
            @Override
            public void callback(String url, JSONObject object, AjaxStatus status) {
                String companyName;
                int companyID,num=0;

                if(object!=null){
                    Log.d("getcompanies","object!=null");
                    try{
                        num=object.getInt("num");

                        String companies=object.getString("apartments");

                        JSONObject reader=new JSONObject(companies);

                        for(int i=0;i<num;i++){
                            JSONObject obj=reader.getJSONObject(String.valueOf(i));

                            companyName=obj.getString("apartmentName");
                            companyID=obj.getInt("apartmentID");

                            apartmentItems.add(new ApartmentItem(companyName,companyID));
                        }
                    }catch (Exception e){
                        e.printStackTrace();
                    }
                }else{
                    Log.d("getcompanies","object=null");
                }

                if(status.getCode()==200){
                    EventBus.getDefault().post(new ApartmentsFetchEvent(true, apartmentItems));
                }
            }
        });
    }

    public void residentLogin(final ResidentItem residentItem, String playerID){
        HashMap<String,Object> loginParams=new HashMap<>();
        loginParams.put("wing", residentItem.getWing());
        loginParams.put("flatNum", residentItem.getFlatNum());
        loginParams.put("apartmentID", residentItem.getApartmentID());
        loginParams.put("residentName", residentItem.getResidentName());
        loginParams.put("mobNum", residentItem.getMobNum());
        loginParams.put("playerID", playerID);

        Log.d("residentlogin", residentItem.getWing()+" " + residentItem.getFlatNum()+" "+residentItem.getApartmentName() +" "+residentItem.getApartmentID()+" "+residentItem.getResidentName());

        aQuery.ajax(employeeLogin,loginParams,JSONObject.class,new AjaxCallback<JSONObject>(){
            @Override
            public void callback(String url, JSONObject object, AjaxStatus status) {
                String sessionID=null, securityName = "";
                int userID = 0;

                if(object!=null){
                    Log.d("login","object!=null");
                    try{
                        sessionID=object.getString("sessionID");
                        securityName = object.getString("securityName");
                        userID = object.getInt("userID");
                    }catch (Exception e){
                        e.printStackTrace();
                    }
                }else{
                    Log.d("login","object=null status:" +status.getCode());
                    Toast.makeText(context,"Failed to Login\nTry again",Toast.LENGTH_SHORT).show();
                    EventBus.getDefault().post(new LoginEvent(null, false, residentItem));
                }

                if(status.getCode()==200){
                    SharedPreferences sharedPreferences = context.getSharedPreferences("LoginDetails", Context.MODE_PRIVATE);
                    SharedPreferences.Editor editor = sharedPreferences.edit();
                    editor.putString("securityName", securityName);
                    editor.putInt("userID", userID);
                    editor.apply();

                    EventBus.getDefault().post(new LoginEvent(sessionID,true, residentItem));
                }
            }
        });
    }
}
