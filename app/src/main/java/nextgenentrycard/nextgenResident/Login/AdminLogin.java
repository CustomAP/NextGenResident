package nextgenentrycard.nextgenResident.Login;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.androidquery.AQuery;
import com.androidquery.callback.AjaxCallback;
import com.androidquery.callback.AjaxStatus;
import com.rengwuxian.materialedittext.MaterialEditText;

import org.json.JSONObject;

import java.util.HashMap;

import info.hoang8f.widget.FButton;
import nextgenentrycard.nextgenResident.MainActivity2;
import nextgenentrycard.nextgenResident.R;

/**
 * Created by Ashish Pawar(ashishpawar2015.ap@gmail.com) on 29/1/18.
 */

public class AdminLogin extends AppCompatActivity {
    FButton bLogin;
    TextView tvSkip;
    MaterialEditText etPassword;
    AQuery aQuery;

    String login = "http://www.accesspoint.in/index.php/Login/adminLogin";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_admin_login);

        bLogin = (FButton)findViewById(R.id.bLogin_AdminLogin);
        tvSkip = (TextView)findViewById(R.id.tvSkip_adminLogin);
        etPassword = (MaterialEditText)findViewById(R.id.etPassword_AdminLogin);

        aQuery = new AQuery(getApplicationContext());

        bLogin.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                final SharedPreferences preferences = getSharedPreferences("LoginDetails", Context.MODE_PRIVATE);
                String sessionID = preferences.getString("sessionID",null);

                HashMap<String, Object> loginParams = new HashMap<>();
                loginParams.put("adminPassword",etPassword.getText().toString());
                loginParams.put("sessionID",sessionID);


                aQuery.ajax(login, loginParams, JSONObject.class, new AjaxCallback<JSONObject>() {
                    @Override
                    public void callback(String url, JSONObject object, AjaxStatus status) {
                        String adminSessionID = null;
                        int adminID = 0;

                        if (object != null) {
                            Log.d("login","object!=null");
                            try {
                                adminSessionID = object.getString("adminSessionID");
                                adminID = object.getInt("adminID");
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        } else {
                            Log.d("login","object=null "+status.getCode());
                            Toast.makeText(getApplicationContext(),"Failed to login",Toast.LENGTH_SHORT).show();
                        }

                        if (status.getCode() == 200) {
                            if (adminID != 0){
                                SharedPreferences.Editor editor = preferences.edit();
                                editor.putString("adminSessionID",adminSessionID);
                                editor.putBoolean("isAdmin",true);
                                editor.apply();

                                Intent i = new Intent(getApplicationContext(), MainActivity2.class);
                                getApplicationContext().startActivity(i);
                            }
                            else{
                                Toast.makeText(getApplicationContext(), "Wrong password",Toast.LENGTH_SHORT).show();
                            }
                        }
                    }
                });
            }
        });

        tvSkip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SharedPreferences preferences = getSharedPreferences("LoginDetails",Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = preferences.edit();
                editor.putBoolean("isAdmin",false);
                editor.apply();

                Intent i = new Intent(getApplicationContext(), MainActivity2.class);
                getApplicationContext().startActivity(i);
            }
        });
    }

    @Override
    protected void onPause() {
        super.onPause();
        SharedPreferences preferences = getSharedPreferences("LoginDetails", Context.MODE_PRIVATE);
        if(preferences.getBoolean("isLoggedIn", false))
            finish();
    }
}
