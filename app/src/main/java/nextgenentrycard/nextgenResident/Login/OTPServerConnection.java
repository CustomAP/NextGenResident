package nextgenentrycard.nextgenResident.Login;

import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import com.androidquery.AQuery;
import com.androidquery.callback.AjaxCallback;
import com.androidquery.callback.AjaxStatus;

import org.json.JSONObject;

import java.util.HashMap;

import de.greenrobot.event.EventBus;
import nextgenentrycard.nextgenResident.EventBus.OTPSentEvent;
import nextgenentrycard.nextgenResident.EventBus.OTPVerifiedEvent;

/**
 * Created by Ashish Pawar(ashishpawar2015.ap@gmail.com) on 18/10/17.
 */

public class OTPServerConnection {
    private AQuery aQuery;
    private Context context;

    final String sendOTP = "http://www.accesspoint.in/index.php/Otp/sendOTP";
    final String verifyOTP = "http://www.accesspoint.in/index.php/Otp/verifyOTP";
    private String sessionID, otpStatus;

    public OTPServerConnection(Context context) {
        this.context = context;
        aQuery = new AQuery(context);
        sessionID = null;
    }

    public void sendOTP(final String mobNum) {
        final HashMap<String, Object> sendOTPParams = new HashMap<>();
        sendOTPParams.put("mobNum", mobNum);
        aQuery.ajax(sendOTP, sendOTPParams, JSONObject.class, new AjaxCallback<JSONObject>() {

            @Override
            public void callback(String url, JSONObject object, AjaxStatus status) {
                if (object != null) {
                    Log.d("sendOTP", "object!=null");
                    try {
                        otpStatus = object.getString("Status");
                        sessionID = object.getString("Details");
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else {
                    Log.d("sendOTP", "object=null");
                    EventBus.getDefault().post(new OTPSentEvent(false, mobNum, null));
                    Toast.makeText(context, "Failed to send OTP try again", Toast.LENGTH_SHORT).show();
                }

                if (status.getCode() == 200) {
                    if (otpStatus.equals("Success"))
                        EventBus.getDefault().post(new OTPSentEvent(true, mobNum, sessionID));
                    else
                        EventBus.getDefault().post(new OTPSentEvent(false,mobNum,null));
                }
            }
        });
    }

    public void verifyOTP(String sessionID, String otp) {
        HashMap<String, Object> verifyOTPParams = new HashMap<>();
        verifyOTPParams.put("sessionID", sessionID);
        verifyOTPParams.put("OTP", otp);

        aQuery.ajax(verifyOTP, verifyOTPParams, JSONObject.class, new AjaxCallback<JSONObject>() {
            @Override
            public void callback(String url, JSONObject object, AjaxStatus status) {
                int result = 0;
                if (object != null) {
                    Log.d("verifyOTP", "object!=null");
                    try {
                        result = object.getInt("ResultSet");
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else {
                    EventBus.getDefault().post(new OTPVerifiedEvent(false));
                    Log.d("verifyOTP", "object=null");
                }

                if (status.getCode() == 200) {
                    if (result == 0)
                        EventBus.getDefault().post(new OTPVerifiedEvent(true));
                    else
                        EventBus.getDefault().post(new OTPVerifiedEvent(false));
                }
            }
        });
    }
}
