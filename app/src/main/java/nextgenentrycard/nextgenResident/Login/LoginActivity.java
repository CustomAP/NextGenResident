package nextgenentrycard.nextgenResident.Login;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.androidquery.AQuery;
import com.androidquery.callback.AjaxCallback;
import com.androidquery.callback.AjaxStatus;
import com.onesignal.OSPermissionSubscriptionState;
import com.onesignal.OneSignal;
import com.rengwuxian.materialedittext.MaterialEditText;
import com.wang.avi.AVLoadingIndicatorView;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;

import de.greenrobot.event.EventBus;
import info.hoang8f.widget.FButton;
import nextgenentrycard.nextgenResident.DbHelper.DbHelper;
import nextgenentrycard.nextgenResident.EventBus.ApartmentsFetchEvent;
import nextgenentrycard.nextgenResident.EventBus.LoginEvent;
import nextgenentrycard.nextgenResident.EventBus.OTPSentEvent;
import nextgenentrycard.nextgenResident.EventBus.OTPVerifiedEvent;
import nextgenentrycard.nextgenResident.Models.ApartmentItem;
import nextgenentrycard.nextgenResident.Models.DSPLinkItem;
import nextgenentrycard.nextgenResident.Models.ResidentItem;
import nextgenentrycard.nextgenResident.Models.RoomItem;
import nextgenentrycard.nextgenResident.R;

/**
 * Created by Ashish Pawar(ashishpawar2015.ap@gmail.com) on 25/10/17.
 */

public class LoginActivity extends AppCompatActivity implements View.OnClickListener {
    TextView tvCompanyName, tvCountDown;
    Spinner sCompany;
    MaterialEditText etMobileNumSignUp, etOTPSignUp, etName, etWing, etFlatNum;
    FButton bSendOTP, bLogin;
    AVLoadingIndicatorView aviLogin;
    String mobNum = null, otpString, sessionID = null, mobileNum;
    OTPServerConnection otpServerConnection;
    LoginServerConnection loginServerConnection;
    ArrayList<ApartmentItem> apartmentItems;
    ArrayList<DSPLinkItem> dspLinkItems;
    ImageView ivDelete;


    String getRooms = "http://www.accesspoint.in/index.php/Rooms/getRooms";
    String getDSPLinks = "http://www.accesspoint.in/index.php/DSP/getAllDSPLinksForResident";

    ArrayList<RoomItem> roomItems;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_login);

        initialize();

        loginServerConnection.getApartments();
    }

    private void initialize() {

        EventBus.getDefault().register(this);

        /*
            initializing views
         */
        tvCompanyName = (TextView) findViewById(R.id.tvCompanyName_Login);
        etMobileNumSignUp = (MaterialEditText) findViewById(R.id.etMobileNum_Login);
        etOTPSignUp = (MaterialEditText) findViewById(R.id.etOTP_login);
        bSendOTP = (FButton) findViewById(R.id.bSendOtp_login);
        bLogin = (FButton) findViewById(R.id.bSubmit_Login);
        sCompany = (Spinner) findViewById(R.id.sCompanyName_Login);
        aviLogin = (AVLoadingIndicatorView) findViewById(R.id.aviLogin);
        tvCountDown = (TextView) findViewById(R.id.tvCountDown_Login);
        etName = (MaterialEditText) findViewById(R.id.etEmployeeName_Login);
        ivDelete = (ImageView) findViewById(R.id.ivDelete_Login);
        etWing = (MaterialEditText) findViewById(R.id.etWing_Login);
        etFlatNum = (MaterialEditText) findViewById(R.id.etFlatNum_Login);

        setTitle("Access Point");

        otpServerConnection = new OTPServerConnection(this);
        loginServerConnection = new LoginServerConnection(this);
        apartmentItems = new ArrayList<>();
        roomItems = new ArrayList<>();
        dspLinkItems = new ArrayList<>();

        /*
            setting on click listeners
         */
        tvCompanyName.setOnClickListener(this);
        bSendOTP.setOnClickListener(this);
        bLogin.setOnClickListener(this);
        ivDelete.setOnClickListener(this);

        aviLogin.setVisibility(View.VISIBLE);
        aviLogin.show();

    }

    private int getIDForSelectedCompany(String companyName) {
        for (int i = 0; i < apartmentItems.size(); i++) {
            if (apartmentItems.get(i).getApartmentName().equals(companyName))
                return apartmentItems.get(i).getApartmentID();
        }
        return 0;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tvCompanyName_Login:
                tvCompanyName.setVisibility(View.GONE);
                sCompany.setVisibility(View.VISIBLE);
                sCompany.performClick();
                break;

            case R.id.bSendOtp_login:
                /*
                    sending otp
                 */
                mobileNum = etMobileNumSignUp.getText().toString();
                if (mobileNum.length() != 10) {
                    Toast.makeText(this, "Mobile Number should be of 10 Characters", Toast.LENGTH_SHORT).show();
                } else {
                    etMobileNumSignUp.setFocusable(false);
                    etMobileNumSignUp.setClickable(false);
                    etMobileNumSignUp.setFocusableInTouchMode(false);

                    ivDelete.setVisibility(View.VISIBLE);

                    aviLogin.setVisibility(View.VISIBLE);
                    aviLogin.show();
                    otpServerConnection.sendOTP(mobileNum);
                }
                break;

            case R.id.bSubmit_Login:
                if (tvCompanyName.getVisibility() == View.GONE) {
                    if (!etName.getText().toString().equals("")) {
                            if (sessionID != null) {
                              /*
                                Verify if the otp matches

                                */
                        otpString = etOTPSignUp.getText().toString();
                        aviLogin.setVisibility(View.VISIBLE);
                        aviLogin.show();
                        bLogin.setEnabled(false);
                        bLogin.setFocusable(false);
                        bLogin.setClickable(false);
                        otpServerConnection.verifyOTP(sessionID, otpString);
                            } else {
                                Toast.makeText(this, "Please verify mobile number first", Toast.LENGTH_SHORT).show();
                            }
                    } else {
                        Toast.makeText(this, "Please enter Name", Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(this, "Please select Company", Toast.LENGTH_SHORT).show();
                }
                break;

            case R.id.ivDelete_Login:
                /*
                    making edit text and send otp button clickable again
                 */
                etMobileNumSignUp.setClickable(true);
                etMobileNumSignUp.setFocusable(true);
                etMobileNumSignUp.setFocusableInTouchMode(true);

                bSendOTP.setEnabled(true);
                bSendOTP.setFocusable(true);
                bSendOTP.setFocusableInTouchMode(true);
                bSendOTP.setClickable(true);

                bLogin.setEnabled(true);
                bLogin.setFocusableInTouchMode(true);
                bLogin.setFocusable(true);
                bLogin.setClickable(true);

                etMobileNumSignUp.setText("");
                tvCountDown.setVisibility(View.GONE);

                sessionID = null;
                otpString = null;
                mobNum = null;

                ivDelete.setVisibility(View.GONE);
                break;
        }
    }

    public void onEvent(OTPSentEvent otpSentEvent) {
        boolean otpSuccess = otpSentEvent.isOtpSuccess();

        aviLogin.hide();
        aviLogin.setVisibility(View.GONE);
        if (otpSuccess) {
            sessionID = otpSentEvent.getSessionID();
            mobNum = otpSentEvent.getMobNum();
            Log.d("otp sent", "success");
            Log.d("mobNum", mobNum);
            Log.d("sesssionID", sessionID);

            etMobileNumSignUp.setClickable(false);
            etMobileNumSignUp.setFocusable(false);
            etMobileNumSignUp.setFocusableInTouchMode(false);

            bSendOTP.setEnabled(false);
            bSendOTP.setFocusable(false);
            bSendOTP.setFocusableInTouchMode(false);
            bSendOTP.setClickable(false);

            tvCountDown.setVisibility(View.VISIBLE);
            new CountDownTimer(60000, 1000) {

                public void onTick(long millisUntilFinished) {
                    tvCountDown.setText("OTP sent successfully\nTry again in 00:" + millisUntilFinished / 1000);
                }

                public void onFinish() {
                    tvCountDown.setVisibility(View.GONE);

                    etMobileNumSignUp.setFocusable(true);
                    etMobileNumSignUp.setClickable(true);
                    etMobileNumSignUp.setFocusableInTouchMode(true);

                    bSendOTP.setEnabled(true);
                    bSendOTP.setFocusable(true);
                    bSendOTP.setFocusableInTouchMode(true);
                    bSendOTP.setClickable(true);

                    ivDelete.setVisibility(View.GONE);
                }
            }.start();
        } else {
            etMobileNumSignUp.setFocusable(true);
            etMobileNumSignUp.setClickable(true);
            etMobileNumSignUp.setFocusableInTouchMode(true);

            bSendOTP.setEnabled(true);
            bSendOTP.setFocusable(true);
            bSendOTP.setFocusableInTouchMode(true);
            bSendOTP.setClickable(true);

            ivDelete.setVisibility(View.GONE);
        }
    }

    public void onEvent(OTPVerifiedEvent otpVerifiedEvent) {

        boolean isOTPVerified = otpVerifiedEvent.isOTPVerified();

        if (isOTPVerified) {
            aviLogin.hide();
            aviLogin.setVisibility(View.GONE);

            SharedPreferences sharedPreferences = getSharedPreferences("LoginDetails", MODE_PRIVATE);
            SharedPreferences.Editor editor = sharedPreferences.edit();
            editor.putString("mobileNum", mobNum);
            editor.apply();

//            Intent i = new Intent(this, MainActivity.class);
//            startActivity(i);

//            for (int i = 0; i < apartmentItems.size(); i++)
//                Log.d("companyitems", apartmentItems.get(i).getApartmentName());
//            for (int i = 0; i < departmentItems.size(); i++)
//                Log.d("departmentitems", departmentItems.get(i).getDepartmentName());
//
//            Log.d("company", sCompany.getSelectedItem().toString());

//            mobNum = etMobileNumSignUp.getText().toString();   ///comment this later only for testing

            LoginServerConnection loginServerConnection = new LoginServerConnection(this);

            OSPermissionSubscriptionState status = OneSignal.getPermissionSubscriptionState();

            loginServerConnection.residentLogin(new ResidentItem(etName.getText().toString(),
                    mobNum,
                    sCompany.getSelectedItem().toString(),
                    getIDForSelectedCompany(sCompany.getSelectedItem().toString()),
                    etWing.getText().toString(),
                    etFlatNum.getText().toString()
            ), status.getSubscriptionStatus().getUserId());


            Log.d("onesignal", status.getSubscriptionStatus().getUserId());

        } else {
            Toast.makeText(this, "OTP verification failed", Toast.LENGTH_SHORT).show();
            aviLogin.setVisibility(View.GONE);
            aviLogin.hide();
            bLogin.setClickable(true);
            bLogin.setFocusable(true);
            bLogin.setFocusableInTouchMode(true);
            bLogin.setEnabled(true);
        }


    }

    public void onEvent(ApartmentsFetchEvent apartmentsFetchEvent) {

        aviLogin.hide();
        aviLogin.setVisibility(View.GONE);
        bLogin.setEnabled(true);
        bLogin.setFocusable(true);
        bLogin.setClickable(true);
        bLogin.setFocusableInTouchMode(true);

        if (apartmentsFetchEvent.isFetchComplete()) {

            apartmentItems = apartmentsFetchEvent.getApartmentItems();

            ArrayList<String> companyNames = new ArrayList<>();

            Log.d("event", "here");

            for (int i = 0; i < apartmentItems.size(); i++) {
                companyNames.add(apartmentItems.get(i).getApartmentName());
                Log.d("companies", apartmentItems.get(i).getApartmentName());
            }

            Collections.sort(companyNames);

            ArrayAdapter<String> companiesAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item, companyNames);

            sCompany.setAdapter(companiesAdapter);
        } else {
            Toast.makeText(this, "Failed to fetch companies", Toast.LENGTH_SHORT).show();
        }
    }

    public void onEvent(LoginEvent loginEvent) {

        aviLogin.hide();
        aviLogin.setVisibility(View.GONE);
        bLogin.setEnabled(true);
        bLogin.setFocusable(true);
        bLogin.setClickable(true);
        bLogin.setFocusableInTouchMode(true);

        if (loginEvent.isLoginSucceeded()) {
            SharedPreferences preferences = getSharedPreferences("LoginDetails", MODE_PRIVATE);
            SharedPreferences.Editor editor = preferences.edit();
            editor.putString("mobileNum", loginEvent.getResidentItem().getMobNum());
            editor.putString("apartmentName", loginEvent.getResidentItem().getApartmentName());
            editor.putString("sessionID", loginEvent.getSessionID());
            editor.putString("residentName", loginEvent.getResidentItem().getResidentName());
            editor.putString("wing", loginEvent.getResidentItem().getWing());
            editor.putString("flatNum", loginEvent.getResidentItem().getFlatNum());
            editor.putInt("apartmentID", loginEvent.getResidentItem().getApartmentID());
            editor.putBoolean("isLoggedIn", true);
            editor.apply();

             /*
                Initializing db
             */
            DbHelper dbHelper = new DbHelper(this);

            getDSPLinks();
//            getRooms(preferences.getString("sessionID", null));
        }
    }

    private void getRooms(String sessionID) {
        AQuery aQuery = new AQuery(getApplicationContext());

        HashMap<String, Object> roomsParams = new HashMap<>();
        roomsParams.put("sessionID", sessionID);

        aQuery.ajax(getRooms, roomsParams, JSONObject.class, new AjaxCallback<JSONObject>() {
            @Override
            public void callback(String url, JSONObject object, AjaxStatus status) {
                String roomName, roomDescription, rooms;
                int roomID, num;

                if (object != null) {
                    Log.d("getrooms", "object != null");
                    try {
                        rooms = object.getString("rooms");
                        num = object.getInt("num");

                        JSONObject reader = new JSONObject(rooms);

                        for (int i = 0; i < num; i++) {
                            JSONObject obj = reader.getJSONObject(String.valueOf(i));
                            roomName = obj.getString("name");
                            roomDescription = obj.getString("description");
                            roomID = obj.getInt("roomID");

                            roomItems.add(new RoomItem(roomID, roomName, roomDescription));
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                } else {
                    Toast.makeText(getApplicationContext(), "Failed to get Rooms try again.", Toast.LENGTH_SHORT).show();
                    Log.d("getrooms", "object = null");
                }

                if (status.getCode() == 200) {
                    DbHelper helper = new DbHelper(getApplicationContext());

                    for (int i = 0; i < roomItems.size(); i++)
                        helper.addRoom(roomItems.get(i));


//                    getDSPLinks();
                }
            }
        });
    }

    private void getDSPLinks(){
        AQuery aQuery = new AQuery(getApplicationContext());

        SharedPreferences sharedPreferences = getSharedPreferences("LoginDetails", Context.MODE_PRIVATE);
        String sessionID = sharedPreferences.getString("sessionID", null);

        HashMap<String, Object> getDSPLinkParams = new HashMap<>();
        getDSPLinkParams.put("sessionID", sessionID);

        aQuery.ajax(getDSPLinks, getDSPLinkParams, JSONObject.class, new AjaxCallback<JSONObject>(){
            @Override
            public void callback(String url, JSONObject object, AjaxStatus status) {
                String dspLinks;

                String work, dspName;
                int dspID, dspLinkID, num;

                if(object != null){
                    try{
                        Log.d("dspLink", "object != null");
                        dspLinks = object.getString("dspLinks");
                        num = object.getInt("num");

                        JSONObject reader = new JSONObject(dspLinks);

                        for(int i = 0; i < num; i++) {
                            JSONObject obj = reader.getJSONObject(String.valueOf(i));

                            dspName = obj.getString("name");
                            dspLinkID = obj.getInt("dspLinkID");
                            dspID = obj.getInt("dspID");
                            work = obj.getString("work");

                            dspLinkItems.add(new DSPLinkItem(dspLinkID, dspID, dspName, work));
                        }

                    }catch (Exception e){
                        e.printStackTrace();
                    }
                }else{
                    Log.d("dspLink", "object = null status: " + status.getCode());
                    Toast.makeText(getApplicationContext(), "Failed to get Daily Service Providers", Toast.LENGTH_SHORT).show();
                }

                if(status.getCode() == 200){
                    DbHelper dbHelper = new DbHelper(getApplicationContext());

                    for(int i = 0; i < dspLinkItems.size(); i++){
                        dbHelper.addDSPLink(dspLinkItems.get(i));
                    }

                    Intent i = new Intent(getApplicationContext(), AdminLogin.class);
                    startActivity(i);
                }
            }
        });
    }

    @Override
    protected void onPause() {
        super.onPause();
        SharedPreferences preferences = getSharedPreferences("LoginDetails", MODE_PRIVATE);
        if (preferences.getBoolean("isLoggedIn", false))
            finish();
    }
}
