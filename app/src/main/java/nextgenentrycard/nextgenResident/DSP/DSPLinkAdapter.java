package nextgenentrycard.nextgenResident.DSP;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.yarolegovich.lovelydialog.LovelyChoiceDialog;

import java.util.ArrayList;

import nextgenentrycard.nextgenResident.DbHelper.DbHelper;
import nextgenentrycard.nextgenResident.Models.DSPLinkItem;
import nextgenentrycard.nextgenResident.R;

/**
 * Created by Ashish Pawar(ashishpawar2015.ap@gmail.com) on 12/3/18.
 */

public class DSPLinkAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private ArrayList<DSPLinkItem> dspLinkItems;
    private Context context;

    public DSPLinkAdapter(ArrayList<DSPLinkItem> dspLinkItems){
        this.dspLinkItems = dspLinkItems;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.dsp_single_item, parent, false);
        return new DSPLinkHolder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        final DSPLinkHolder dspLinkHolder = (DSPLinkHolder)holder;
        context = dspLinkHolder.llDSP.getContext();

        dspLinkHolder.tvWork.setText(dspLinkItems.get(dspLinkHolder.getAdapterPosition()).getWork());
        dspLinkHolder.tvDSPName.setText(dspLinkItems.get(dspLinkHolder.getAdapterPosition()).getDspName());

        final DbHelper dbHelper = new DbHelper(context);

        dspLinkHolder.llDSP.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                new LovelyChoiceDialog(context)
                        .setTopColorRes(R.color.colorPrimary)
                        .setIcon(R.drawable.options)
                        .setItems(new String[]{"Call"}, new LovelyChoiceDialog.OnItemSelectedListener<String>() {
                            @Override
                            public void onItemSelected(int position, String item) {
                                if(position == 0){
                                    Intent intent = new Intent(Intent.ACTION_DIAL, Uri.parse("tel:" + dbHelper.getDSPItem(dspLinkItems.get(dspLinkHolder.getAdapterPosition()).getDspID()).getMobNum()));
                                    context.startActivity(intent);
                                }
                            }
                        })
                        .show();
            }
        });

    }

    @Override
    public int getItemCount() {
        return dspLinkItems.size();
    }

    public class DSPLinkHolder extends RecyclerView.ViewHolder {
        TextView tvDSPName, tvWork;
        LinearLayout llDSP;
        public DSPLinkHolder(View itemView) {
            super(itemView);
            tvDSPName = (TextView)itemView.findViewById(R.id.tvDSP_dspSingleItem);
            tvWork = (TextView)itemView.findViewById(R.id.tvWork_dspSingleItem);
            llDSP = (LinearLayout)itemView.findViewById(R.id.llDSPSingleItem);
        }
    }

    public void update(ArrayList<DSPLinkItem> dspLinkItems){
        this.dspLinkItems = dspLinkItems;
        this.notifyDataSetChanged();
    }
}
