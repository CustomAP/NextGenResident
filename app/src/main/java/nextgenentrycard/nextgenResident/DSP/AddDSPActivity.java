package nextgenentrycard.nextgenResident.DSP;

import android.app.Dialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.androidquery.AQuery;
import com.androidquery.callback.AjaxCallback;
import com.androidquery.callback.AjaxStatus;
import com.rengwuxian.materialedittext.MaterialEditText;
import com.yarolegovich.lovelydialog.LovelyCustomDialog;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;

import nextgenentrycard.nextgenResident.DbHelper.DbHelper;
import nextgenentrycard.nextgenResident.Models.DSPItem;
import nextgenentrycard.nextgenResident.Models.DSPLinkItem;
import nextgenentrycard.nextgenResident.R;

/**
 * Created by Ashish Pawar(ashishpawar2015.ap@gmail.com) on 10/3/18.
 */

public class AddDSPActivity extends AppCompatActivity {
    RecyclerView rvDSP;
    FloatingActionButton fabAddDSP;
    Dialog addDspDialog;
    Spinner sDSPName;
    TextView tvDSPName, tvAdd;
    MaterialEditText etWork;
    AQuery aQuery;
    ArrayList<DSPItem> dspItems;
    ArrayList<DSPLinkItem> dspLinkItems;
    Context context;
    CardView cvNoDSP,cvAddDSP;

    String addDSPLink = "http://www.accesspoint.in/index.php/DSP/addDspLink";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_dsp);
        setTitle("Daily Service Provider");

        fabAddDSP = (FloatingActionButton) findViewById(R.id.bAddDSP);
        rvDSP = (RecyclerView)findViewById(R.id.rvDSP);
        cvNoDSP = (CardView)findViewById(R.id.cvNoDSP);
        cvAddDSP = (CardView)findViewById(R.id.cvAddDSP);

        context = this;
        aQuery = new AQuery(context);

        DbHelper dbHelper = new DbHelper(getApplicationContext());
        dspLinkItems = dbHelper.getDSPLinkItems();

        if(dspLinkItems.size() != 0) {
            cvNoDSP.setVisibility(View.GONE);
            cvAddDSP.setVisibility(View.VISIBLE);
        }

        final DSPLinkAdapter dspLinkAdapter = new DSPLinkAdapter(dspLinkItems);
        rvDSP.setAdapter(dspLinkAdapter);
        rvDSP.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
        rvDSP.setItemAnimator(new DefaultItemAnimator());

        fabAddDSP.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addDspDialog = new LovelyCustomDialog(context)
                        .setView(R.layout.fragment_add_dsp_dialog)
                        .setTopColorRes(R.color.colorPrimary)
                        .setIcon(R.drawable.add_dsp)
                        .configureView(new LovelyCustomDialog.ViewConfigurator() {
                            @Override
                            public void configureView(View v) {
                                sDSPName = (Spinner) v.findViewById(R.id.sDSP_fragmentAddDSPDialog);
                                tvDSPName = (TextView) v.findViewById(R.id.tvDSP_fragmentAddDSPDialog);
                                etWork = (MaterialEditText) v.findViewById(R.id.etTypeOfWork_fragmentAddDSPDialog);
                                tvAdd = (TextView) v.findViewById(R.id.tvAdd_fragmentAddDSPDialog);

                                DbHelper dbHelper = new DbHelper(getApplicationContext());
                                dspItems = dbHelper.getDSPItems();

                                Collections.sort(dspItems);

                                String dspNames[] = new String[dspItems.size()];

                                for (int i = 0; i < dspItems.size(); i++)
                                    dspNames[i] = dspItems.get(i).getDspName();

                                Log.d("dspnames",dspNames.length +"");

                                ArrayAdapter<String> dspAdapter = new ArrayAdapter<String>(getApplicationContext(), android.R.layout.simple_spinner_dropdown_item, dspNames);

                                sDSPName.setAdapter(dspAdapter);

                            }
                        })
                        .setListener(R.id.tvAdd_fragmentAddDSPDialog, new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                if (tvDSPName.getVisibility() == View.GONE) {
                                    if(!etWork.getText().toString().equals("")) {
                                        SharedPreferences sharedPreferences = getSharedPreferences("LoginDetails", Context.MODE_PRIVATE);
                                        String sessionID = sharedPreferences.getString("sessionID", null);

                                        final int dspID = dspItems.get(sDSPName.getSelectedItemPosition()).getDspID();
                                        final String dspName = sDSPName.getSelectedItem().toString();
                                        final String work = etWork.getText().toString();

                                        HashMap<String, Object> dspLinkParams = new HashMap<>();
                                        dspLinkParams.put("sessionID", sessionID);
                                        dspLinkParams.put("dspID", dspID);
                                        dspLinkParams.put("work", work);

                                        aQuery.ajax(addDSPLink, dspLinkParams, JSONObject.class, new AjaxCallback<JSONObject>() {
                                            @Override
                                            public void callback(String url, JSONObject object, AjaxStatus status) {
                                                int dspLinkID = 0;
                                                if (object != null) {
                                                    try {
                                                        Log.d("addDspLink", "object != null");
                                                        dspLinkID = object.getInt("dspLinkID");
                                                    } catch (Exception e) {
                                                        e.printStackTrace();
                                                    }
                                                } else {
                                                    Log.d("addDspLink", "object != null status: " + status.getCode());
                                                    Toast.makeText(getApplicationContext(), "Failed to add Daily Service Provider", Toast.LENGTH_SHORT).show();
                                                }

                                                if (status.getCode() == 200) {
                                                    DbHelper dbHelper = new DbHelper(getApplicationContext());
                                                    dbHelper.addDSPLink(new DSPLinkItem(dspLinkID, dspID, dspName, work));

                                                    dspLinkItems = dbHelper.getDSPLinkItems();
                                                    dspLinkAdapter.update(dspLinkItems);

                                                    cvAddDSP.setVisibility(View.VISIBLE);
                                                    cvNoDSP.setVisibility(View.GONE);
                                                }

                                                Toast.makeText(getApplicationContext(), "DSP added successfully", Toast.LENGTH_SHORT).show();

                                                addDspDialog.dismiss();
                                            }
                                        });

                                    }else{
                                        Toast.makeText(getApplicationContext(), "Enter type of work", Toast.LENGTH_SHORT).show();
                                    }
                                } else {
                                    Toast.makeText(getApplicationContext(), "Select Daily Service Provider", Toast.LENGTH_SHORT).show();
                                }
                            }
                        })
                        .setListener(R.id.tvDSP_fragmentAddDSPDialog, new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                tvDSPName.setVisibility(View.GONE);
                                sDSPName.setVisibility(View.VISIBLE);
                                sDSPName.performClick();
                            }
                        })
                        .show();
            }
        });
    }


}
