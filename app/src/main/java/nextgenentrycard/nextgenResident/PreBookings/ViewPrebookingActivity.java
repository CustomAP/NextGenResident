package nextgenentrycard.nextgenResident.PreBookings;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;

import nextgenentrycard.nextgenResident.Models.PreBookItem;
import nextgenentrycard.nextgenResident.R;

/**
 * Created by Ashish Pawar(ashishpawar2015.ap@gmail.com) on 27/10/17.
 */

public class ViewPrebookingActivity extends AppCompatActivity {
    TextView tvName,tvOrganisation,tvDate,tvTime, tvApartmentName, tvSecurityName;
    ImageButton bOkay;
    PreBookItem preBookItem;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_prebook);
        setTitle("PreBooked Visitor");

        preBookItem= (PreBookItem) getIntent().getSerializableExtra("PreBookItem");

        /*
            initializing views
         */
        tvDate=(TextView)findViewById(R.id.tvDate_viewPrebooking);
        tvTime=(TextView)findViewById(R.id.tvTime_viewPrebooking);
        tvName=(TextView)findViewById(R.id.tvName_viewPrebooking);
//        tvPurpose=(TextView)findViewById(R.id.tvPurposeOfMeet_viewPrebooking);
        tvOrganisation=(TextView)findViewById(R.id.tvOrganisationName_viewPrebooking);
        bOkay=(ImageButton) findViewById(R.id.bOkay_viewPrebooking);
        tvApartmentName = (TextView)findViewById(R.id.tvApartmentName_viewPreBookings);
        tvSecurityName = (TextView)findViewById(R.id.tvSecurityName_viewPreBookings);
        /*
            setting field values
         */
        tvName.setText(preBookItem.getVisitorName());
        tvDate.setText(preBookItem.getDate());
//        tvType.setText(preBookItem.getType());
        tvTime.setText(preBookItem.getTime());
//        tvPurpose.setText(preBookItem.getPurposeOfMeet());
        tvOrganisation.setText(preBookItem.getOrganisation());

        SharedPreferences sharedPreferences = getSharedPreferences("LoginDetails", Context.MODE_PRIVATE);
        tvApartmentName.setText(sharedPreferences.getString("apartmentName", ""));
        tvSecurityName.setText(sharedPreferences.getString("securityName", ""));

        /*
            setting onclick listener
         */
        bOkay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }
}
