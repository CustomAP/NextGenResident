package nextgenentrycard.nextgenResident.PreBookings;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.CardView;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.Collections;

import nextgenentrycard.nextgenResident.DbHelper.DbHelper;
import nextgenentrycard.nextgenResident.Models.PreBookItem;
import nextgenentrycard.nextgenResident.R;

/**
 * Created by Ashish Pawar(ashishpawar2015.ap@gmail.com) on 26/10/17.
 */

public class PreBookingFragment extends Fragment{
    RecyclerView rvPrebookings;
    CardView cvNoPrebookings;
    ArrayList<PreBookItem> preBookItems;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_prebookings,container,false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        rvPrebookings=(RecyclerView)view.findViewById(R.id.rvPrebookings);
        cvNoPrebookings=(CardView)view.findViewById(R.id.cvNoPrebookings);

        DbHelper dbHelper=new DbHelper(getContext());

        preBookItems=dbHelper.getAllPreBookVisitors();

        Collections.reverse(preBookItems);

        if(preBookItems.size()!=0)
            cvNoPrebookings.setVisibility(View.GONE);

        PreBookingAdapter preBookingAdapter=new PreBookingAdapter(preBookItems);

        rvPrebookings.setAdapter(preBookingAdapter);
        rvPrebookings.setLayoutManager(new LinearLayoutManager(getContext()));
        rvPrebookings.setItemAnimator(new DefaultItemAnimator());
    }
}
