package nextgenentrycard.nextgenResident.PreBookings;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;

import nextgenentrycard.nextgenResident.Models.PreBookItem;
import nextgenentrycard.nextgenResident.PreBook.PreBookActivity;
import nextgenentrycard.nextgenResident.R;

/**
 * Created by Ashish Pawar(ashishpawar2015.ap@gmail.com) on 26/10/17.
 */

public class PreBookingAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private ArrayList<PreBookItem> preBookItems;
    private Context context;

    public PreBookingAdapter(ArrayList<PreBookItem> preBookItems){
        this.preBookItems=preBookItems;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(parent.getContext()).inflate(R.layout.prebook_single_item,parent,false);
        return new PreBookingHolder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        final PreBookingHolder preBookingHolder=(PreBookingHolder)holder;
        context=preBookingHolder.llPrebooking.getContext();

        /*
            setting field values
         */
        preBookingHolder.tvDate.setText(preBookItems.get(position).getDate());
        preBookingHolder.tvName.setText(preBookItems.get(position).getVisitorName());
        preBookingHolder.tvTime.setText(preBookItems.get(position).getTime());

        preBookingHolder.bRebook.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(context, PreBookActivity.class);
                i.putExtra("PreBookItem",preBookItems.get(preBookingHolder.getAdapterPosition()));
                context.startActivity(i);
            }
        });

        /*
            setting on click listeners
         */
        preBookingHolder.llPrebooking.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i =new Intent(context,ViewPrebookingActivity.class);
                i.putExtra("PreBookItem",preBookItems.get(preBookingHolder.getAdapterPosition()));
                context.startActivity(i);
            }
        });
    }

    @Override
    public int getItemCount() {
        return preBookItems.size();
    }

    private class PreBookingHolder extends RecyclerView.ViewHolder {
        TextView tvDate, tvName, tvTime;
        LinearLayout llPrebooking;
        TextView bRebook;

        public PreBookingHolder(View itemView) {
            super(itemView);
            tvDate=(TextView)itemView.findViewById(R.id.tvDate_prebooking);
            llPrebooking=(LinearLayout)itemView.findViewById(R.id.llPrebook);
            tvName=(TextView)itemView.findViewById(R.id.tvName_PreBook);
            tvTime=(TextView)itemView.findViewById(R.id.tvTimePrebooking);
            bRebook=(TextView) itemView.findViewById(R.id.brebook_prebooking);
        }

    }
}
