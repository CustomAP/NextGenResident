package nextgenentrycard.nextgenResident;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import com.androidquery.AQuery;
import com.androidquery.callback.AjaxCallback;
import com.androidquery.callback.AjaxStatus;
import com.onesignal.OSPermissionSubscriptionState;
import com.onesignal.OneSignal;
import com.yarolegovich.lovelydialog.LovelyTextInputDialog;

import org.json.JSONObject;

import java.util.HashMap;

import nextgenentrycard.nextgenResident.ChatBot.ChatBotActivity;
import nextgenentrycard.nextgenResident.Home.HomeFragment;
import nextgenentrycard.nextgenResident.PreBook.PreBookServerConnection;
import nextgenentrycard.nextgenResident.Profile.ProfileActivity;
import nextgenentrycard.nextgenResident.Sync.DSPSync;
import nextgenentrycard.nextgenResident.Sync.NewVisitorSync;

/**
 * Created by Ashish Pawar(ashishpawar2015.ap@gmail.com) on 30/1/18.
 */

public class MainActivity2 extends AppCompatActivity {

    private boolean exit = false;
    Handler mHandler = new Handler();
    final int INTERVAL = 1000 * 15;

    String sendFeedBack = "http://www.accesspoint.in/index.php/Feedback/sendFeedback";
    private AQuery aQuery;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);

        aQuery = new AQuery(getApplicationContext());

        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.add(R.id.mainactivity_container, new HomeFragment());
        fragmentTransaction.commit();

        mHandlerTask.run();

    }

    Runnable mHandlerTask = new Runnable() {
        @Override
        public void run() {

            NewVisitorSync newVisitorSync = new NewVisitorSync(getApplicationContext());
            newVisitorSync.fetchNewVisitors();

            DSPSync dspSync = new DSPSync(getApplicationContext());
            dspSync.getDSPs();

            PreBookServerConnection preBookServerConnection = new PreBookServerConnection(getApplicationContext());
            preBookServerConnection.getPreBookIDsToDelete();

            mHandler.postDelayed(mHandlerTask, INTERVAL);
        }
    };

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        /*
            colors:
            6680cbc4
            2c3039
            04bebd
            b7ce63
            80bebebe
            262626
            404040
            333333
            05bdbf
            009688
         */

        //noinspection SimplifiableIfStatement
        if (id == R.id.menu_about) {
            Intent i = new Intent(getApplicationContext(), AboutActivity.class);
            getApplicationContext().startActivity(i);
            return true;
        }else if(id == R.id.menu_profile){
            Intent i = new Intent(getApplicationContext(), ProfileActivity.class);
            startActivity(i);
        } else if(id == R.id.menu_feedback){
            new LovelyTextInputDialog(this)
                    .setTitle("FeedBack")
                    .setTopColorRes(R.color.colorPrimary)
                    .setIcon(R.drawable.feedback)
                    .setConfirmButton("SEND", new LovelyTextInputDialog.OnTextInputConfirmListener() {
                        @Override
                        public void onTextInputConfirmed(String text) {
                            SharedPreferences sharedPreferences = getSharedPreferences("LoginDetails", Context.MODE_PRIVATE);
                            String sessionID = sharedPreferences.getString("sessionID", null);

                            HashMap<String, Object> feedbackParams = new HashMap<>();
                            feedbackParams.put("sessionID", sessionID);
                            feedbackParams.put("feedback", text);

                            aQuery.ajax(sendFeedBack, feedbackParams, JSONObject.class, new AjaxCallback<JSONObject>(){
                                @Override
                                public void callback(String url, JSONObject object, AjaxStatus status) {
                                    int result = 0;

                                    if(object != null){
                                        Log.d("sendFeedback", "object != null");
                                        try{
                                            result = object.getInt("result");
                                        } catch (Exception e) {
                                            e.printStackTrace();
                                        }
                                    } else {
                                        Log.d("sendFeedback", "object = null status :" +status.getCode());
                                        Toast.makeText(getApplicationContext(), "Failed to send feedback", Toast.LENGTH_SHORT).show();
                                    }

                                    if (status.getCode() == 200) {
                                        if(result == 0) {
                                            Toast.makeText(getApplicationContext(), "Feedback sent successfully", Toast.LENGTH_SHORT).show();
                                        }
                                    }
                                }
                            });
                        }
                    })
                    .show();
        } else if(id == R.id.menu_chatbot){
            Intent i = new Intent(getApplicationContext(), ChatBotActivity.class);
            startActivity(i);
        }

        return super.onOptionsItemSelected(item);
    }
}
