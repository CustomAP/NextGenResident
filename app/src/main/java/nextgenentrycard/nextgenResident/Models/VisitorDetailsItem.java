package nextgenentrycard.nextgenResident.Models;

/**
 * Created by Ashish Pawar(ashishpawar2015.ap@gmail.com) on 19/1/18.
 */

public class VisitorDetailsItem {
    private int visitorID, employeeID;
    private String visitorName, employeeName, inTime, outTime, date;

    public int getVisitorID() {
        return visitorID;
    }

    public String getVisitorName() {
        return visitorName;
    }

    public String getEmployeeName() {
        return employeeName;
    }

    public String getInTime() {
        return inTime;
    }

    public String getOutTime() {
        return outTime;
    }

    public String getDate() {
        return date;
    }

    public VisitorDetailsItem(int visitorID, String visitorName, String employeeName, String inTime, String outTime, String date, int employeeID) {

        this.visitorID = visitorID;
        this.visitorName = visitorName;
        this.employeeName = employeeName;
        this.inTime = inTime;
        this.outTime = outTime;
        this.employeeID = employeeID;
        this.date = date;

    }

    public int getEmployeeID() {
        return employeeID;
    }
}
