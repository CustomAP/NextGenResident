package nextgenentrycard.nextgenResident.Models;

/**
 * Created by Ashish Pawar(ashishpawar2015.ap@gmail.com) on 18/1/18.
 */

public class VisitorItem {
    private int visitorID;
    private String visitorName;

    public int getVisitorID() {
        return visitorID;
    }

    public String getVisitorName() {
        return visitorName;
    }

    public VisitorItem(int visitorID, String visitorName) {

        this.visitorID = visitorID;
        this.visitorName = visitorName;
    }
}
