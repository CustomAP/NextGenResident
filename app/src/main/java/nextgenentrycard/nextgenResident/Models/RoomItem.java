package nextgenentrycard.nextgenResident.Models;

/**
 * Created by Ashish Pawar(ashishpawar2015.ap@gmail.com) on 19/2/18.
 */

public class RoomItem {
    private int roomID;
    private String name, description;

    public int getRoomID() {
        return roomID;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public RoomItem(int roomID, String name, String description) {

        this.roomID = roomID;
        this.name = name;
        this.description = description;
    }
}
