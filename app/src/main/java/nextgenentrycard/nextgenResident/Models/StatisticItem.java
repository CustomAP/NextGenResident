package nextgenentrycard.nextgenResident.Models;

/**
 * Created by Ashish Pawar(ashishpawar2015.ap@gmail.com) on 17/1/18.
 */

public class StatisticItem {
    private int dailyVisitors, preBookedVisitors, newVisitors, vendors, guests, preLunch, postLunch;
    private PurposeOfMeetItem purposeOfMeetItem;

    public int getDailyVisitors() {
        return dailyVisitors;
    }

    public int getPreBookedVisitors() {
        return preBookedVisitors;
    }

    public int getNewVisitors() {
        return newVisitors;
    }

    public int getVendors() {
        return vendors;
    }

    public int getGuests() {
        return guests;
    }

    public int getPreLunch() {
        return preLunch;
    }

    public int getPostLunch() {
        return postLunch;
    }

    public PurposeOfMeetItem getPurposeOfMeetItem() {
        return purposeOfMeetItem;
    }

    public StatisticItem(int dailyVisitors, int preBookedVisitors, int newVisitors, int vendors, int guests, int preLunch, int postLunch, PurposeOfMeetItem purposeOfMeetItem) {
        this.dailyVisitors = dailyVisitors;
        this.preBookedVisitors = preBookedVisitors;
        this.newVisitors = newVisitors;
        this.vendors = vendors;
        this.guests = guests;
        this.preLunch = preLunch;
        this.postLunch = postLunch;
        this.purposeOfMeetItem = purposeOfMeetItem;
    }
}
