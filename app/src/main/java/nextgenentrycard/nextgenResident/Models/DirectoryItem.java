package nextgenentrycard.nextgenResident.Models;

/**
 * Created by Ashish Pawar(ashishpawar2015.ap@gmail.com) on 18/3/18.
 */

public class DirectoryItem {
    private int directoryID;
    private String mobNum, name, wing, flatNum;

    public int getDirectoryID() {
        return directoryID;
    }

    public String getMobNum() {
        return mobNum;
    }

    public String getName() {
        return name;
    }

    public String getWing() {
        return wing;
    }

    public String getFlatNum() {
        return flatNum;
    }

    public DirectoryItem(int directoryID, String mobNum, String name, String wing, String flatNum) {

        this.directoryID = directoryID;
        this.mobNum = mobNum;
        this.name = name;
        this.wing = wing;
        this.flatNum = flatNum;
    }
}
