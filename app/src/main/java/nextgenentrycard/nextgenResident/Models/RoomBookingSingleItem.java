package nextgenentrycard.nextgenResident.Models;

/**
 * Created by Ashish Pawar(ashishpawar2015.ap@gmail.com) on 19/2/18.
 */

public class RoomBookingSingleItem {
    private int roomBookingID, employeeID;
    private String startTime, endTime, employeeName;

    public int getRoomBookingID() {
        return roomBookingID;
    }

    public int getEmployeeID() {
        return employeeID;
    }

    public String getStartTime() {
        return startTime;
    }

    public String getEndTime() {
        return endTime;
    }

    public String getEmployeeName() {
        return employeeName;
    }

    public RoomBookingSingleItem(int roomBookingID, int employeeID, String startTime, String endTime, String employeeName) {

        this.roomBookingID = roomBookingID;
        this.employeeID = employeeID;
        this.startTime = startTime;
        this.endTime = endTime;
        this.employeeName = employeeName;
    }
}
