package nextgenentrycard.nextgenResident.Models;

/**
 * Created by Ashish Pawar(ashishpawar2015.ap@gmail.com) on 19/5/18.
 */

public class VoteItem {
    private int voteID, optionNum;
    private String userName, wing, flatNum;

    public int getVoteID() {
        return voteID;
    }

    public int getOptionNum() {
        return optionNum;
    }

    public String getUserName() {
        return userName;
    }

    public String getWing() {
        return wing;
    }

    public String getFlatNum() {
        return flatNum;
    }

    public VoteItem(int voteID, int optionNum, String userName, String wing, String flatNum) {

        this.voteID = voteID;
        this.optionNum = optionNum;
        this.userName = userName;
        this.wing = wing;
        this.flatNum = flatNum;
    }
}
