package nextgenentrycard.nextgenResident.Models;

import java.io.Serializable;

/**
 * Created by Ashish Pawar(ashishpawar2015.ap@gmail.com) on 17/3/18.
 */

public class NearbyUtilitiesItem implements Serializable{
    private int utilityID;

    public int getDelivery() {
        return delivery;
    }

    private int delivery;
    private String storeName, proprietorName, mobileNum, secondaryMobNum;

    public int getUtilityID() {
        return utilityID;
    }

    public String getSecondaryMobNum() {
        return secondaryMobNum;
    }

    public String getStoreName() {
        return storeName;
    }

    public String getProprietorName() {
        return proprietorName;
    }

    public String getMobileNum() {
        return mobileNum;
    }

    public NearbyUtilitiesItem(int utilityID, String storeName, String proprietorName, String mobileNum, String secondaryMobNum, int delivery) {

        this.utilityID = utilityID;
        this.storeName = storeName;
        this.proprietorName = proprietorName;
        this.mobileNum = mobileNum;
        this.secondaryMobNum = secondaryMobNum;
        this.delivery = delivery;
    }
}
