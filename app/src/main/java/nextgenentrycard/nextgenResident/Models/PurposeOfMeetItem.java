package nextgenentrycard.nextgenResident.Models;

/**
 * Created by Ashish Pawar(ashishpawar2015.ap@gmail.com) on 17/1/18.
 */

public class PurposeOfMeetItem {
    private int vendor, conference, government, interview, official, personal ,promotional, social;

    public int getVendor() {
        return vendor;
    }

    public int getConference() {
        return conference;
    }

    public int getGovernment() {
        return government;
    }

    public int getInterview() {
        return interview;
    }

    public int getOfficial() {
        return official;
    }

    public int getPersonal() {
        return personal;
    }

    public int getPromotional() {
        return promotional;
    }

    public int getSocial() {
        return social;
    }

    public PurposeOfMeetItem(int vendor, int conference, int government, int interview, int official, int personal, int promotional, int social) {
        this.vendor = vendor;
        this.conference = conference;
        this.government = government;
        this.interview = interview;
        this.official = official;
        this.personal = personal;
        this.promotional = promotional;
        this.social = social;
    }
}
