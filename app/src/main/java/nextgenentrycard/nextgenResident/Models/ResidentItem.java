package nextgenentrycard.nextgenResident.Models;

/**
 * Created by Ashish Pawar(ashishpawar2015.ap@gmail.com) on 25/10/17.
 */

public class ResidentItem {
    private String residentName;
    private String mobNum;
    private String apartmentName;
    private int apartmentID;
    private String wing, flatNum;

    public ResidentItem(String residentName, String mobNum, String apartmentName, int apartmentID, String wing, String flatNum) {
        this.residentName = residentName;
        this.mobNum = mobNum;
        this.apartmentName = apartmentName;
        this.apartmentID = apartmentID;
        this.wing = wing;
        this.flatNum = flatNum;
    }

    public String getApartmentName() {
        return apartmentName;
    }

    public String getResidentName() {
        return residentName;
    }

    public String getMobNum() {
        return mobNum;
    }

    public int getApartmentID() {
        return apartmentID;
    }

    public String getWing() {
        return wing;
    }

    public String getFlatNum() {
        return flatNum;
    }
}
