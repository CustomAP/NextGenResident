package nextgenentrycard.nextgenResident.Models;

import java.io.Serializable;

/**
 * Created by Ashish Pawar(ashishpawar2015.ap@gmail.com) on 27/10/17.
 */

public class NewVisitorItem implements Serializable{
    private String mobNum;
    private String name;
    private String purposeOfMeet;
    private String organisation;
    private String govtID;
    private String IDNum;
    private String type;
    private String inTime;
    private String outTime;
    private int visitorID;
    private String date;
    private int exited;

    public String getDate() {
        return date;
    }

    public String getMobNum() {
        return mobNum;
    }

    public String getName() {
        return name;
    }

    public String getPurposeOfMeet() {
        return purposeOfMeet;
    }

    public String getOrganisation() {
        return organisation;
    }

    public String getGovtID() {
        return govtID;
    }

    public String getIDNum() {
        return IDNum;
    }

    public String getType() {
        return type;
    }

    public String getInTime() {
        return inTime;
    }

    public String getOutTime() {
        return outTime;
    }

    public int getVisitorID() {
        return visitorID;
    }

    public int getExited() {
        return exited;
    }

    public void setExited(int exited) {
        this.exited = exited;
    }

    public NewVisitorItem(String mobNum, String name, String purposeOfMeet, String organisation, String govtID, String IDNum, String type, String inTime, String outTime, int visitorID, String date, int exited) {

        this.mobNum = mobNum;
        this.name = name;
        this.purposeOfMeet = purposeOfMeet;
        this.organisation = organisation;
        this.govtID = govtID;
        this.IDNum = IDNum;
        this.type = type;
        this.inTime = inTime;
        this.outTime = outTime;
        this.visitorID = visitorID;
        this.date=date;
        this.exited = exited;
    }
}
