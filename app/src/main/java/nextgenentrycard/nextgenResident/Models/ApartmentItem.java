package nextgenentrycard.nextgenResident.Models;

import android.support.annotation.NonNull;

import java.io.Serializable;

/**
 * Created by Ashish Pawar(ashishpawar2015.ap@gmail.com) on 19/10/17.
 */

public class ApartmentItem implements Serializable,Comparable<ApartmentItem> {
    private String apartmentName;
    private int apartmentID;

    public String getApartmentName() {
        return apartmentName;
    }

    public int getApartmentID() {
        return apartmentID;
    }

    public ApartmentItem(String apartmentName, int apartmentID) {

        this.apartmentName = apartmentName;
        this.apartmentID = apartmentID;
    }

    @Override
    public int compareTo(@NonNull ApartmentItem o) {
        return apartmentName.compareTo(o.apartmentName);
    }
}
