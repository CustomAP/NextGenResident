package nextgenentrycard.nextgenResident.Models;

import java.io.Serializable;

/**
 * Created by Ashish Pawar(ashishpawar2015.ap@gmail.com) on 25/10/17.
 */

public class PreBookItem implements Serializable{
    private String visitorName;
    private String purposeOfMeet;
    private String organisation;
    private String date;
    private String time;
    private String IDProof;
    private String IDNum;
    private String mobNum;
    private String type;
    private int prebookID;
    private int visitorID;

    public void setVisitorID(int visitorID) {
        this.visitorID = visitorID;
    }

    public int getVisitorID() {
        return visitorID;
    }

    public String getIDProof() {
        return IDProof;
    }

    public String getIDNum() {
        return IDNum;
    }

    public String getMobNum() {
        return mobNum;
    }

    public String getType() {
        return type;
    }


    public String getVisitorName() {
        return visitorName;
    }

    public String getPurposeOfMeet() {
        return purposeOfMeet;
    }

    public String getOrganisation() {
        return organisation;
    }

    public String getDate() {
        return date;
    }

    public String getTime() {
        return time;
    }

    public PreBookItem() {

    }

    public void setPrebookID(int prebookID) {
        this.prebookID = prebookID;
    }

    public int getPrebookID() {

        return prebookID;
    }

    public PreBookItem(String visitorName, String purposeOfMeet, String organisation, String date, String time, String IDProof, String IDNum, String mobNum, String type, int prebookID,int visitorID) {

        this.visitorName = visitorName;
        this.purposeOfMeet = purposeOfMeet;
        this.organisation = organisation;
        this.date = date;
        this.time = time;
        this.IDProof = IDProof;
        this.IDNum = IDNum;
        this.mobNum = mobNum;
        this.type = type;
        this.prebookID=prebookID;
        this.visitorID=visitorID;

    }

    public void setVisitorName(String visitorName) {

        this.visitorName = visitorName;
    }

    public void setPurposeOfMeet(String purposeOfMeet) {
        this.purposeOfMeet = purposeOfMeet;
    }

    public void setOrganisation(String organisation) {
        this.organisation = organisation;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public void setIDProof(String IDProof) {
        this.IDProof = IDProof;
    }

    public void setIDNum(String IDNum) {
        this.IDNum = IDNum;
    }

    public void setMobNum(String mobNum) {
        this.mobNum = mobNum;
    }

    public void setType(String type) {
        this.type = type;
    }
}
