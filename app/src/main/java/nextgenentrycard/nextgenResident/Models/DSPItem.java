package nextgenentrycard.nextgenResident.Models;

import android.support.annotation.NonNull;

/**
 * Created by Ashish Pawar(ashishpawar2015.ap@gmail.com) on 11/3/18.
 */

public class DSPItem implements Comparable<DSPItem>{
    private String dspName, mobNum;
    private int dspID;

    public String getDspName() {
        return dspName;
    }

    public int getDspID() {
        return dspID;
    }

    public String getMobNum() {
        return mobNum;
    }

    public DSPItem(String dspName, int dspID, String mobNum) {

        this.dspName = dspName;
        this.dspID = dspID;
        this.mobNum = mobNum;

    }

    @Override
    public int compareTo(@NonNull DSPItem o) {
        return dspName.compareTo(o.dspName);
    }
}
