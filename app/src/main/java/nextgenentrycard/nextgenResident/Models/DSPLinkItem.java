package nextgenentrycard.nextgenResident.Models;

/**
 * Created by Ashish Pawar(ashishpawar2015.ap@gmail.com) on 11/3/18.
 */

public class DSPLinkItem {
    private int dspLinkID, dspID;
    private String dspName, work;

    public int getDspLinkID() {
        return dspLinkID;
    }

    public int getDspID() {
        return dspID;
    }

    public String getDspName() {
        return dspName;
    }

    public String getWork() {
        return work;
    }

    public DSPLinkItem(int dspLinkID, int dspID, String dspName, String work) {

        this.dspLinkID = dspLinkID;
        this.dspID = dspID;
        this.dspName = dspName;
        this.work = work;
    }
}
